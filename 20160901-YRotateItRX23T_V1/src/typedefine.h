
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		typedefine.h
*	Project:	YRotateIt-RX23T (three shunts)
*	Revision:	1.0
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	Data types definition
*******************************************************************************/

#ifndef	_TYPEDEFINE_H
#define	_TYPEDEFINE_H

// MISRA types inclusion
#ifndef _MISRA_H_INCLUDED_
#define _MISRA_H_INCLUDED_

typedef          char		char_t;
typedef unsigned char		uint8_t;
typedef unsigned short int  uint16_t;
typedef unsigned long int   uint32_t;
typedef signed   char		int8_t;
typedef signed   short int	int16_t;
typedef signed   long int	int32_t;
typedef          float		float32_t;
typedef          double		float64_t;

#ifdef TRUE
#undef TRUE
#endif

#ifdef FALSE
#undef FALSE
#endif

typedef enum bool
{
	FALSE = 0,
	TRUE = 1
}	bool_t;

#endif // _MISRA_H_INCLUDED_

// Renesas Project Generator aliases of integer types
typedef signed char				_SBYTE;
typedef unsigned char			_UBYTE;
typedef signed short			_SWORD;
typedef unsigned short			_UWORD;
typedef signed int				_SINT;
typedef unsigned int			_UINT;
typedef signed long				_SDWORD;
typedef unsigned long			_UDWORD;
typedef signed long long		_SQWORD;
typedef unsigned long long		_UQWORD;

// other aliases of integer types
#ifndef MY_INT_TYPES
#define MY_INT_TYPES
typedef signed char				s8;
typedef unsigned char			u8;
typedef signed short			s16;
typedef unsigned short			u16;
typedef signed long				s32;
typedef unsigned long			u32;
typedef signed long long		s64;
typedef unsigned long long		u64;
#endif	// MY_INT_TYPES

// general use types definition
typedef signed char				schar;
typedef const schar				cschar;
typedef schar *					add_schar;
typedef cschar *				add_cschar;
typedef const add_schar			c_add_schar;
typedef const add_cschar		c_add_cschar;
typedef unsigned char			uchar;
typedef const uchar				cuchar;
typedef uchar *					add_uchar;
typedef cuchar *				add_cuchar;
typedef const add_uchar			c_add_uchar;
typedef const add_cuchar		c_add_cuchar;
typedef signed short			sshort;
typedef const sshort			csshort;
typedef sshort *				add_sshort;
typedef csshort *				add_csshort;
typedef const add_sshort		c_add_sshort;
typedef const add_csshort		c_add_csshort;
typedef unsigned short			ushort;
typedef const ushort			cushort;
typedef ushort *				add_ushort;
typedef cushort *				add_cushort;
typedef const add_ushort		c_add_ushort;
typedef const add_cushort		c_add_cushort;
typedef signed int				sint;
typedef const sint				csint;
typedef sint *					add_sint;
typedef csint *					add_csint;
typedef const add_sint			c_add_sint;
typedef const add_csint			c_add_csint;
typedef unsigned int			uint;
typedef const uint				cuint;
typedef uint *					add_uint;
typedef cuint *					add_cuint;
typedef const add_uint			c_add_uint;
typedef const add_cuint			c_add_cuint;
typedef signed long				slong;
typedef const slong				cslong;
typedef slong *					add_slong;
typedef cslong *				add_cslong;
typedef const add_slong			c_add_slong;
typedef const add_cslong		c_add_cslong;
typedef unsigned long			ulong;
typedef const ulong				culong;
typedef ulong *					add_ulong;
typedef culong *				add_culong;
typedef const add_ulong			c_add_ulong;
typedef const add_culong		c_add_culong;

// byte_t definition
#ifndef BYTE_T
	#define BYTE_T
#pragma bit_order left
	typedef union
	{
		unsigned char		byte;
		struct
		{
			unsigned char	b7:1;
			unsigned char	b6:1;
			unsigned char	b5:1;
			unsigned char	b4:1;
			unsigned char	b3:1;
			unsigned char	b2:1;
			unsigned char	b1:1;
			unsigned char	b0:1;
		}					bit;
	} 						byte_t;
#pragma bit_order
#endif

// word_t definition
#ifndef WORD_T
	#define WORD_T
#ifdef __BIG
#pragma bit_order left
	typedef union
	{
		unsigned short		word;
		struct
		{
			unsigned char	h;
			unsigned char	l;
		}					byte;
		struct
		{
			unsigned char	b15:1;
			unsigned char	b14:1;
			unsigned char	b13:1;
			unsigned char	b12:1;
			unsigned char	b11:1;
			unsigned char	b10:1;
			unsigned char	b9:1;
			unsigned char	b8:1;
			unsigned char	b7:1;
			unsigned char	b6:1;
			unsigned char	b5:1;
			unsigned char	b4:1;
			unsigned char	b3:1;
			unsigned char	b2:1;
			unsigned char	b1:1;
			unsigned char	b0:1;
		}					bit;
	} 						word_t;
#else	// __BIG
#pragma bit_order right
	typedef union
	{
		unsigned short		word;
		struct
		{
			unsigned char	l;
			unsigned char	h;
		}					byte;
		struct
		{
			unsigned char	b0:1;
			unsigned char	b1:1;
			unsigned char	b2:1;
			unsigned char	b3:1;
			unsigned char	b4:1;
			unsigned char	b5:1;
			unsigned char	b6:1;
			unsigned char	b7:1;
			unsigned char	b8:1;
			unsigned char	b9:1;
			unsigned char	b10:1;
			unsigned char	b11:1;
			unsigned char	b12:1;
			unsigned char	b13:1;
			unsigned char	b14:1;
			unsigned char	b15:1;
		}					bit;
	} 						word_t;
#endif	// __BIG
#pragma bit_order
#endif

#endif	// _TYPEDEFINE_H

// EOF typedefine.h

