
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		mask.h
*	Project:	YRotateIt-RX23T (three shunts)
*	Revision:	1.0
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	General use mask defines
*******************************************************************************/

#ifndef	_MASK_H
#define	_MASK_H

#include "typedefine.h"

#define SET0		((uint8_t)(0x01))
#define SET1		((uint8_t)(0x02))
#define SET2		((uint8_t)(0x04))
#define SET3		((uint8_t)(0x08))
#define SET4		((uint8_t)(0x10))
#define SET5		((uint8_t)(0x20))
#define SET6		((uint8_t)(0x40))
#define SET7		((uint8_t)(0x80))

#define CLR0		((uint8_t)(0xFE))
#define CLR1		((uint8_t)(0xFD))
#define CLR2		((uint8_t)(0xFB))
#define CLR3		((uint8_t)(0xF7))
#define CLR4		((uint8_t)(0xEF))
#define CLR5		((uint8_t)(0xDF))
#define CLR6		((uint8_t)(0xBF))
#define CLR7		((uint8_t)(0x7F))

#define WSET0		((uint16_t)(0x0001))
#define WSET1		((uint16_t)(0x0002))
#define WSET2		((uint16_t)(0x0004))
#define WSET3		((uint16_t)(0x0008))
#define WSET4		((uint16_t)(0x0010))
#define WSET5		((uint16_t)(0x0020))
#define WSET6		((uint16_t)(0x0040))
#define WSET7		((uint16_t)(0x0080))
#define WSET8		((uint16_t)(0x0100))
#define WSET9		((uint16_t)(0x0200))
#define WSETA		((uint16_t)(0x0400))
#define WSETB		((uint16_t)(0x0800))
#define WSETC		((uint16_t)(0x1000))
#define WSETD		((uint16_t)(0x2000))
#define WSETE		((uint16_t)(0x4000))
#define WSETF		((uint16_t)(0x8000))

#define WCLR0		((uint16_t)(0xFFFE))
#define WCLR1		((uint16_t)(0xFFFD))
#define WCLR2		((uint16_t)(0xFFFB))
#define WCLR3		((uint16_t)(0xFFF7))
#define WCLR4		((uint16_t)(0xFFEF))
#define WCLR5		((uint16_t)(0xFFDF))
#define WCLR6		((uint16_t)(0xFFBF))
#define WCLR7		((uint16_t)(0xFF7F))
#define WCLR8		((uint16_t)(0xFEFF))
#define WCLR9		((uint16_t)(0xFDFF))
#define WCLRA		((uint16_t)(0xFBFF))
#define WCLRB		((uint16_t)(0xF7FF))
#define WCLRC		((uint16_t)(0xEFFF))
#define WCLRD		((uint16_t)(0xDFFF))
#define WCLRE		((uint16_t)(0xBFFF))
#define WCLRF		((uint16_t)(0x7FFF))

#endif	// _MASK_H

// EOF mask.h

