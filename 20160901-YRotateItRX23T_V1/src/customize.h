
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		customize.h
*	Project:	YRotateIt-RX23T (three shunts)
*	Revision:	1.0
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	Custom values definition
*******************************************************************************/

#ifndef _CUSTOMIZE_H
#define _CUSTOMIZE_H

//#include "typedefine.h"

// power stage selection (NOTE: review tables in motorcontrol.c when changing)
//#define EXTERNAL_POWER_STAGE	// comment if you are using internal power stage

// power stage related definitions
#ifdef EXTERNAL_POWER_STAGE
#include "pws_E6108A.h"
#else	// EXTERNAL_POWER_STAGE
#include "pws_E6132.h"
#endif	// EXTERNAL_POWER_STAGE

// eeprom (comment if not used)
//#define EEPROM_USED

// debug (comment after first control)
#define MACRO_DEBUG

// open loop debug (comment in final release)
// if not commented it can be enabled writing 55 in par. 0
#define DEBUG_OPENLOOP

// DC current vector setting debug mode (comment if not used)
// if not commented it can be enabled writing 66 in par. 0
//#define DCCUR_SETTING

// ADDITIONAL FEATURE: current PI manual tuning (comment if not used)
//#define CPI_TUN

// ADDITIONAL FEATURE: current PI automatic tuning (comment if not used)
//#define CPI_AUT

// ADDITIONAL FEATURE: motor parameters auto-identification (comment if not used)
//#define MOT_IDENT

// ADDITIONAL FEATURE: oscilloscope window in GUI management (comment if not used)
//#define OSC_WIN

// PI gains amplification original
//#define AMP_KPCUR			(     4.0f )
//#define UAMP_KICUR			(   128.0f )
//#define AMP_KICUR			((float32_t)(1.0 / UAMP_KICUR))	// 0.0078125f
//#define AMP_KPVEL			( 16384.0f )
//#define AMP_KIVEL			(  2048.0f )

#define AMP_KPCUR			(     4.0f )
#define UAMP_KICUR			(   128.0f )
#define AMP_KICUR			((float32_t)(1.0 / UAMP_KICUR))	// 0.0078125f
#define AMP_KPVEL			( 16384.0f )
#define AMP_KIVEL			(  2048.0f )

// parameters measurement units resolution
#define	SEC_RES				(  1000.0f )// time parameters expressed in sec/SEC_RES
#define	AMP_RES				(  1000.0f )// current parameters expressed in ampere/AMP_RES
#define	VOL_RES				(    10.0f )// voltage parameters expressed in volt/VOL_RES
#define	OHM_RES				(   100.0f )// resistance parameters expressed in ohm/OHM_RES
#define	HEN_RES				( 10000.0f )// inductance parameters expressed in henry/HEN_RES
#define	WEB_RES				( 10000.0f )// flux parameters expressed in weber/WEB_RES

/*
// motor default parameters (Nanotec DB42S03) for USB supply
#define SEL_OP_DEF      	(      0 )	//     0 <= X <=  32767	// operation selection code
#define RPM_MIN_DEF     	(    600 )	//     0 <= X <=   3000	// minimum speed [rpm]
#define RPM_MAX_DEF     	(   3500 )	//    60 <= X <=  30000	// maximum speed [rpm]
#define R_ACC_DEF       	(   3000 )	//     1 <= X <=  10000	// acceleration ramp [rpm/sec]
#define R_DEC_DEF       	(   3000 )	//     1 <= X <=  10000	// deceleration ramp [rpm/sec]
#define C_POLI_DEF      	(      4 )	//     1 <= X <=     24	// polar couples number
#define I_START_DEF     	(    200 )	//     0 <= X <=  30000	// start-up current (peak) [Amp/AMP_RES]
#define I_MAX_DEF       	(    300 )	//     0 <= X <=  30000	// maximum current (peak) [Amp/AMP_RES]
#define R_STA_DEF       	(     86 )	//     0 <= X <=  30000	// stator phase resistance [Ohm/OHM_RES]
#define L_SYN_DEF       	(     12 )	//     0 <= X <=  30000	// synchronous inductance [Hen/HEN_RES]
#define PM_FLX_DEF      	(     60 )	//     0 <= X <=  30000	// permanent magnet flux [Web/WEB_RES]
#define KP_CUR_DEF      	(     20 )	//     0 <= X <=  30000	// current control proportional gain
#define KI_CUR_DEF      	(     50 )	//     0 <= X <=  30000	// current control integral gain
#define KP_VEL_DEF      	(     50 )	//     0 <= X <=  30000	// speed control proportional gain
#define KI_VEL_DEF      	(    200 )	//     0 <= X <=  30000	// speed control integral gain
#define FB_GAIN_DEF     	(    300 )	//     0 <= X <=  30000	// flux amplitude feedback gain (exact. est.)
#define PHA_OFF_DEF     	(      0 )	//     0 <= X <=    359	// offset angle [deg]
#define SUP_TIM_DEF     	(   1000 )	//   100 <= X <=  30000	// speed rising time (0 -> min_speed) during start-up [Sec/SEC_RES]
#define FLX_FTAU_DEF    	(     80 )	//     8 <= X <=   1000	// flux est. filter time constant [Sec/SEC_RES] (approx. est.)
#define SAM_FRE_DEF     	(   8000 )	//  4000 <= X <=  14000	// sampling frequency [Hz]
#define F_RATIO_DEF     	(      2 )	//     1 <= X <=      4	// pwm_freq/sam_freq ratio
*/

// motor parameters default values include
//#include "defpar_DB42S03.h"			// Nanotec DB42S03 normal supply
#include "defpar.h"

// other definitions related to startup
#define STUP_CUR_RT_MS		(   200 )	// current rising time during startup alignment [ms]
#define STUP_CUR_FT_MS		(  1000 )	// d current falling time after startup [ms] (assuming id=i_start)
#define ISTART_PH			( 0.523598775f )	// pi/6

// rotation direction
//#define ROT_REV

// current offsets refresh when motor not driven
#define OFF_REFRESH

// modulation
//#define CENTRED							// centred modulation
#define CEN_POS_K			(  1.0f )	// 0<=CEN_POS_K<=1; 0 means clamped to halfper, 1 means centred at (halfper-0.5(halfper-dmin))

// compensations in modulation
//#define REFCOMP							// modulation compensation (if performed) is based on current references
//#define COMMRED							// reduction of commutations number when clamping modulation (can produce voltage distortion)

// overmodulation (adjust to obtain overmodulation)
#define K_OVERMOD			(  1.0f )	// 1 -> no overmod, 1.1547=2/sqrt(3) -> full hex sat, <1 to limit out vol
//#define K_OVERMOD				(  1.1547f )// 1 -> no overmod, 1.1547=2/sqrt(3) -> full hex sat, <1 to limit out vol

// other control parameters
#define K_MARGV_DQ			(   0.9f )	// max_d_volt/max_volt ratio

// speed PI error clamping
#define SPEEDPI_ERRCLAMP				// input to speed PI is clamped to MAXERR_RPM
#define MAXERR_RPM			( 300.0f )	// maximum error for ramp limitation and PI clamping

// DC bus voltage limit for ramp limitation
//#define VB_RAMP						// comment this to avoid ramp limitation due to vbus
#define K_RAMPLIM			(   0.9f )	// ramp limitation gain
#define HVBUS_LIM			((float32_t)(1.25 * VB_NOM_VOL))	// high level DC bus voltage limit [V]

// ramp clamp (comment if you don't want it)
//#define RAMP_CL0						// clamp if heavy speed error

// flux weakening
//#define FLUXWEAK						// comment to avoid flux weakening
#define K_MARGV_FW			(   0.9f )	// percent of available voltage at which FW begins
#define K_MARGI_FW			(   0.8f )	// max_d_cur/max_cur ratio
#define DCUR_DT				(   2.0f )	// d current variation rate [A/s] during FW

// trip control
//#define TRIP_CONTROL					// comment to avoid trip control
#define T_MARGL				(   0.5f )	// minimum speed trip (ratio of requested speed or of minimum speed)
#define T_MARGH				(   1.3f )	// maximum speed trip (ratio of requested speed or of maximum speed)
#define R_MARG				( 100.0f )	// further speed margin [rad/sec]
#define MAX_TRIP			( 200 )		// trip counter threshold

// speed low-pass filter cut-off frequency f0 (time constant will be tau=(1/w0)=(1/(2PIf0)))
#define	SPEED_LP_CUTOFF_HZ	( 1000.0f )	// cut-off frequency [Hz]

// flux estimation method selection
//#define APP_INT							// uncomment this if you prefer approximated integration

#endif	// _CUSTOMIZE_H

// EOF customize.h

