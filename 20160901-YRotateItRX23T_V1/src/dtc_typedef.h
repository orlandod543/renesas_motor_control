
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		dtc_typedef.h
*	Project:	YRotateIt-RX23T (three shunts)
*	Revision:	1.0
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	dtc transfer data structure definition (FULL ADDRESS)
*******************************************************************************/

#ifndef _DTC_TYPEDEF
#define _DTC_TYPEDEF

#pragma bit_order left
#pragma unpack

// WARNING: the structure definition is referred to FULL ADDRESS addressing mode!

#ifdef __BIG
// structure definition when BIG ENDIAN
typedef struct {
	union {
		unsigned long UL;					// assures correct alignment
		struct {
			union {
				unsigned char BYTE;
				struct {
					unsigned char MD:2;		// B7, B6 Transfer Mode Select
					unsigned char SZ:2;		// B5, B4 Data Transfer Size
					unsigned char SM:2;		// B3, B2 Transfer Source Address Addressing Mode
					unsigned char dmy:2;	// B1, B0 reserved
				} BIT;
			} MRA; 							// DTC mode register A
			union {
				unsigned char BYTE;
				struct {
					unsigned char CHNE:1;	// B7 Chain Transfer Enable
					unsigned char CHNS:1;	// B6 Chain Transfer Select
					unsigned char DISEL:1;	// B5 Interrupt Select
					unsigned char DTS:1;	// B4 Transfer Mode Select
					unsigned char DM:2;		// B3, B2 Transfer Destination Address Addressing Mode
					unsigned char dmy:2;	// B1, B0 reserved
				} BIT;
			} MRB; 							// DTC mode register B
			unsigned short dmy;
		} REG;
	} DW0;
	unsigned long SAR; 						// DTC source address register
	unsigned long DAR; 						// DTC destination address register
	union {
		unsigned long UL;					// assures correct alignment
		struct {
			unsigned short CRA; 			// DTC transfer count register A
			unsigned short CRB; 			// DTC transfer count register B
		} REG;
	} DW3;
} st_DTC_TransData;
#else	// __BIG
// structure definition when LITTLE ENDIAN
typedef struct {
	union {
		unsigned long UL;					// assures correct alignment
		struct {
			unsigned short dmy;
			union {
				unsigned char BYTE;
				struct {
					unsigned char CHNE:1;	// B7 Chain Transfer Enable
					unsigned char CHNS:1;	// B6 Chain Transfer Select
					unsigned char DISEL:1;	// B5 Interrupt Select
					unsigned char DTS:1;	// B4 Transfer Mode Select
					unsigned char DM:2;		// B3, B2 Transfer Destination Address Addressing Mode
					unsigned char dmy:2;	// B1, B0 reserved
				} BIT;
			} MRB; 							// DTC mode register B
			union {
				unsigned char BYTE;
				struct {
					unsigned char MD:2;		// B7, B6 Transfer Mode Select
					unsigned char SZ:2;		// B5, B4 Data Transfer Size
					unsigned char SM:2;		// B3, B2 Transfer Source Address Addressing Mode
					unsigned char dmy:2;	// B1, B0 reserved
				} BIT;
			} MRA; 							// DTC mode register A
		} REG;
	} DW0;
	unsigned long SAR; 						// DTC source address register
	unsigned long DAR; 						// DTC destination address register
	union {
		unsigned long UL;					// assures correct alignment
		struct {
			unsigned short CRB; 			// DTC transfer count register B
			unsigned short CRA; 			// DTC transfer count register A
		} REG;
	} DW3;
} st_DTC_TransData;
#endif	// __BIG

#pragma bit_order
#pragma packoption

#endif	// _DTC_TYPEDEF

// EOF dtc_typedef.h

