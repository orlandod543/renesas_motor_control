
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		const_def.h
*	Project:	YRotateIt-RX23T (three shunts)
*	Revision:	1.0
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	Internal constants definition
*******************************************************************************/

#ifndef _CONST_DEF_H
#define _CONST_DEF_H

#include "typedefine.h"
#include "customize.h"

// floating point numerical constants
#ifndef F_CONST
#define F_CONST
#define PI				( 3.141592654 )		// 180
#define PISIXTH			(PI / 6.0)			// 030
#define PIFOURTHS		(PI / 4.0)			// 045
#define PITHIRDS		(PI / 3.0)			// 060
#define PIHALVES		(PI / 2.0)			// 090
#define TWOPITHIRDS		(PI * 2.0 / 3.0)	// 120
#define THREEPIFOURTHS	(PI * 3.0 / 4.0)	// 135
#define FIVEPISIXTH		(PI * 5.0 / 6.0)	// 150
#define SEVENPISIXTH	(PI * 7.0 / 6.0)	// 210
#define FIVEPIFOURTHS	(PI * 5.0 / 4.0)	// 225
#define FOURPITHIRDS	(PI * 4.0 / 3.0)	// 240
#define THREEPIHALVES	(PI * 3.0 / 2.0)	// 270
#define FIVEPITHIRDS	(PI * 5.0 / 3.0)	// 300
#define SEVENPIFOURTHS	(PI * 7.0 / 4.0)	// 315
#define ELEVENPISIXTH	(PI * 11.0 / 6.0)	// 330
#define TWOPI			(PI * 2.0)			// 360
#define	ONETHIRD		(1.0 / 3.0)
#define	TWOTHIRDS		(2.0 / 3.0)
#define	FOURTHIRDS		(4.0 / 3.0)
#define	SQRT2			( 1.414213562 )
#define	SQRT3			( 1.732050808 )
#define SQRT2D2			(SQRT2 / 2.0)		// 1/SQRT(2)
#define SQRT3D2			(SQRT3 / 2.0)
#define SQRT3D3			(SQRT3 / 3.0)		// 1/SQRT(3)
#define FPI				((float32_t) PI)
#define FPISIXTH		((float32_t) PISIXTH)
#define FPIFOURTHS		((float32_t) PIFOURTHS)
#define FPITHIRDS		((float32_t) PITHIRDS)
#define FPIHALVES		((float32_t) PIHALVES)
#define FTWOPITHIRDS	((float32_t) TWOPITHIRDS)
#define FTHREEPIFOURTHS	((float32_t) THREEPIFOURTHS)
#define FFIVEPISIXTH	((float32_t) FIVEPISIXTH)
#define FSEVENPISIXTH	((float32_t) SEVENPISIXTH)
#define FFIVEPIFOURTHS	((float32_t) FIVEPIFOURTHS)
#define FFOURPITHIRDS	((float32_t) FOURPITHIRDS)
#define FTHREEPIHALVES	((float32_t) THREEPIHALVES)
#define FFIVEPITHIRDS	((float32_t) FIVEPITHIRDS)
#define FSEVENPIFOURTHS	((float32_t) SEVENPIFOURTHS)
#define FELEVENPISIXTH	((float32_t) ELEVENPISIXTH)
#define FTWOPI			((float32_t) TWOPI)
#define	FONETHIRD		((float32_t) ONETHIRD)
#define	FTWOTHIRDS		((float32_t) TWOTHIRDS)
#define	FFOURTHIRDS		((float32_t) FOURTHIRDS)
#define	FSQRT2			((float32_t) SQRT2)
#define	FSQRT3			((float32_t) SQRT3)
#define FSQRT2D2		((float32_t) SQRT2D2)
#define FSQRT3D2		((float32_t) SQRT3D2)
#define FSQRT3D3		((float32_t) SQRT3D3)
#endif	// F_CONST

// base timings
#define MTU_FRE_MHZ		(    40.0 )	// MTU frequency in MHz
#define	MLOOP_FRE_HZ	(   400.0 )	// desired main loop frequency in Hz
#define	MIN_SAM_FRE_HZ	(  4000   )	// minimum sampling frequency in Hz
#define	MAX_SAM_FRE_HZ	( 16000   )	// maximum sampling frequency in Hz
#define MIN_FRE_RATIO	(     1   )	// minimum frequency ratio (pwm_fre/sam_fre)
#define MAX_FRE_RATIO	(     4   )	// maximum frequency ratio (pwm_fre/sam_fre)

#define MTU_FRE_KHZ		(1000.0 * MTU_FRE_MHZ)
#define MTU_FRE_HZ		(1000.0 * MTU_FRE_KHZ)
#define MLOOPS_1MIN		(60 * MLOOP_FRE_HZ)

#define SAM_FRE_MIN		MIN_SAM_FRE_HZ
#define SAM_FRE_MAX		MAX_SAM_FRE_HZ
#define F_RATIO_MIN		MIN_FRE_RATIO
#define F_RATIO_MAX		MAX_FRE_RATIO

// other pwm parameters
#if (25 >= DEADTIM_NS)	// 25=(1000000000/MTU_FRE_HZ)
#define DEADTIM			( 1 )
#else	// if (25 >= DEADTIM_NS)
#define DEADTIM			((uint16_t)(MTU_FRE_HZ * (DEADTIM_NS / 1000000000.0)))
#endif	// if (25 >= DEADTIM_NS)

// duty cycle and modulation restrictions
#define MIN_TON_LOW		((uint16_t)(MTU_FRE_HZ * (MIN_TON_LOW_NS / 1000000000.0)))
#define DMIN			( MIN_TON_LOW )
#define	VBMIN			( 10.0f )
#define SH_NRM			( 14 )

// current A/D conversion trigger delay respect pwm trough
#define CON_DELAY		((uint16_t)(MTU_FRE_HZ * (CON_DELAY_NS / 1000000000.0)))

// 12 bit A/D converter voltage and range
#define A12VCC			(    5.0 )	// A/D Range V
#define AD12FS			( 4096.0 )	// A/D full scale (bit)

// output currents A/D conversion (the reading is referred to AVCC/2, so half the range is used)
#define RAV_AD			(1.0 / (RSHUNT_OHM * RSGAIN))	// peak real_ampere/pin_volt ratio at the input pin

// voltages (bus and output) A/D conversion (the reading is referred to 0, so all the range is used)
#define RVV_AD			((RVBUSH_OHM + RVBUSL_OHM) / RVBUSL_OHM)	// peak real_volt/pin_volt ratio at the input pin

// currents
#define KADI			((float32_t)(RAV_AD * A12VCC / AD12FS))

// voltages
#define KADV			((float32_t)(RVV_AD * A12VCC / AD12FS))

// time, speed etc. conversion constants
#define KFO				((float32_t)(TWOPI))		// Omega[rad/sec]=KFO*Freq[Hertz]
#define KOF				((float32_t)(1.0 / TWOPI))	// Freq[Hertz]=KOF*Omega[rad/sec]
#define KRPMO			((float32_t)(TWOPI / 60.0))	// Omega[rad/sec]=KRPMO*velocita[rpm]
#define KORPM			((float32_t)(60.0 / TWOPI))	// velocita[rpm]=KORPM*Omega[rad/sec]

// flux weakening
#define INTID_RAMP		((float32_t)(DCUR_DT / SAM_FRE_HZ))

// phase compensation (number of sampling periods)
#define K_COMPPH		( 1.5f )

// alarms
#define	EQP_ALL			( 1 )	// eeprom alarm code
#define	FAULT_ALL		( 2 )	// overcurrent hardware alarm code (POE)
#define	TRIP_ALL		( 3 )	// loss of phase alarm code

// interrupts priorities
#define OVC_INT_P		( 8 )	// overcurrent
#define MC_INT_P		( 6 )	// motor control
#define SCI_INT_P		( 4 )	// communication interface

#endif	// _CONST_DEF_H

// EOF const_def.h

