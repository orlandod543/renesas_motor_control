
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		hardware_setup.c
*	Project:	YRotateIt-RX23T (three shunts)
*	Revision:	1.0
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	Hardware setup file
*******************************************************************************/

#define _HARDWARE_SETUP_C		// insert define BEFORE including header files

#include "iodefine.h"

/* PIN DESCRIPTION YRotateIt-RX23T *********************************************
 1	P02	DA0		TP			O
 2	P00			TP			I
 3	VCL
 4	P01			EXT_SUP		I
 5	MD
 6	RES
 7	P37	XTAL
 8	VSS
 9	P36	EXTAL
10	VCC
11	PE2	NMI					I
12	PD7			TP			I
13	PD6			TP			O
14	PD5	RXD1	RXPC		I
15	PD4			TP			I
16	PD3	TXD1	TXPC		O
>
17	PB7			J10-4		I
18	PB6			J10-3		I
19	PB5			J10-2		I
20	VCC
21	PB4			LIM-OFF		O
22	VSS
23	PB3			HALL-U		I
24	PB2			HALL-V		I
25	PB1			HALL-W		I
26	PB0			LED3		O
27	PA3	MTIOC2A	TP			O
28	PA2	MTIOC2B	TP			O
29	P94			LED2		O
30	P93			KEY3		I
31	P92			KEY2		I
32	P91			KEY1		I
>
33	P76	MTIOC4D	WL			O
34	P75	MTIOC4C	VL			O
35	P74	MTIOC3D	UL			O
36	P73	MTIOC4B	WH			O
37	P72	MTIOC4A	VH			O
38	P71	MTIOC3B	UH			O
39	P70	POE0	OVC			I
40	P33 MTCLKA	ENCA		I
41	P32	MTCLKB	ENCB		I
42	VCC
43	P31 MTCLKC	ENCA-EXT	I
44	VSS
45	P30	MTCLKD	ENCB-EXT	I
46	P24	J10-1				I
47	P23	J10-9				I
48	P22	J10-10				I
>
49	P47	AN7		EX			I
50	P46	AN6		VW			I
51	P45	AN5		VV			I
52	P44	AN4		VU			I
53	P43	AN3		VB			I
54	P42	AN2		IW			I
55	P41	AN1		IV			I
56	P40	AN0		IU			I
57	AVCC0
58	VREFH0
59	VREFL0
60	AVSS0
61	P11	J10-7				I
62	P10	J10-8				I
63	PA5	EEP-SCL				O
64	PA4	EEP-SDA				I/O
*******************************************************************************/

void HardwareSetup(void)
{

	// NON-EXISTENT PORTS INIT *************************************************

	PORT0.PDR.BYTE = 0xF8;			// B7, .., B3 do not exist
	PORT1.PDR.BYTE = 0xFC;			// B7, .., B2 do not exist
	PORT2.PDR.BYTE = 0xE3;			// B7, B6, B5, B1, B0 do not exist
	PORT3.PDR.BYTE = 0x30;			// B5, B4 do not exist
	PORT4.PDR.BYTE = 0x00;
	PORT7.PDR.BYTE = 0x80;			// B7 does not exist
	PORT9.PDR.BYTE = 0xE1;			// B7, B6, B5, B0 do not exist
	PORTA.PDR.BYTE = 0xC3;			// B7, B6, B1, B0 do not exist
	PORTB.PDR.BYTE = 0x00;
	PORTD.PDR.BYTE = 0x07;			// B2, B1, B0 do not exist


	// CLOCK INIT **************************************************************

	SYSTEM.PRCR.WORD = 0xA503;		// disable write protection

	SYSTEM.MOFCR.BYTE = 0x20;		// main clock oscillator drive capability: 10 MHz crystal resonator
	SYSTEM.MOSCWTCR.BYTE = 0x06;	// wait time for stabilization: 32768 cycles (8.192 ms)
	SYSTEM.MOSCCR.BYTE = 0x00;		// main clock oscillator is operating
	while (1 != SYSTEM.OSCOVFSR.BIT.MOOVF) { /* Waits until MOOVF bit is set to 1 */ }
	SYSTEM.PLLCR.WORD = 0x0700;		// PLL division ratio and multiplication factor: no division, multiply-by-4
	SYSTEM.PLLCR2.BYTE = 0x00;		// PLL is operating
	while (1 != SYSTEM.OSCOVFSR.BIT.PLOVF) { /* Waits until PLOVF bit is set to 1 */ }
	while (0 != SYSTEM.OPCCR.BIT.OPCMTSF) { /* Check that operation power control mode transition is completed */ }
	SYSTEM.OPCCR.BYTE = 0x00;		// 00 -> high speed operating mode
	while (0 != SYSTEM.OPCCR.BIT.OPCMTSF) { /* Confirm that the operation power control mode transition completed */ }
	SYSTEM.MEMWAIT.BYTE = 0x01;		// 01 -> insert a wait cycle in ROM access
	while (0x01 != SYSTEM.MEMWAIT.BYTE) { /* Confirm that the written value can be read correctly */ }
	SYSTEM.SCKCR.LONG = 0x10030000;	// ICLK, PCLKA, PCLKB, PCLKD: no division, FCLK: divide-by-2
	while (0x10030000 != SYSTEM.SCKCR.LONG) { /* Confirm that the written value can be read correctly */ }
	SYSTEM.SCKCR3.WORD = 0x0400;	// internal clock source: PLL clock
	while (0x0400 != SYSTEM.SCKCR3.WORD) { /* Confirm that the written value can be read correctly */ }

	SYSTEM.PRCR.WORD = 0xA500;		// enable write protection


	// PIN FUNCTION SELECTION **************************************************

/* will be done when initializing the peripherals

	MPC.PWPR.BIT.B0WI = 0;			// enable writing to bit PFSWE
	MPC.PWPR.BIT.PFSWE = 1;			// enable writing to PnmPFS

	MPC.P33PFS.BYTE = 0x02;			// pin40 P33 ENCA I -> MTCLKA
	MPC.P32PFS.BYTE = 0x02;			// pin41 P32 ENCB I -> MTCLKB
	MPC.P31PFS.BYTE = 0x02;			// pin43 P31 ENCA-EXT I -> MTCLKC
	MPC.P30PFS.BYTE = 0x02;			// pin45 P30 ENCB-EXT I -> MTCLKD

	MPC.P47PFS.BYTE = 0x80;			// pin49 P47 EXT I -> AN7
	MPC.P46PFS.BYTE = 0x80;			// pin50 P46 VW I -> AN6
	MPC.P45PFS.BYTE = 0x80;			// pin51 P45 VV I -> AN5
	MPC.P44PFS.BYTE = 0x80;			// pin52 P44 VU I -> AN4
	MPC.P43PFS.BYTE = 0x80;			// pin53 P43 VB I -> AN3
	MPC.P42PFS.BYTE = 0x80;			// pin54 P42 IW I -> AN2
	MPC.P41PFS.BYTE = 0x80;			// pin55 P41 IV I -> AN1
	MPC.P40PFS.BYTE = 0x80;			// pin56 P40 IU I -> AN0

	MPC.P76PFS.BYTE = 0x01;			// pin33 P76 WL O -> MTIOC4D
	MPC.P75PFS.BYTE = 0x01;			// pin34 P75 VL O -> MTIOC4C
	MPC.P74PFS.BYTE = 0x01;			// pin35 P74 UL O -> MTIOC3D
	MPC.P73PFS.BYTE = 0x01;			// pin36 P73 WH O -> MTIOC4B
	MPC.P72PFS.BYTE = 0x01;			// pin37 P72 VH O -> MTIOC4A
	MPC.P71PFS.BYTE = 0x01;			// pin38 P71 UH O -> MTIOC3B
	MPC.P70PFS.BYTE = 0x07;			// pin39 P70 OVC I -> POE0

	MPC.PA3PFS.BYTE = 0x01;			// pin27 PA3 TP O -> MTIOC2A	
	MPC.PA2PFS.BYTE = 0x01;			// pin28 PA2 TP O -> MTIOC2B

	MPC.PD5PFS.BYTE = 0x0A;			// pin14 PD5 RXPC I -> RXD1
	MPC.PD3PFS.BYTE = 0x0A;			// pin16 PD3 TXPC O -> TXD1

	MPC.PWPR.BIT.PFSWE = 0;			// disable writing to PnmPFS
	MPC.PWPR.BIT.B0WI = 1;			// disable writing to bit PFSWE

*/

	// I/O PORTS INIT **********************************************************

	// PORT 0

	PORT0.PODR.BYTE = 0x04;			// port output register
	// B7, .., B3
	//PORT0.PODR.BIT.B2 = 1;		// pin01 P02 TP O DA0
	//PORT0.PODR.BIT.B1 = 0;		// pin04 P01 EXT_SUP I
	//PORT0.PODR.BIT.B0 = 0;		// pin02 P00 TP I

	PORT0.PDR.BYTE |= 0x04;			// port direction register
	// B7, .., B3
	//PORT0.PDR.BIT.B2 = 1;			// pin01 P02 TP O DA0
	//PORT0.PDR.BIT.B1 = 0;			// pin04 P01 EXT_SUP I
	//PORT0.PDR.BIT.B0 = 0;			// pin02 P00 TP I

	//PORT0.PIDR.BYTE 				// port input register
	// B7, .., B3
	//PORT0.PIDR.BIT.B2				// pin01 P02 TP O DA0
	//PORT0.PIDR.BIT.B1				// pin04 P01 EXT_SUP I
	//PORT0.PIDR.BIT.B0				// pin02 P00 TP I

	//PORT0.PMR.BYTE = 0x00;		// port mode register
	// B7, .., B3
	//PORT0.PMR.BIT.B2 = 1;			// pin01 P02 TP O DA0
	//PORT0.PMR.BIT.B1 = 0;			// pin04 P01 EXT_SUP I
	//PORT0.PMR.BIT.B0 = 0;			// pin02 P00 TP I

	//PORT0.PCR.BYTE = 0x00;		// pull-up control register
	// B7, .., B3
	//PORT0.PCR.BIT.B2 = 1;			// pin01 P02 TP O DA0
	//PORT0.PCR.BIT.B1 = 0;			// pin04 P01 EXT_SUP I
	//PORT0.PCR.BIT.B0 = 0;			// pin02 P00 TP I

	//PORT0.DSCR.BYTE = 0x00;		// drive capacity control register
	// B7, .., B3
	//PORT0.DSCR.BIT.B2 = 1;		// pin01 P02 TP O DA0
	//PORT0.DSCR.BIT.B1 = 0;		// pin04 P01 EXT_SUP I
	//PORT0.DSCR.BIT.B0 = 0;		// pin02 P00 TP I

	//PORT0.ODR0.BYTE = 0x00;		// open drain control register 0
	// B7, B6, B5
	//PORT0.ODR0.BIT.B4 = 1;		// pin01 P02 TP O DA0
	// B3
	//PORT0.ODR0.BIT.B2 = 0;		// pin04 P01 EXT_SUP I
	// B1
	//PORT0.ODR0.BIT.B0 = 0;		// pin02 P00 TP I


	// PORT 1

	//PORT1.PODR.BYTE = 0x00;		// port output register
	// B7, .., B2
	//PORT1.PODR.BIT.B1 = 0;		// pin61 P11 J10-07 I
	//PORT1.PODR.BIT.B0 = 0;		// pin62 P10 J10-08 I

	//PORT1.PDR.BYTE |= 0x00;		// port direction register
	// B7, .., B2
	//PORT1.PDR.BIT.B1 = 0;			// pin61 P11 J10-07 I
	//PORT1.PDR.BIT.B0 = 0;			// pin62 P10 J10-08 I

	//PORT1.PIDR.BYTE				// port input register
	// B7, .., B2
	//PORT1.PIDR.BIT.B1				// pin61 P11 J10-07 I
	//PORT1.PIDR.BIT.B0				// pin62 P10 J10-08 I

	//PORT1.PMR.BYTE |= 0x00;		// port mode register
	// B7, .., B2
	//PORT1.PMR.BIT.B1 = 0;			// pin61 P11 J10-07 I
	//PORT1.PMR.BIT.B0 = 0;			// pin62 P10 J10-08 I

	//PORT1.PCR.BYTE |= 0x00;		// pull-up control register
	// B7, .., B2
	//PORT1.PCR.BIT.B1 = 0;			// pin61 P11 J10-07 I
	//PORT1.PCR.BIT.B0 = 0;			// pin62 P10 J10-08 I

	//PORT1.DSCR.BYTE |= 0x00;		// drive capacity control register
	// B7, .., B2
	//PORT1.DSCR.BIT.B1 = 0;		// pin61 P11 J10-07 I
	//PORT1.DSCR.BIT.B0 = 0;		// pin62 P10 J10-08 I

	//PORT1.ODR0.BYTE = 0x00;		// open drain control register 0
	// B7, .., B3
	//PORT1.ODR0.BIT.B2 = 0;		// pin61 P11 J10-07 I
	// B1
	//PORT1.ODR0.BIT.B0 = 0;		// pin62 P10 J10-08 I


	// PORT 2

	//PORT2.PODR.BYTE = 0x00;		// port output register
	// B7, B6, B5
	//PORT2.PODR.BIT.B4 = 0;		// pin46 P24 J10-01 I
	//PORT2.PODR.BIT.B3 = 0;		// pin47 P23 J10-09 I
	//PORT2.PODR.BIT.B2 = 0;		// pin48 P22 J10-10 I
	// B1, B0

	//PORT2.PDR.BYTE |= 0x00;		// port direction register
	// B7, B6, B5
	//PORT2.PDR.BIT.B4 = 0;			// pin46 P24 J10-01 I
	//PORT2.PDR.BIT.B3 = 0;			// pin47 P23 J10-09 I
	//PORT2.PDR.BIT.B2 = 0;			// pin48 P22 J10-10 I
	// B1, B0

	//PORT2.PIDR.BYTE				// port input register
	// B7, B6, B5
	//PORT2.PIDR.BIT.B4				// pin46 P24 J10-01 I
	//PORT2.PIDR.BIT.B3				// pin47 P23 J10-09 I
	//PORT2.PIDR.BIT.B2				// pin48 P22 J10-10 I
	// B1, B0

	//PORT2.PMR.BYTE = 0x00;		// port mode register
	// B7, B6, B5
	//PORT2.PMR.BIT.B4 = 0;			// pin46 P24 J10-01 I
	//PORT2.PMR.BIT.B3 = 0;			// pin47 P23 J10-09 I
	//PORT2.PMR.BIT.B2 = 0;			// pin48 P22 J10-10 I
	// B1, B0

	//PORT2.PCR.BYTE = 0x00;		// pull-up control register
	// B7, B6, B5
	//PORT2.PCR.BIT.B4 = 0;			// pin46 P24 J10-01 I
	//PORT2.PCR.BIT.B3 = 0;			// pin47 P23 J10-09 I
	//PORT2.PCR.BIT.B2 = 0;			// pin48 P22 J10-10 I
	// B1, B0

	//PORT2.DSCR.BYTE = 0x00;		// drive capacity control register
	// B7, B6, B5
	//PORT2.DSCR.BIT.B4 = 0;		// pin46 P24 J10-01 I
	//PORT2.DSCR.BIT.B3 = 0;		// pin47 P23 J10-09 I
	//PORT2.DSCR.BIT.B2 = 0;		// pin48 P22 J10-10 I
	// B1, B0

	//PORT2.ODR1.BYTE = 0x00;		// open drain control register 1
	// B7, .., B1
	//PORT2.ODR1.BIT.B0 = 0;		// pin46 P24 J10-01 I

	//PORT2.ODR0.BYTE = 0x00;		// open drain control register 0
	// B7
	//PORT2.ODR0.BIT.B6 = 0;		// pin47 P23 J10-09 I
	// B5
	//PORT2.ODR0.BIT.B4 = 0;		// pin48 P22 J10-10 I
	// B3, .., B0


	// PORT 3

	//PORT3.PODR.BYTE = 0x00;		// port output register
	//PORT3.PODR.BIT.B7 = 0;		// pin07 P37 XTAL
	//PORT3.PODR.BIT.B6 = 0;		// pin09 P36 EXTAL
	// B5, B4
	//PORT3.PODR.BIT.B3 = 0;		// pin40 P33 ENCA I MTCLKA
	//PORT3.PODR.BIT.B2 = 0;		// pin41 P32 ENCB I MTCLKB
	//PORT3.PODR.BIT.B1 = 0;		// pin43 P31 ENCA-EXT I MTCLKC
	//PORT3.PODR.BIT.B0 = 0;		// pin45 P30 ENCB-EXT I MTCLKD

	//PORT3.PDR.BYTE |= 0x00;		// port direction register
	//PORT3.PDR.BIT.B7 = 0;			// pin07 P37 XTAL
	//PORT3.PDR.BIT.B6 = 0;			// pin09 P36 EXTAL
	// B5, B4
	//PORT3.PDR.BIT.B3 = 0;			// pin40 P33 ENCA I MTCLKA
	//PORT3.PDR.BIT.B2 = 0;			// pin41 P32 ENCB I MTCLKB
	//PORT3.PDR.BIT.B1 = 0;			// pin43 P31 ENCA-EXT I MTCLKC
	//PORT3.PDR.BIT.B0 = 0;			// pin45 P30 ENCB-EXT I MTCLKD

	//PORT3.PIDR.BYTE				// port input register
	//PORT3.PIDR.BIT.B7				// pin07 P37 XTAL
	//PORT3.PIDR.BIT.B6				// pin09 P36 EXTAL
	// B5, B4
	//PORT3.PIDR.BIT.B3				// pin40 P33 ENCA I MTCLKA
	//PORT3.PIDR.BIT.B2				// pin41 P32 ENCB I MTCLKB
	//PORT3.PIDR.BIT.B1				// pin43 P31 ENCA-EXT I MTCLKC
	//PORT3.PIDR.BIT.B0				// pin45 P30 ENCB-EXT I MTCLKD

	//PORT3.PMR.BYTE = 0x00;		// port mode register
	//PORT3.PMR.BIT.B7 = 0;			// pin07 P37 XTAL
	//PORT3.PMR.BIT.B6 = 0;			// pin09 P36 EXTAL
	// B5, B4
	//PORT3.PMR.BIT.B3 = 0;			// pin40 P33 ENCA I MTCLKA
	//PORT3.PMR.BIT.B2 = 0;			// pin41 P32 ENCB I MTCLKB
	//PORT3.PMR.BIT.B1 = 0;			// pin43 P31 ENCA-EXT I MTCLKC
	//PORT3.PMR.BIT.B0 = 0;			// pin45 P30 ENCB-EXT I MTCLKD

	//PORT3.PCR.BYTE = 0x00;		// pull-up control register
	//PORT3.PCR.BIT.B7 = 0;			// pin07 P37 XTAL
	//PORT3.PCR.BIT.B6 = 0;			// pin09 P36 EXTAL
	// B5, B4
	//PORT3.PCR.BIT.B3 = 0;			// pin40 P33 ENCA I MTCLKA
	//PORT3.PCR.BIT.B2 = 0;			// pin41 P32 ENCB I MTCLKB
	//PORT3.PCR.BIT.B1 = 0;			// pin43 P31 ENCA-EXT I MTCLKC
	//PORT3.PCR.BIT.B0 = 0;			// pin45 P30 ENCB-EXT I MTCLKD

	//PORT3.DSCR.BYTE = 0x00;		// drive capacity control register
	//PORT3.DSCR.BIT.B7 = 0;		// pin07 P37 XTAL
	//PORT3.DSCR.BIT.B6 = 0;		// pin09 P36 EXTAL
	// B5, B4
	//PORT3.DSCR.BIT.B3 = 0;		// pin40 P33 ENCA I MTCLKA
	//PORT3.DSCR.BIT.B2 = 0;		// pin41 P32 ENCB I MTCLKB
	//PORT3.DSCR.BIT.B1 = 0;		// pin43 P31 ENCA-EXT I MTCLKC
	//PORT3.DSCR.BIT.B0 = 0;		// pin45 P30 ENCB-EXT I MTCLKD

	//PORT3.ODR1.BYTE = 0x00;		// open drain control register 1
	//B7
	//PORT3.ODR1.BIT.B6 = 0;		// pin07 P37 XTAL
	// B5
	//PORT3.ODR1.BIT.B4 = 0;		// pin09 P36 EXTAL
	//B3, .., B0

	//PORT3.ODR0.BYTE = 0x00;		// open drain control register 0
	//B7
	//PORT3.ODR0.BIT.B6 = 0;		// pin40 P33 ENCA I MTCLKA
	// B5
	//PORT3.ODR0.BIT.B4 = 0;		// pin41 P32 ENCB I MTCLKB
	//B3
	//PORT3.ODR0.BIT.B2 = 0;		// pin43 P31 ENCA-EXT I MTCLKC
	//B1
	//PORT3.ODR0.BIT.B0 = 0;		// pin45 P30 ENCB-EXT I MTCLKD


	// PORT 4

	//PORT4.PODR.BYTE = 0x00;		// port output register
	//PORT4.PODR.BIT.B7 = 0;		// pin49 P47 EX I AN7
	//PORT4.PODR.BIT.B6 = 0;		// pin50 P46 VW I AN6
	//PORT4.PODR.BIT.B5 = 0;		// pin51 P45 VV I AN5
	//PORT4.PODR.BIT.B4 = 0;		// pin52 P44 VU I AN4
	//PORT4.PODR.BIT.B3 = 0;		// pin53 P43 VB I AN3
	//PORT4.PODR.BIT.B2 = 0;		// pin54 P42 IW I AN2
	//PORT4.PODR.BIT.B1 = 0;		// pin55 P41 IV I AN1
	//PORT4.PODR.BIT.B0 = 0;		// pin56 P40 IU I AN0

	//PORT4.PDR.BYTE |= 0x00;		// port direction register
	//PORT4.PDR.BIT.B7 = 0;			// pin49 P47 EX I AN7
	//PORT4.PDR.BIT.B6 = 0;			// pin50 P46 VW I AN6
	//PORT4.PDR.BIT.B5 = 0;			// pin51 P45 VV I AN5
	//PORT4.PDR.BIT.B4 = 0;			// pin52 P44 VU I AN4
	//PORT4.PDR.BIT.B3 = 0;			// pin53 P43 VB I AN3
	//PORT4.PDR.BIT.B2 = 0;			// pin54 P42 IW I AN2
	//PORT4.PDR.BIT.B1 = 0;			// pin55 P41 IV I AN1
	//PORT4.PDR.BIT.B0 = 0;			// pin56 P40 IU I AN0

	//PORT4.PIDR.BYTE				// port input register
	//PORT4.PIDR.BIT.B7				// pin49 P47 EX I AN7
	//PORT4.PIDR.BIT.B6				// pin50 P46 VW I AN6
	//PORT4.PIDR.BIT.B5				// pin51 P45 VV I AN5
	//PORT4.PIDR.BIT.B4				// pin52 P44 VU I AN4
	//PORT4.PIDR.BIT.B3				// pin53 P43 VB I AN3
	//PORT4.PIDR.BIT.B2				// pin54 P42 IW I AN2
	//PORT4.PIDR.BIT.B1				// pin55 P41 IV I AN1
	//PORT4.PIDR.BIT.B0				// pin56 P40 IU I AN0

	//PORT4.PCR.BYTE = 0x00;		// pull-up control register
	//PORT4.PCR.BIT.B7 = 0;			// pin49 P47 EX I AN7
	//PORT4.PCR.BIT.B6 = 0;			// pin50 P46 VW I AN6
	//PORT4.PCR.BIT.B5 = 0;			// pin51 P45 VV I AN5
	//PORT4.PCR.BIT.B4 = 0;			// pin52 P44 VU I AN4
	//PORT4.PCR.BIT.B3 = 0;			// pin53 P43 VB I AN3
	//PORT4.PCR.BIT.B2 = 0;			// pin54 P42 IW I AN2
	//PORT4.PCR.BIT.B1 = 0;			// pin55 P41 IV I AN1
	//PORT4.PCR.BIT.B0 = 0;			// pin56 P40 IU I AN0


	// PORT 7
	// WARNING: PWM OUTPUTS ARE ALL HIGH ACTIVE!

	PORT7.PODR.BYTE = 0x00;			// port output register
	// B7
	//PORT7.PODR.BIT.B6 = 0;		// pin33 P76 WL O MTIOC4D
	//PORT7.PODR.BIT.B5 = 0;		// pin34 P75 VL O MTIOC4C
	//PORT7.PODR.BIT.B4 = 0;		// pin35 P74 UL O MTIOC3D
	//PORT7.PODR.BIT.B3 = 0;		// pin36 P73 WH O MTIOC4B
	//PORT7.PODR.BIT.B2 = 0;		// pin37 P72 VH O MTIOC4A
	//PORT7.PODR.BIT.B1 = 0;		// pin38 P71 UH O MTIOC3B
	//PORT7.PODR.BIT.B0 = 0;		// pin39 P70 OVC I POE0

	PORT7.PDR.BYTE |= 0x7E;			// port direction register
	// B7
	//PORT7.PDR.BIT.B6 = 1;			// pin33 P76 WL O MTIOC4D
	//PORT7.PDR.BIT.B5 = 1;			// pin34 P75 VL O MTIOC4C
	//PORT7.PDR.BIT.B4 = 1;			// pin35 P74 UL O MTIOC3D
	//PORT7.PDR.BIT.B3 = 1;			// pin36 P73 WH O MTIOC4B
	//PORT7.PDR.BIT.B2 = 1;			// pin37 P72 VH O MTIOC4A
	//PORT7.PDR.BIT.B1 = 1;			// pin38 P71 UH O MTIOC3B
	//PORT7.PDR.BIT.B0 = 0;			// pin39 P70 OVC I POE0

	PORT7.PODR.BYTE = 0x00;			// AGAIN FOR SAFETY

	//PORT7.PIDR.BYTE				// port input register
	// B7
	//PORT7.PIDR.BIT.B6				// pin33 P76 WL O MTIOC4D
	//PORT7.PIDR.BIT.B5				// pin34 P75 VL O MTIOC4C
	//PORT7.PIDR.BIT.B4				// pin35 P74 UL O MTIOC3D
	//PORT7.PIDR.BIT.B3				// pin36 P73 WH O MTIOC4B
	//PORT7.PIDR.BIT.B2				// pin37 P72 VH O MTIOC4A
	//PORT7.PIDR.BIT.B1				// pin38 P71 UH O MTIOC3B
	//PORT7.PIDR.BIT.B0				// pin39 P70 OVC I POE0

	//PORT7.PMR.BYTE = 0x00;		// port mode register
	// B7
	//PORT7.PMR.BIT.B6 = 0;			// pin33 P76 WL O MTIOC4D
	//PORT7.PMR.BIT.B5 = 0;			// pin34 P75 VL O MTIOC4C
	//PORT7.PMR.BIT.B4 = 0;			// pin35 P74 UL O MTIOC3D
	//PORT7.PMR.BIT.B3 = 0;			// pin36 P73 WH O MTIOC4B
	//PORT7.PMR.BIT.B2 = 0;			// pin37 P72 VH O MTIOC4A
	//PORT7.PMR.BIT.B1 = 0;			// pin38 P71 UH O MTIOC3B
	//PORT7.PMR.BIT.B0 = 0;			// pin39 P70 OVC I POE0

	//PORT7.PCR.BYTE = 0x00;		// pull-up control register
	// B7
	//PORT7.PCR.BIT.B6 = 0;			// pin33 P76 WL O MTIOC4D
	//PORT7.PCR.BIT.B5 = 0;			// pin34 P75 VL O MTIOC4C
	//PORT7.PCR.BIT.B4 = 0;			// pin35 P74 UL O MTIOC3D
	//PORT7.PCR.BIT.B3 = 0;			// pin36 P73 WH O MTIOC4B
	//PORT7.PCR.BIT.B2 = 0;			// pin37 P72 VH O MTIOC4A
	//PORT7.PCR.BIT.B1 = 0;			// pin38 P71 UH O MTIOC3B
	//PORT7.PCR.BIT.B0 = 0;			// pin39 P70 OVC I POE0

	//PORT7.DSCR.BYTE = 0x00;		// drive capacity control register
	// B7, .., B1
	//PORT7.DSCR.BIT.B0 = 0;		// pin39 P70 OVC I POE0

	//PORT7.ODR1.BYTE = 0x00;		// open drain control register 1
	//B7, B6, B5
	//PORT7.ODR1.BIT.B4 = 0;		// pin33 P76 WL O MTIOC4D
	//B3
	//PORT7.ODR1.BIT.B2 = 0;		// pin34 P75 VL O MTIOC4C
	//B1
	//PORT7.ODR1.BIT.B0 = 0;		// pin35 P74 UL O MTIOC3D

	//PORT7.ODR0.BYTE = 0x00;		// open drain control register 0
	//B7
	//PORT7.ODR0.BIT.B6 = 0;		// pin36 P73 WH O MTIOC4B
	// B5
	//PORT7.ODR0.BIT.B4 = 0;		// pin37 P72 VH O MTIOC4A
	//B3
	//PORT7.ODR0.BIT.B2 = 0;		// pin38 P71 UH O MTIOC3B
	//B1
	//PORT7.ODR0.BIT.B0 = 0;		// pin39 P70 OVC I POE0


	// PORT 9

	PORT9.PODR.BYTE = 0x10;			// port output register
	// B7, B6, B5
	//PORT9.PODR.BIT.B4 = 1;		// pin29 P94 LED2 O
	//PORT9.PODR.BIT.B3 = 0;		// pin30 P93 KEY3 I
	//PORT9.PODR.BIT.B2 = 0;		// pin31 P92 KEY2 I
	//PORT9.PODR.BIT.B1 = 0;		// pin32 P91 KEY1 I
	// B0

	PORT9.PDR.BYTE |= 0x10;			// port direction register
	// B7, B6, B5
	//PORT9.PDR.BIT.B4 = 1;			// pin29 P94 LED2 O
	//PORT9.PDR.BIT.B3 = 0;			// pin30 P93 KEY3 I
	//PORT9.PDR.BIT.B2 = 0;			// pin31 P92 KEY2 I
	//PORT9.PDR.BIT.B1 = 0;			// pin32 P91 KEY1 I
	// B0

	//PORT9.PIDR.BYTE				// port input register
	// B7, B6, B5
	//PORT9.PIDR.BIT.B4				// pin29 P94 LED2 O
	//PORT9.PIDR.BIT.B3				// pin30 P93 KEY3 I
	//PORT9.PIDR.BIT.B2				// pin31 P92 KEY2 I
	//PORT9.PIDR.BIT.B1				// pin32 P91 KEY1 I
	// B0

	//PORT9.PMR.BYTE = 0x00;		// port mode register
	// B7, B6, B5
	//PORT9.PMR.BIT.B4 = 0;			// pin29 P94 LED2 O
	//PORT9.PMR.BIT.B3 = 0;			// pin30 P93 KEY3 I
	//PORT9.PMR.BIT.B2 = 0;			// pin31 P92 KEY2 I
	//PORT9.PMR.BIT.B1 = 0;			// pin32 P91 KEY1 I
	// B0

	//PORT9.PCR.BYTE = 0x00;		// pull-up control register
	// B7, B6, B5
	//PORT9.PCR.BIT.B4 = 0;			// pin29 P94 LED2 O
	//PORT9.PCR.BIT.B3 = 0;			// pin30 P93 KEY3 I
	//PORT9.PCR.BIT.B2 = 0;			// pin31 P92 KEY2 I
	//PORT9.PCR.BIT.B1 = 0;			// pin32 P91 KEY1 I
	// B0

	PORT9.DSCR.BYTE = 0x10;			// drive capacity control register
	// B7, B6, B5
	//PORT9.DSCR.BIT.B4 = 1;		// pin29 P94 LED2 O
	//PORT9.DSCR.BIT.B3 = 0;		// pin30 P93 KEY3 I
	//PORT9.DSCR.BIT.B2 = 0;		// pin31 P92 KEY2 I
	//PORT9.DSCR.BIT.B1 = 0;		// pin32 P91 KEY1 I
	// B0

	//PORT9.ODR1.BYTE = 0x00;		// open drain control register 1
	//B7, .., B1
	//PORT9.ODR1.BIT.B0 = 0;		// pin29 P94 LED2 O

	//PORT9.ODR0.BYTE = 0x00;		// open drain control register 0
	//B7
	//PORT9.ODR0.BIT.B6 = 0;		// pin30 P93 KEY3 I
	// B5
	//PORT9.ODR0.BIT.B4 = 0;		// pin31 P92 KEY2 I
	//B3
	//PORT9.ODR0.BIT.B2 = 0;		// pin32 P91 KEY1 I
	//B1, B0

	
	// PORT A

	PORTA.PODR.BYTE = 0x20;			// port output register
	// B7, B6
	//PORTA.PODR.BIT.B5 = 1;		// pin63 PA5 EEP-SCL O
	//PORTA.PODR.BIT.B4 = 0;		// pin64 PA4 EEP-SDA I/O
	//PORTA.PODR.BIT.B3 = 0;		// pin27 PA3 TP O MTIOC2A
	//PORTA.PODR.BIT.B2 = 0;		// pin28 PA2 TP O MTIOC2B
	// B1, B0

	PORTA.PDR.BYTE |= 0x2C;			// port direction register
	// B7, B6
	//PORTA.PDR.BIT.B5 = 1;			// pin63 PA5 EEP-SCL O
	//PORTA.PDR.BIT.B4 = 0;			// pin64 PA4 EEP-SDA I/O
	//PORTA.PDR.BIT.B3 = 1;			// pin27 PA3 TP O MTIOC2A
	//PORTA.PDR.BIT.B2 = 1;			// pin28 PA2 TP O MTIOC2B
	// B1, B0

	//PORTA.PIDR.BYTE				// port input register
	// B7, B6
	//PORTA.PIDR.BIT.B5				// pin63 PA5 EEP-SCL O
	//PORTA.PIDR.BIT.B4				// pin64 PA4 EEP-SDA I/O
	//PORTA.PIDR.BIT.B3				// pin27 PA3 TP O MTIOC2A
	//PORTA.PIDR.BIT.B2				// pin28 PA2 TP O MTIOC2B
	// B1, B0

	//PORTA.PMR.BYTE = 0x00;		// port mode register
	// B7, B6
	//PORTA.PMR.BIT.B5 = 0;			// pin63 PA5 EEP-SCL O
	//PORTA.PMR.BIT.B4 = 0;			// pin64 PA4 EEP-SDA I/O
	//PORTA.PMR.BIT.B3 = 0;			// pin27 PA3 TP O MTIOC2A
	//PORTA.PMR.BIT.B2 = 0;			// pin28 PA2 TP O MTIOC2B
	// B1, B0

	//PORTA.PCR.BYTE = 0x00;		// pull-up control register
	// B7, B6
	//PORTA.PCR.BIT.B5 = 0;			// pin63 PA5 EEP-SCL O
	//PORTA.PCR.BIT.B4 = 0;			// pin64 PA4 EEP-SDA I/O
	//PORTA.PCR.BIT.B3 = 0;			// pin27 PA3 TP O MTIOC2A
	//PORTA.PCR.BIT.B2 = 0;			// pin28 PA2 TP O MTIOC2B
	// B1, B0

	//PORTA.DSCR.BYTE = 0x00;		// drive capacity control register
	// B7, B6
	//PORTA.DSCR.BIT.B5 = 0;		// pin63 PA5 EEP-SCL O
	//PORTA.DSCR.BIT.B4 = 0;		// pin64 PA4 EEP-SDA I/O
	//PORTA.DSCR.BIT.B3 = 0;		// pin27 PA3 TP O MTIOC2A
	//PORTA.DSCR.BIT.B2 = 0;		// pin28 PA2 TP O MTIOC2B
	// B1, B0

	PORTA.ODR1.BYTE = 0x01;			// open drain control register 1
	//B7, .., B3
	//PORTA.ODR1.BIT.B2 = 0;		// pin63 PA5 EEP-SCL O
	//B1
	//PORTA.ODR1.BIT.B0 = 1;		// pin64 PA4 EEP-SDA I/O

	//PORTA.ODR0.BYTE = 0x00;		// open drain control register 0
	//B7
	//PORTA.ODR0.BIT.B6 = 0;		// pin27 PA3 TP O MTIOC2A
	// B5
	//PORTA.ODR0.BIT.B4 = 0;		// pin28 PA2 TP O MTIOC2B
	//B3, .., B0


	// PORT B

	PORTB.PODR.BYTE = 0x01;			// port output register
	//PORTB.PODR.BIT.B7 = 0;		// pin17 PB7 J10-04 I
	//PORTB.PODR.BIT.B6 = 0;		// pin18 PB6 J10-03 I
	//PORTB.PODR.BIT.B5 = 0;		// pin19 PB5 J10-02 I
	//PORTB.PODR.BIT.B4 = 0;		// pin21 PB4 LIM-OFF O
	//PORTB.PODR.BIT.B3 = 0;		// pin23 PB3 HALL-U I
	//PORTB.PODR.BIT.B2 = 0;		// pin24 PB2 HALL-V I
	//PORTB.PODR.BIT.B1 = 0;		// pin25 PB1 HALL-W I
	//PORTB.PODR.BIT.B0 = 1;		// pin26 PB0 LED3 O

	PORTB.PDR.BYTE |= 0x11;			// port direction register
	//PORTB.PDR.BIT.B7 = 0;			// pin17 PB7 J10-04 I
	//PORTB.PDR.BIT.B6 = 0;			// pin18 PB6 J10-03 I
	//PORTB.PDR.BIT.B5 = 0;			// pin19 PB5 J10-02 I
	//PORTB.PDR.BIT.B4 = 1;			// pin21 PB4 LIM-OFF O
	//PORTB.PDR.BIT.B3 = 0;			// pin23 PB3 HALL-U I
	//PORTB.PDR.BIT.B2 = 0;			// pin24 PB2 HALL-V I
	//PORTB.PDR.BIT.B1 = 0;			// pin25 PB1 HALL-W I
	//PORTB.PDR.BIT.B0 = 1;			// pin26 PB0 LED3 O

	//PORTB.PIDR.BYTE				// port input register
	//PORTB.PIDR.BIT.B7				// pin17 PB7 J10-04 I
	//PORTB.PIDR.BIT.B6				// pin18 PB6 J10-03 I
	//PORTB.PIDR.BIT.B5				// pin19 PB5 J10-02 I
	//PORTB.PIDR.BIT.B4				// pin21 PB4 LIM-OFF O
	//PORTB.PIDR.BIT.B3				// pin23 PB3 HALL-U I
	//PORTB.PIDR.BIT.B2				// pin24 PB2 HALL-V I
	//PORTB.PIDR.BIT.B1				// pin25 PB1 HALL-W I
	//PORTB.PIDR.BIT.B0				// pin26 PB0 LED3 O

	//PORTB.PMR.BYTE = 0x00;		// port mode register
	//PORTB.PMR.BIT.B7 = 0;			// pin17 PB7 J10-04 I
	//PORTB.PMR.BIT.B6 = 0;			// pin18 PB6 J10-03 I
	//PORTB.PMR.BIT.B5 = 0;			// pin19 PB5 J10-02 I
	//PORTB.PMR.BIT.B4 = 0;			// pin21 PB4 LIM-OFF O
	//PORTB.PMR.BIT.B3 = 0;			// pin23 PB3 HALL-U I
	//PORTB.PMR.BIT.B2 = 0;			// pin24 PB2 HALL-V I
	//PORTB.PMR.BIT.B1 = 0;			// pin25 PB1 HALL-W I
	//PORTB.PMR.BIT.B0 = 0;			// pin26 PB0 LED3 O

	//PORTB.PCR.BYTE = 0x00;		// pull-up control register
	//PORTB.PCR.BIT.B7 = 0;			// pin17 PB7 J10-04 I
	//PORTB.PCR.BIT.B6 = 0;			// pin18 PB6 J10-03 I
	//PORTB.PCR.BIT.B5 = 0;			// pin19 PB5 J10-02 I
	//PORTB.PCR.BIT.B4 = 0;			// pin21 PB4 LIM-OFF O
	//PORTB.PCR.BIT.B3 = 0;			// pin23 PB3 HALL-U I
	//PORTB.PCR.BIT.B2 = 0;			// pin24 PB2 HALL-V I
	//PORTB.PCR.BIT.B1 = 0;			// pin25 PB1 HALL-W I
	//PORTB.PCR.BIT.B0 = 0;			// pin26 PB0 LED3 O

	PORTB.DSCR.BYTE = 0x01;			// drive capacity control register
	//PORTB.DSCR.BIT.B7 = 0;		// pin17 PB7 J10-04 I
	//PORTB.DSCR.BIT.B6 = 0;		// pin18 PB6 J10-03 I
	//PORTB.DSCR.BIT.B5 = 0;		// pin19 PB5 J10-02 I
	//PORTB.DSCR.BIT.B4 = 0;		// pin21 PB4 LIM-OFF O
	//PORTB.DSCR.BIT.B3 = 0;		// pin23 PB3 HALL-U I
	//PORTB.DSCR.BIT.B2 = 0;		// pin24 PB2 HALL-V I
	//PORTB.DSCR.BIT.B1 = 0;		// pin25 PB1 HALL-W I
	//PORTB.DSCR.BIT.B0 = 1;		// pin26 PB0 LED3 O

	//PORTB.ODR1.BYTE = 0x00;		// open drain control register 1
	//B7
	//PORTB.ODR1.BIT.B6 = 0;		// pin17 PB7 J10-04 I
	//B5
	//PORTB.ODR1.BIT.B4 = 0;		// pin18 PB6 J10-03 I
	//B3
	//PORTB.ODR1.BIT.B2 = 0;		// pin19 PB5 J10-02 I
	//B1
	//PORTB.ODR1.BIT.B0 = 0;		// pin21 PB4 LIM-OFF O

	//PORTB.ODR0.BYTE = 0x00;		// open drain control register 0
	//B7
	//PORTB.ODR0.BIT.B6 = 0;		// pin23 PB3 HALL-U I
	// B5
	//PORTB.ODR0.BIT.B4 = 0;		// pin24 PB2 HALL-V I
	//B3
	//PORTB.ODR0.BIT.B2 = 0;		// pin25 PB1 HALL-W I
	//B1
	//PORTB.ODR0.BIT.B0 = 0;		// pin26 PB0 LED3 O


	// PORT D

	PORTD.PODR.BYTE = 0x08;			// port output register
	//PORTD.PODR.BIT.B7 = 0;		// pin12 PD7 TP I
	//PORTD.PODR.BIT.B6 = 0;		// pin13 PD6 TP O
	//PORTD.PODR.BIT.B5 = 0;		// pin14 PD5 RXPC I RXD1
	//PORTD.PODR.BIT.B4 = 0;		// pin15 PD4 TP I
	//PORTD.PODR.BIT.B3 = 1;		// pin16 PD3 TXPC O TXD1
	//B2, B1, B0

	PORTD.PDR.BYTE |= 0x48;			// port direction register
	//PORTD.PDR.BIT.B7 = 0;			// pin12 PD7 TP I
	//PORTD.PDR.BIT.B6 = 1;			// pin13 PD6 TP O
	//PORTD.PDR.BIT.B5 = 0;			// pin14 PD5 RXPC I RXD1
	//PORTD.PDR.BIT.B4 = 0;			// pin15 PD4 TP I
	//PORTD.PDR.BIT.B3 = 1;			// pin16 PD3 TXPC O TXD1
	//B2, B1, B0

	//PORTD.PIDR.BYTE				// port input register
	//PORTD.PIDR.BIT.B7				// pin12 PD7 TP I
	//PORTD.PIDR.BIT.B6				// pin13 PD6 TP O
	//PORTD.PIDR.BIT.B5				// pin14 PD5 RXPC I RXD1
	//PORTD.PIDR.BIT.B4				// pin15 PD4 TP I
	//PORTD.PIDR.BIT.B3				// pin16 PD3 TXPC O TXD1
	//B2, B1, B0

	//PORTD.PMR.BYTE = 0x00;		// port mode register
	//PORTD.PMR.BIT.B7 = 0;			// pin12 PD7 TP I
	//PORTD.PMR.BIT.B6 = 0;			// pin13 PD6 TP O
	//PORTD.PMR.BIT.B5 = 0;			// pin14 PD5 RXPC I RXD1
	//PORTD.PMR.BIT.B4 = 0;			// pin15 PD4 TP I
	//PORTD.PMR.BIT.B3 = 0;			// pin16 PD3 TXPC O TXD1
	//B2, B1, B0

	//PORTD.PCR.BYTE = 0x00;		// pull-up control register
	//PORTD.PCR.BIT.B7 = 0;			// pin12 PD7 TP I
	//PORTD.PCR.BIT.B6 = 0;			// pin13 PD6 TP O
	//PORTD.PCR.BIT.B5 = 0;			// pin14 PD5 RXPC I RXD1
	//PORTD.PCR.BIT.B4 = 0;			// pin15 PD4 TP I
	//PORTD.PCR.BIT.B3 = 0;			// pin16 PD3 TXPC O TXD1
	//B2, B1, B0

	//PORTD.DSCR.BYTE = 0x00;		// drive capacity control register
	//PORTD.DSCR.BIT.B7 = 0;		// pin12 PD7 TP I
	//PORTD.DSCR.BIT.B6 = 0;		// pin13 PD6 TP O
	//PORTD.DSCR.BIT.B5 = 0;		// pin14 PD5 RXPC I RXD1
	//PORTD.DSCR.BIT.B4 = 0;		// pin15 PD4 TP I
	//PORTD.DSCR.BIT.B3 = 0;		// pin16 PD3 TXPC O TXD1
	//B2, B1, B0

	//PORTD.ODR1.BYTE = 0x00;		// open drain control register 1
	//B7
	//PORTD.ODR1.BIT.B6 = 0;		// pin12 PD7 TP I
	//B5
	//PORTD.ODR1.BIT.B4 = 0;		// pin13 PD6 TP O
	//B3
	//PORTD.ODR1.BIT.B2 = 0;		// pin14 PD5 RXPC I RXD1
	//B1
	//PORTD.ODR1.BIT.B0 = 0;		// pin15 PD4 TP I

	//PORTD.ODR0.BYTE = 0x00;		// open drain control register 0
	//B7
	//PORTD.ODR0.BIT.B6 = 0;		// pin16 PD3 TXPC O TXD1
	// B5, .., B0


	// PORT E

	//PORTE.PIDR.BYTE				// port input register
	// B7, .., B3
	//PORTD.PIDR.BIT.B2				// pin11 PE2 NMI I
	// B1, B0

	//PORTE.PMR.BYTE = 0x00;		// port mode register
	// B7, .., B3
	//PORTD.PMR.BIT.B2 = 0;			// pin11 PE2 NMI I
	// B1, B0

}

// EOF hardware_setup.c

