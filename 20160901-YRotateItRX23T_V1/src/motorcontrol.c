
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		motorcontrol.c
*	Project:	YRotateIt-RX23T (three shunts)
*	Revision:	1.0
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	Motor control algorithm
*******************************************************************************/

#define _MOTORCONTROL_C		// insert define BEFORE including header files
#include "encoder.h"

#include "globdef.h"
#include "ges_eqp.h"
#include "userif.h"
#include "motorcontrol.h"
#include ".\YRotateItRX23T_Library\mcrplibf.h"
#ifdef CPI_AUT
#include ".\YRotateItRX23T_Library\PI_tun.h"
#endif	// CPI_AUT
#ifdef	MOT_IDENT
#include ".\YRotateItRX23T_Library\mot_ident.h"
#endif	// MOT_IDENT

// flags
#define	FREE00			(0 != flgx.bit.b0)
#define	ALIGNED			(0 != flgx.bit.b1)
#define	FREE02			(0 != flgx.bit.b2)
#define	RAMPLIM			(0 != flgx.bit.b3)
#define	DECEL			(0 != flgx.bit.b4)
#define	FREE05			(0 != flgx.bit.b5)
#define	STOPPED			(0 != flgx.bit.b6)
#define	STARTED			(0 != flgx.bit.b7)
#define	NOT_FREE00		(0 == flgx.bit.b0)
#define	NOT_ALIGNED		(0 == flgx.bit.b1)
#define	NOT_FREE02		(0 == flgx.bit.b2)
#define	NOT_RAMPLIM		(0 == flgx.bit.b3)
#define	NOT_DECEL		(0 == flgx.bit.b4)
#define	NOT_FREE05		(0 == flgx.bit.b5)
#define	NOT_STOPPED		(0 == flgx.bit.b6)
#define	NOT_STARTED		(0 == flgx.bit.b7)
#define	SET_FREE00		flgx.bit.b0 = 1;
#define	SET_ALIGNED		flgx.bit.b1 = 1;
#define	SET_FREE02		flgx.bit.b2 = 1;
#define	SET_RAMPLIM		flgx.bit.b3 = 1;
#define	SET_DECEL		flgx.bit.b4 = 1;
#define	SET_FREE05		flgx.bit.b5 = 1;
#define	SET_STOPPED		flgx.bit.b6 = 1;
#define	SET_STARTED		flgx.bit.b7 = 1;
#define	RES_FREE00		flgx.bit.b0 = 0;
#define	RES_ALIGNED		flgx.bit.b1 = 0;
#define	RES_FREE02		flgx.bit.b2 = 0;
#define	RES_RAMPLIM		flgx.bit.b3 = 0;
#define	RES_DECEL		flgx.bit.b4 = 0;
#define	RES_FREE05		flgx.bit.b5 = 0;
#define	RES_STOPPED		flgx.bit.b6 = 0;
#define	RES_STARTED		flgx.bit.b7 = 0;

// internal filters (main interrupt) time constants	[sec]
//#define VISU_KT			( 0.3f )	// visualization filters
//#define	VB_KT_FAST		( 0.002f )	// bus voltage filter fast
//#define	VB_KT_SLOW		( 0.2f )	// bus voltage filter slow
//#define	OMR_KT			( 0.05f )	// speed reference filter
//#define	OFF_KT			( 0.2f )	// A/D offsets filters
//#define	VR_KT			( 0.002f )	// voltage reference filter
#define VISU_KT			( 0.0f )	// visualization filters
#define	VB_KT_FAST		( 0.000f )	// bus voltage filter fast
#define	VB_KT_SLOW		( 0.0f )	// bus voltage filter slow
#define	OMR_KT			( 0.00f )	// speed reference filter
#define	OFF_KT			( 0.2f )	// A/D offsets filters
#define	VR_KT			( 0.000f )	// voltage reference filter
// state values for three shunts current reading
#define CR_NO			( 0 )
#define CR_UV			( 1 )
#define CR_VW			( 2 )
#define CR_WU			( 3 )

// speed ramp state machine
#define STOP_0			( 0 )
#define STOP_W			( 1 )
#define START_P			( 2 )
#define START_N			( 3 )
#define GO_P			( 4 )
#define GO_N			( 5 )

// internal variables
byte_t
	flgx,				// flags
	flgy;				// flags

uint16_t
	cnt_int,			// counter for main loop synchronization
	cr_status,			// status memory for current reading
	r_acc_ep,			// acceleration ramp (copy) [rpm/sec]
	r_dec_ep,			// deceleration ramp (copy) [rpm/sec]
	i_start_ep,			// startup current (copy) [A/AMP_DIV]
	i_max_ep,			// maximum current (copy) [A/AMP_DIV]
	r_sta_ep,			// stator resistance (copy) [Ohm/OHM_DIV]
	l_syn_ep,			// synchronous inductance (copy) [Henry/HEN_DIV]
	pm_flx_ep,			// permanent magnets flux (copy) [Weber/WEB_DIV]
	fb_gain_ep,			// flux amplitude feedback gain (copy)
	flx_ftau_ep,		// approx. flux est. filter time const tau (copy) [sec/SEC_DIV]
	sup_tim_ep,			// startup acceleration time (copy) [sec/SEC_DIV]
	kp_cur_ep,			// current PI proportional gain (copy)
	ki_cur_ep,			// current PI integral gain (copy)
	kp_vel_ep,			// speed PI proportional gain (copy)
	ki_vel_ep,			// speed PI integral gain (copy)
	ramp_status;		// speed ramp state machine status memory

int32_t
	startup_cnt;		// startup counter

int16_t
	trip_cnt,			// counter for trip alarm
	duty_u,				// u phase duty cycle
	duty_v,				// v phase duty cycle
	duty_w,				// w phase duty cycle
	vbus_ad,			// bus voltage A/D conversion value
	vref_ad;			// reference voltage A/D conversion value

float32_t
	ium_off,			// IU analog channel offset
	ivm_off,			// IV analog channel offset
	iwm_off,			// IW analog channel offset
	r_sta,				// stator resistance
	l_syn,				// synchronous inductance
	pm_flx,				// permanent magnets flux
	fb_gain,			// flux amplitude feedback gain
	flx_lpco_hz,		// approx. flux estimation filter cutoff frequency [Hz]
	c_poli,				// number of polar couples
	krpmocp,			// conversion constant between mechanical speed and electrical speed
	ukrpmocp,			// 1/krpmocp
	i_start,			// startup current (peak)
	delta_om,			// speed variation (during startup)
	is_rup,				// startup current increasing rate
	is_rdw,				// startup current decreasing rate
	vbus,				// bus voltage
	vbusf,				// bus voltage (filtered)
	vbus_minf,			// filtered available bus voltage (minimum value)
	vfmax,				// max phase voltage (star)
	//vdmax,				// maximum d voltage
	//vqmax,				// maximum q voltage
	i_max,				// maximum total current
	id_max,				// maximum d current
	iqmax,				// maximum q current
	vdc,				// applied d voltage
	vqc,				// applied q voltage
	vdm,				// d voltage (applied at previous step)
	vqm,				// q voltage (applied at previous step)
	vdmf,				// applied d voltage (filtered)
	vqmf,				// applied q voltage (filtered)
	vac,				// applied alpha voltage (star)
	vbc,				// applied beta voltage (star)
	vam,				// alpha voltage (applied at previous step)
	vbm,				// beta voltage (applied at previous step)
	vuc,				// applied u voltage (star)
	vvc,				// applied v voltage (star)
	vwc,				// applied w voltage (star)
	vmarg,				// voltage margin for flux weakening
	ium,				// u current (measured)
	ivm,				// v current (measured)
	iwm,				// w current (measured)
	iam,				// alpha current (measured)
	ibm,				// beta current (measured)
	idm,				// d current (measured)
	iqm,				// q current (measured)
	idmf,				// d current (filtered)
	iqmf,				// q current (filtered)
	is_ref,				// startup current reference
	idr,				// d current reference
	iqr,				// q current reference
	iar,				// alpha current reference
	ibr,				// beta current reference
	idint,				// d current error integral value
	iqint,				// q current error integral value
	iaint,				// a current error integral value
	ibint,				// b current error integral value
	idrvar,				// d current reference
	errint,				// speed error integral memory
	kp_cur,				// current PI proportional gain
	ki_cur,				// current PI integral gain
	kp_vel,				// speed PI proportional gain
	ki_vel,				// speed PI integral gain
	freq,				// electrical frequency [Hz/10]
	mec_rpm,			// mechanical speed [rpm]
	rpmrif_x,			// reference speed (ramp input) [rpm]
	rpmrif_y,			// reference speed (ramp output) [rpm]
	r_acc,				// acceleration ramp [rpm/main_loop_duration]
	r_dec,				// deceleration ramp [rpm/main_loop_duration]
#ifdef RAMP_CL0
	r_marg,				// useful in speed thresholds determination
#endif	// RAMP_CL0
	rpm_max,			// maximum speed [rpm]
	rpm_min,			// minimum speed [rpm]
	min_speed,			// minimum speed [rad/s]
	max_speed,			// maximum speed [rad/s]
	min_speed_trip,		// clamp val for minimum speed (for trip detection)
	max_speed_trip,		// clamp val for maximum speed (for trip detection)
	min_s_trip,			// minimum speed (for trip detection)
	max_s_trip,			// maximum speed (for trip detection)
	Speed_est,			// estimated speed
	omrif,				// reference speed
	omrif_abs,			// |reference speed|
	f_omrif,			// reference speed (filtered)
	omegae,				// electrical angular speed
	omf,				// electrical angular speed (filtered)
	maxerr,				// maximum speed error [rad/s]
	//amp_i,				// current vector amplitude (derived from iam, ibm)
	amp_v,				// voltage vector amplitude (derived from vam, vbm)
	//arg_i,				// current vector phase (derived from iam, ibm)
	//arg_v,				// voltage vector phase (derived from vam, vbm)
	f_amp_i,			// filtered current vector amplitude (derived from idmf, iqmf)
	f_arg_i,			// filtered current vector phase (derived from idmf, iqmf)
	f_amp_v,			// filtered voltage vector amplitude (derived from vdmf, vqmf)
	f_arg_v,			// filtered voltage vector phase (derived from vdmf, vqmf)
	delta_ph,			// phase variation (during startup)
	SystemPhase,		// rotating reference system angular position
	Phase_est,			// estimated electrical phase
	Phase_offset,		// optional phase offset
	FluxAmp,			// flux vector amplitude estimation
	FluxPhase,			// flux vector phase first estimation (not compensated)
	intid_rup,			// id integral variation ramp up in FW
	intid_rdw;			// id integral variation ramp dw in FW

// variables involved in sampling frequency adaptation
float32_t
	//pwm_per_s,			// true pwm period [s]
	pwm_fre_Hz,			// true pwm frequency [Hz]
	sam_per_s,			// true sampling period [s]
	sam_per_ms,			// true sampling period [ms]
	sam_fre_Hz,			// true sampling frequency [Hz]
	sam_fre_kHz,		// true sampling frequency [kHz]
	mloop_per_s,		// true main loop period [s]
	mloop_fre_Hz,		// true main loop frequency [Hz]
	comptime,			// phase compensation time
	uhalfper,			// 1/halfper
	k_vout,				// keeps into account modulation voltage losses
	visu_ukt,			// 1.0 / (1.0 + (VISU_KT / SAM_PER_S))
	vb_ukt_fast,		// 1.0 / (1.0 + (VB_KT_FAST / SAM_PER_S))
	vb_ukt_slow,		// 1.0 / (1.0 + (VB_KT_SLOW / SAM_PER_S))
	omr_ukt,			// 1.0 / (1.0 + (OMR_KT / SAM_PER_S))
	off_ukt,			// 1.0 / (1.0 + (OFF_KT / SAM_PER_S))
	vr_ukt;				// 1.0 / (1.0 + (VR_KT / SAM_PER_S))

// variables used in case of encoder
float
	ele_ang,				// electrical angle position				[rad]
	off_ang,				// electrical angle position offset			[rad]
	tele_ang,				// corrected electrical angle position		[rad]
	om_eme;					// electro-mechanical speed		
uint16_t
	fre_ratio,			// ratio between pwm frequency and sampling frequency
	int_skip,			// interrupt skipping register value
	halfper,			// half pwm period in MTU timer ticks
	pwm_per_int,		// pwm period in MTU timer ticks
	sam_per_int,		// sampling period in MTU timer ticks
	halfperdead,		// half pwm period plus deadtimes in MTU timer ticks
	num_int,			// number of control interrupts for each main loop
	centre_pos,			// pwm centre position in MTU timer ticks
	delmax;				// maximum delta between maximum duty cycle and minimum one

// alignment
#define CNT_MS( X )		((u32)(( X )*sam_fre_Hz/1000.0))	// n. of sam. periods corr. to X[ms]
#define STAB_TIME	CNT_MS(100)	// 1sec (16000@16kHz)

#define pwm_halfper_int	halfper
//#define kt				sam_fre_Hz
// end of variables involved in sampling frequency adjustment

#ifdef CPI_TUN
// current PI tuning
uint16_t
	pit_s,				// counter in tuning state machine
	pit_c;				// counter in tuning state machine
float32_t
	ratio,				// ratio between maximum current and current step amplitude
	cur_ampf;			// filtered measured current amplitude
#endif	// CPI_TUN

#ifdef CPI_AUT
// automatic current PI tuning
#define I_PIT_RATIO		( 0.8f )	// 80% of i_max
#define R_UP			( 0 )
#define R_MS			( 1 )
#define R_DW			( 2 )
#define R_ST			( 3 )
#define R_EN			( 4 )
float32_t
	v_req;				// required voltage
uint16_t
	pitun_status;		// state machine status
#endif	// CPI_AUT

// motor parameters identification
#ifdef MOT_IDENT
#define IDENT_SPEED_HZ	( 20.0f )
#define X_TIM1			((int32_t)(ID_TIM1_MS * sam_fre_kHz))	// current rising + alignment + stabilization
#define X_TIM2			((int32_t)(ID_TIM2_MS * sam_fre_kHz))	// first measurement step
#define X_TIM3			((int32_t)(ID_TIM3_MS * sam_fre_kHz))	// current falling + stabilization
#define X_TIM4			((int32_t)(ID_TIM4_MS * sam_fre_kHz))	// second measurement step
#define X_TIM5			((int32_t)(ID_TIM5_MS * sam_fre_kHz))	// speed rising + stabilization
#define X_TIM6			((int32_t)(ID_TIM6_MS * sam_fre_kHz))	// third measurement step
#define X_TIM7			((int32_t)(ID_TIM7_MS * sam_fre_kHz))	// current falling + stabilization
#define X_TIM8			((int32_t)(ID_TIM8_MS * sam_fre_kHz))	// fourth measurement step
#define X_TIM9			((int32_t)(ID_TIM9_MS * sam_fre_kHz))	// final wait
#define INI_IDENT		{x_cnt = 0; MC_Ident_IniVar(sam_fre_Hz);}
#define IDENT_ENDED		(X_TIM9 < x_cnt)
int32_t
	x_cnt;				// identification synchronization counter
float32_t
	iden_rs,			// identification stator resistance
	iden_ls,			// identification synchronous inductance
	iden_fl,			// identification permanent magnets flux
	iden_om,			// identification speed
	iden_or,			// identification speed ramp
	ial_1,				// identification current 1
	ial_2,				// identification current 2
	ial_3,				// identification current 3
	ial_r;				// identification current ramp
#endif	// MOT_IDENT

// oscilloscope window in GUI
#ifdef OSC_WIN
float32_t
	phmem;				// angular position memory
int16_t
	sample,				// measurement sample
	selmem,				// variable selection memory
	bcntm,				// counter memory
	bcnt0,				// counter
	bcnt1;				// counter
#endif	// OSC_WIN

#ifdef NOCOMP

// avoid any compensation in modulation
#ifdef REFCOMP
#undef REFCOMP
#endif	// REFCOMP

#else	// NOCOMP

// modulation compensation based on currents
#ifdef REFCOMP
float32_t
	iar,				// reference a current [internal current unit]
	ibr,				// reference b current [internal current unit]
	iur,				// iu reference [int]
	ivr,				// iv reference [int]
	iwr;				// iw reference [int]
#define IU				( iur )
#define IV				( ivr )
#define IW				( iwr )
#else	// REFCOMP
#define IU				( ium )
#define IV				( ivm )
#define IW				( iwm )
#endif	// REFCOMP

// inverter errors compensation
#define T_INL1_INMAX	((float32_t)(T_INL1_INDMAX * T_INL_DELTAIN))	// maximum input
const float32_t
	tab_inl1[T_INL1_DIM] =
	{
		0.0f,
		((float32_t)INL1_01),
		((float32_t)INL1_02),
		((float32_t)INL1_03),
		((float32_t)INL1_04),
		((float32_t)INL1_05),
		((float32_t)INL1_06),
		((float32_t)INL1_07),
		((float32_t)INL1_08),
		((float32_t)INL1_09),
		((float32_t)INL1_10),
		((float32_t)INL1_11),
		((float32_t)INL1_12),
		((float32_t)INL1_13),
		((float32_t)INL1_14),
		((float32_t)INL1_15)
	};
float32_t
	t_inl1[T_INL1_INDMAX + 1];		// saves calculations
#define MC_INL1( X )	(vbus * MC_INL1TabCal( X ))

#ifndef PSVDROP

#define MC_INL( X )		(0 <= ( X )? MC_INL1( X ): (-(MC_INL1(-( X )))))

#else	// PSVDROP

#define T_INL2_INMAX	((float32_t)(T_INL2_INDMAX * T_INL_DELTAIN))	// maximum input
const int16_t
	tab_inl2[T_INL2_DIM] =
	{
		0.0f,
		((float32_t)INL2_01),
		((float32_t)INL2_02),
		((float32_t)INL2_03),
		((float32_t)INL2_04),
		((float32_t)INL2_05),
		((float32_t)INL2_06),
		((float32_t)INL2_07),
		((float32_t)INL2_08),
		((float32_t)INL2_09),
		((float32_t)INL2_10),
		((float32_t)INL2_11),
		((float32_t)INL2_12),
		((float32_t)INL2_13),
		((float32_t)INL2_14),
		((float32_t)INL2_15)
	};
#define MC_INL2( X )	(MC_INL2TabCal( X ))

#define MC_INLU( X )	(MC_INL1( X ) + MC_INL2( X ))
#define MC_INL( X )		(0 <= ( X )? MC_INLU( X ): (-(MC_INLU(-( X )))))

#endif	// PSVDROP

#endif	// NOCOMP

#ifdef MACRO_DEBUG
float32_t
	p_mtu_fre_hz,		// MTU frequency in Hz
	p_deadtim,			// deadtimes
	p_min_ton_low,		// minimum on time of lower arms
	p_con_delay,		// conversion delay
	p_kfo,				// Omega[rad/sec]=KFO*Freq[Hertz]
	p_kof,				// Freq[Hertz]=KOF*Omega[rad/sec]
	p_krpmo,			// Omega[rad/sec]=KRPMO*velocita[rpm]
	p_korpm,			// speed[rpm]=KORPM*Omega[rad/sec]
	p_kadi,				// current A/D conversion constant
	p_kadv;				// voltage A/D conversion constant
void const_debug(void)
{
	p_mtu_fre_hz = MTU_FRE_HZ;
	p_deadtim = DEADTIM;
	p_min_ton_low = MIN_TON_LOW;
	p_con_delay = CON_DELAY;
	p_kfo = KFO;
	p_kof = KOF;
	p_krpmo = KRPMO;
	p_korpm = KORPM;
	p_kadi = KADI;
	p_kadv = KADV;
}
#endif	// MACRO_DEBUG

// sampling frequency related constants calculation
/*******************************************************************************
Function:		MC_SetPwmConst
Description:	calculation of constants related to sampling (and pwm) freq.
Input:			desired sampling frequency in Hz sam_fre_Hz_req
				desired pwm_freq/sam_freq ratio fre_ratio_req
Output:			0 all OK, an error code if something wrong (clamp
				performed because limits exceeded)
Modifies:		many global variables related to sampling frequency and used as
				constants
*******************************************************************************/
uint16_t MC_SetPwmConst(uint16_t sam_fre_Hz_req, uint16_t fre_ratio_req)
{
	uint16_t retcode;

	retcode = 0;
	if(MIN_SAM_FRE_HZ > sam_fre_Hz_req)
	{
		sam_fre_Hz = MIN_SAM_FRE_HZ;
		retcode += 1;
	}
	else if(MAX_SAM_FRE_HZ < sam_fre_Hz_req)
	{
		sam_fre_Hz = MAX_SAM_FRE_HZ;
		retcode += 3;
	}
	else
	{
		sam_fre_Hz = sam_fre_Hz_req;
	}
	if(MIN_FRE_RATIO > fre_ratio_req)
	{
		fre_ratio = MIN_FRE_RATIO;
		retcode += 5;
	}
	else if(MAX_FRE_RATIO < fre_ratio_req)
	{
		fre_ratio = MAX_FRE_RATIO;
		retcode += 7;
	}
	else
	{
		fre_ratio = fre_ratio_req;
	}

	if(1 == fre_ratio)
	{
		int_skip = 0;
	}
	else
	{
		int_skip = fre_ratio + 7;	// 8 (ena bit = 1) + (fre_ratio - 1) (number of skipped int)
	}

	// pwm and sampling period calc in internal timer ticks
	pwm_fre_Hz = fre_ratio * sam_fre_Hz;
	pwm_halfper_int = ((uint16_t)((0.5f * MTU_FRE_HZ) / pwm_fre_Hz));	// half pwm period in MTU ticks (int)
	pwm_per_int = pwm_halfper_int << 1;								// pwm period in MTU ticks (int)
	sam_per_int = fre_ratio * pwm_per_int;							// sampling period in MTU ticks (int)

	// pwm and sampling freq. re-calculation
	pwm_fre_Hz = ((float32_t)(MTU_FRE_HZ / pwm_per_int));			// true pwm frequency in Hz (float)
	//pwm_per_s = ((float32_t)(pwm_per_int / MTU_FRE_HZ));			// true pwm period in sec (float)
	sam_fre_Hz = ((float32_t)(MTU_FRE_HZ / sam_per_int));			// true sampling frequency in Hz (float)
	sam_per_s = ((float32_t)(sam_per_int / MTU_FRE_HZ));			// true sampling period in sec (float)

	// main loop timing
	num_int = ((uint16_t)(sam_fre_Hz / MLOOP_FRE_HZ));				// number of sampling periods in one main loop (int)
	//mloop_fre_Hz = sam_fre_Hz / num_int;							// true main loop frequency in Hz (float)
	mloop_per_s = num_int / sam_fre_Hz;								// true main loop period in sec (float)

	// control parameters
	comptime = K_COMPPH * sam_per_s;								// phase compensation time

	// pwm management parameters
	//halfper = pwm_halfper_int;
	halfperdead = halfper + DEADTIM;								// half pwm period plus deadtimes in MTU ticks (int)
	delmax = halfper - DMIN;										// max delta between max duty and min duty in MTU ticks (int)
	uhalfper = 1.0f / halfper;
	//k_vout = delmax * uhalfper * ((float32_t) SQRT3D3);				// given VB, the max star vector is VB/SQRT3 (without hexagonal saturation)
	k_vout = delmax * uhalfper * ((float32_t) SQRT3D3 * K_OVERMOD);
	centre_pos = halfper - (((uint16_t)(CEN_POS_K * delmax)) >> 1);	// desired star centre position

	// other useful quantities
	sam_fre_kHz = sam_fre_Hz * 0.001f;								// true sampling frequency in kHz (float)
	sam_per_ms = 1000.0f * sam_per_s;								// true sampling period in ms (float)

	// filters time constants
	visu_ukt = 1.0f / (1.0f + (((float32_t)VISU_KT) * sam_fre_Hz));
	vb_ukt_fast = 1.0f / (1.0f + (((float32_t)VB_KT_FAST) * sam_fre_Hz));
	vb_ukt_slow = 1.0f / (1.0f + (((float32_t)VB_KT_SLOW) * sam_fre_Hz));
	omr_ukt = 1.0f / (1.0f + (((float32_t)OMR_KT) * sam_fre_Hz));
	off_ukt = 1.0f / (1.0f + (((float32_t)OFF_KT) * sam_fre_Hz));
	vr_ukt = 1.0f / (1.0f + (((float32_t)VR_KT) * sam_fre_Hz));

	// inverter non linearities table
#ifndef NOCOMP
	{
		int16_t i;
		for(i = 0; i < T_INL1_INDMAX; i++)
		{
			t_inl1[i] = pwm_fre_Hz * tab_inl1[i];
		}
	}
#endif	// NOCOMP

	return(retcode);
}
// end of sampling frequency related constants calculation

/******************************************************************************
Function:		MC_WaitSync
Description:	time synchronization control function
Input:			nothing
Output:			0 if main loop time is elapsed, 1 if not
Modifies:		internal status variable
******************************************************************************/
uint16_t MC_WaitSync(void)
{
	if(cnt_int)
	{
		return(1);
	}
	cnt_int = num_int;
	return(0);
}

#ifndef NOCOMP
/******************************************************************************
Function:		MC_TabCal
Description:	linear interpolation from a table
Input:			actual input in, one divided by input step udeltain, table
				address *table, maximum index indmax
Output:			interpolation value
******************************************************************************/
/*
float32_t MC_TabCal(float32_t in, float32_t udeltain, float32_t *table, uint16_t indmax)
{
	float32_t	f32a;
	uint16_t	index;

	if(0.0f >= in)
	{
		return(table[0]);
	}
	f32a = in * udeltain;
	index = f32a;
	if(index >= indmax)
	{
		return(table[indmax]);
	}
	f32a = f32a - index;
	return(table[index] + (f32a * (table[index + 1] - table[index])));
}
*/

/******************************************************************************
Function:		MC_INL1TabCal
Description:	linear interpolation from a table (simplified i/o)
Input:			actual input in
Output:			interpolation value
Note:			it is derived from the general function, avoiding the
				overload due to parameters passing
******************************************************************************/
float32_t MC_INL1TabCal(float32_t in)
{
	float32_t	f32a;
	uint16_t	index;

	if(0.0f >= in)
	{
		return(0.0f);
	}
	if(T_INL1_INMAX <= in)
	{
		return(t_inl1[T_INL1_INDMAX]);
	}
	f32a = in * T_INL_UDIN;
	index = f32a;		// integer part
	f32a = f32a - index;
	return(t_inl1[index] + (f32a * (t_inl1[index + 1] - t_inl1[index])));
}

#ifdef PSVDROP
/******************************************************************************
Function:		MC_INL2TabCal
Description:	linear interpolation from a table (simplified i/o)
Input:			actual input in
Output:			interpolation value
Note:			it is derived from the general function, avoiding the
				overload due to parameters passing
******************************************************************************/
float32_t MC_INL2TabCal(float32_t in)
{
	float32_t	f32a;
	uint16_t	index;

	if(0.0f >= in)
	{
		return(0.0f);
	}
	if(T_INL2_INMAX <= in)
	{
		return(tab_inl2[T_INL2_INDMAX]);
	}
	f32a = in * T_INL_UDIN;
	index = f32a;		// integer part
	f32a = f32a - index;
	return(tab_inl2[index] + (f32a * (tab_inl2[index + 1] - tab_inl2[index])));
}
#endif	// PSVDROP
#endif	// NOCOMP

// current reading functions
/******************************************************************************
Function:		MC_SetOff
Description:	A/D init and current offsets measurement
Input:			nothing
Output:			nothing
Modifies:		current reading offsets, conversion constants
******************************************************************************/
void MC_SetOff(void)
{
	uint32_t u32_oful, u32_ofvl, u32_ofwl;
	uint16_t u16_i;

	// wake up A/D module before accessing registers
	WAKEUP_S12AD
	DELAY1MS(10);

	// sampling times setting (16 states @25ns means 0.4us - minimum is 5 states)
	S12AD.ADSSTR0 = 0x10;			// pin56 P40 IU I AN0
	S12AD.ADSSTR1 = 0x10;			// pin55 P41 IV I AN1
	S12AD.ADSSTR2 = 0x10;			// pin54 P42 IW I AN2
	S12AD.ADSSTR3 = 0x10;			// pin53 P43 VB I AN3
	S12AD.ADSSTR4 = 0x10;			// pin52 P44 VU I AN4
	S12AD.ADSSTR5 = 0x10;			// pin51 P45 VV I AN5
	S12AD.ADSSTR6 = 0x10;			// pin50 P46 VW I AN6
	S12AD.ADSSTR7 = 0x10;			// pin49 P47 EX I AN7

	// AD Control register (reset 0x0000)
	S12AD.ADCSR.WORD = 0x0000;
	//S12AD.ADCSR.BIT.ADST = 0;		// B15		0  -> stopped
	//S12AD.ADCSR.BIT.ADCS = 0;		// B14, B13	00 -> single cycle scan mode
	//S12AD.ADCSR.BIT.ADIE = 0;		// B12		0  -> no int enabled
		// B11, B10
	//S12AD.ADCSR.BIT.TRGE = 0;		// B9		0  -> no start trigger enabled
	//S12AD.ADCSR.BIT.EXTRG = 0;	// B8		0  -> no external trigger selected
	//S12AD.ADCSR.BIT.DBLE = 0;		// B7
	//S12AD.ADCSR.BIT.GBADIE = 0;	// B6
		// B5
	//S12AD.ADCSR.BIT.DBLANS = 0;	// B4, .., B0

	// dedicated sample and hold setting
	S12AD.ADSHCR.WORD = 0x0710;		// AD Sample and Hold Circuit Control Register (reset 0x0000)
		// B15, .., B11
	//S12AD.ADSHCR.BIT.SHANS = 7;	// B10, B9, B8	7 -> all the three channels selected
	//S12AD.ADSHCR.BIT.SSTSH = 0x10;// B7, .., B0	16 states @25ns means 0.4us (16 states is the minimum @40MHz)

	// bits number and alignment: default value of ADCER (0) selects 12bit and right alignment
	S12AD.ADCER.WORD = 0x0000;		// AD Control Extended register

	// pins assignment to the peripheral
	MPC.PWPR.BYTE = 0x00;			// enable writing BIT6
	MPC.PWPR.BYTE = 0x40;			// enable writing MPC registers
	MPC.P47PFS.BYTE = 0x80;			// pin49 P47 EXT I -> AN7
	MPC.P46PFS.BYTE = 0x80;			// pin50 P46 VW I -> AN6
	MPC.P45PFS.BYTE = 0x80;			// pin51 P45 VV I -> AN5
	MPC.P44PFS.BYTE = 0x80;			// pin52 P44 VU I -> AN4
	MPC.P43PFS.BYTE = 0x80;			// pin53 P43 VB I -> AN3
	MPC.P42PFS.BYTE = 0x80;			// pin54 P42 IW I -> AN2
	MPC.P41PFS.BYTE = 0x80;			// pin55 P41 IV I -> AN1
	MPC.P40PFS.BYTE = 0x80;			// pin56 P40 IU I -> AN0
	MPC.PWPR.BYTE = 0x80;			// disable writing MPC registers

	// current offsets measurement cycle
	SEL_CUR_AD
	u32_oful = 0;
	u32_ofvl = 0;
	u32_ofwl = 0;
	for (u16_i = 0; 32 > u16_i; u16_i++)
	{
		S12AD_START
		WAIT_S12AD
		u32_oful += AN0_IU;
		u32_ofvl += AN1_IV;
		u32_ofwl += AN2_IW;
		DELAY1MS(1)
	}
	ium_off = u32_oful * ((float32_t)(1.0 / 32.0));
	ivm_off = u32_ofvl * ((float32_t)(1.0 / 32.0));
	iwm_off = u32_ofwl * ((float32_t)(1.0 / 32.0));

}

/******************************************************************************
Function:		MC_ADMan
Description:	management of A/D conversions for currents; three shunts
				current reading;
Input:			nothing
Output:			nothing
Modifies:		global variables ium, ivm, iwm (current samples reading)
Note:			global variable cr_status is used to determinate the situation
******************************************************************************/
static void MC_ADMan(void)
{

	if(OUT_ENABLED)
	{
		switch(cr_status)
		{
			case CR_UV:
				ium = KADI * (AN0_IU - ium_off);
				ivm = KADI * (AN1_IV - ivm_off);
				iwm = -ium - ivm;
				break;
			case CR_VW:
				ivm = KADI * (AN1_IV - ivm_off);
				iwm = KADI * (AN2_IW - iwm_off);
				ium = -ivm - iwm;
				break;
			case CR_WU:
				iwm = KADI * (AN2_IW - iwm_off);
				ium = KADI * (AN0_IU - ium_off);
				ivm = -iwm - ium;
				break;
			default:
				ium = 0.0f;
				ivm = 0.0f;
				iwm = 0.0f;
				break;
		}

		// alpha, beta currents calc
		//McrpLibf_uvw_alphabeta(ium, ivm, iwm, &iam, &ibm);
		McrpLibf_uv_alphabeta(ium, ivm, &iam, &ibm);
	}
	else
	{
#ifdef OFF_REFRESH
		// current offsets refresh
		ium_off = ium_off + ((AN0_IU - ium_off) * off_ukt);
		ivm_off = ivm_off + ((AN1_IV - ivm_off) * off_ukt);
		iwm_off = iwm_off + ((AN2_IW - iwm_off) * off_ukt);
#endif	// OFF_REFRESH
		ium = ivm = iwm = 0.0f;
		iam = ibm = 0.0f;
	}
}	// end of function MC_ADMan()
// end of current reading functions

// PWM management functions
/******************************************************************************
Function:		MC_IniPWM
Description:	pwm initialization
Input:			nothing
Output:			nothing
******************************************************************************/
void MC_IniPWM(void)
{

	// wake up module before accessing registers
	WAKEUP_MTU
	DELAY1MS(1);

	// enable writing, stop timers
	MTU.TRWERA.BIT.RWE = 1;			// MTU registers writing enable
	MTU.TSTRA.BIT.CST3 = 0;			// stop timer 3
	MTU.TSTRA.BIT.CST4 = 0;			// stop timer 4

	// general settings
	MTU3.TCR.BYTE = 0x00;			//
	//MTU3.TCR.BIT.CCLR = 0;		// synchronous clearing
	//MTU3.TCR.BIT.CKEG = 0;		// count at rising edge
	//MTU3.TCR.BIT.TPSC = 0;		// no peripheral clock frequency divider
	MTU4.TCR.BYTE = 0x00;			//
	//MTU4.TCR.BIT.CCLR = 0;		// synchronous clearing
	//MTU4.TCR.BIT.CKEG = 0;		// count at rising edge
	//MTU4.TCR.BIT.TPSC = 0;		// no peripheral clock frequency divider

	// registers and buffers setting
	MTU3.TCNT = DEADTIM;			// deadtime in TCNT3
	MTU4.TCNT = 0;					// 0 in TCNT4
	MTU.TDDRA = DEADTIM;			// deadtime in TDDR
	MTU.TCDRA = halfper;			// half pwm period in TCDR (note: MUST be > (TDDR*2+2)
	MTU.TCBRA = halfper;			// buffer for TCDR
	MTU3.TGRA = halfperdead;		// halfper + deadtime in TGRA3
	MTU3.TGRB = halfperdead;		// duty U (0%, low arms always on)
	MTU4.TGRA = halfperdead;		// duty V (0%, low arms always on)
	MTU4.TGRB = halfperdead;		// duty W (0%, low arms always on)
	MTU3.TGRC = halfperdead;		// buffer for TGRA3
	MTU3.TGRD = halfperdead;		// buffer for TGRB3
	MTU4.TGRC = halfperdead;		// buffer for TGRA4
	MTU4.TGRD = halfperdead;		// buffer for TGRB4 (LAST TO BE WRITTEN!)

	// pwm frequency output enable and active levels setting
	MTU.TOCR1A.BIT.PSYE = 0;		// disable output of pwm frequency
	MTU.TOCR1A.BIT.OLSN = 1;		// select high-active output level
	MTU.TOCR1A.BIT.OLSP = 1;		// select high-active output level

	// buffer setting
	MTU3.TMDR1.BIT.BFB = 1;			// TGRD3 is a buffer for TGRB3
	MTU3.TMDR1.BIT.BFA = 1;			// TGRC3 is a buffer for TGRA3
	//MTU4.TMDR1.BIT.BFB = 1;		// TGRD4 is a buffer for TGRB4
	//MTU4.TMDR1.BIT.BFA = 1;		// TGRC4 is a buffer for TGRA4

	// complementary pwm mode setting
	MTU3.TMDR1.BIT.MD = 0x0D;		// complementary PWM mode, transmit at crest
	//MTU3.TMDR1.BIT.MD = 0x0E;		// complementary PWM mode, transmit at trough
	//MTU3.TMDR1.BIT.MD = 0x0F;		// complementary PWM mode, transmit at crest and trough
	//MTU4.TMDR1.BIT.MD = 0;		// do not set here, but only in MTU3.TMDR1

	// output disable
	MTU.TOERA.BYTE = 0xC0;			// all outputs disabled (at inactive level)
	//MTU.TOERA.BYTE = 0xFF;		// all outputs enabled

	// pins assignment to the peripheral
	// NOW the pin configuration is all outputs (at not active level) I/O ports (from hwsetup.c)
	MPC.PWPR.BYTE = 0x00;			// enable writing BIT6
	MPC.PWPR.BYTE = 0x40;			// enable writing MPC registers
	MPC.P76PFS.BYTE = 0x01;			// pin33 P76 WL O -> MTIOC4D
	MPC.P75PFS.BYTE = 0x01;			// pin34 P75 VL O -> MTIOC4C
	MPC.P74PFS.BYTE = 0x01;			// pin35 P74 UL O -> MTIOC3D
	MPC.P73PFS.BYTE = 0x01;			// pin36 P73 WH O -> MTIOC4B
	MPC.P72PFS.BYTE = 0x01;			// pin37 P72 VH O -> MTIOC4A
	MPC.P71PFS.BYTE = 0x01;			// pin38 P71 UH O -> MTIOC3B
	MPC.PWPR.BYTE = 0x80;			// disable writing MPC registers

	// port mode registers setting
	PORT7.PMR.BYTE |= 0x7E;			// port mode register
	//PORT7.PMR.BIT.B6 = 1;			// pin33 P76 WL O MTIOC4D
	//PORT7.PMR.BIT.B5 = 1;			// pin34 P75 VL O MTIOC4C
	//PORT7.PMR.BIT.B4 = 1;			// pin35 P74 UL O MTIOC3D
	//PORT7.PMR.BIT.B3 = 1;			// pin36 P73 WH O MTIOC4B
	//PORT7.PMR.BIT.B2 = 1;			// pin37 P72 VH O MTIOC4A
	//PORT7.PMR.BIT.B1 = 1;			// pin38 P71 UH O MTIOC3B

	// POE pin status check before POE init
	if(0 == PORT7.PIDR.BIT.B0)		// pin39 P70 OVC I POE0
	{
		SET_ALRM_ON
		UIF_R.var.all = FAULT_ALL;
	}

	// POE init for pwm pins protection
	POE.ICSR1.WORD = 0x0000;		// pin POE0 is used, falling edge 
	//POE.ICSR1.BIT.POE0M = 0;		// POE0 Mode	0 -> Falling edge (modif. only once)
	//POE.ICSR1.BIT.PIE1 = 0;		// POE Port  interrupt enable (POE 0, 1, 2)
	//POE.ICSR1.BIT.POE0F = 0;		// POE0 Flag
	//INTC.IPRF.BIT._MTU_POE = 5;	// POE
	POE.OCSR1.WORD = 0x20;			// POE output level control/status register 1
	//POE.OCSR1.BIT.OSF1 = 0;		// POE output short circuit Flag 1
	//POE.OCSR1.BIT.OCE1 = 1;		// POE output short circuit high impedance control 1 (modif. only once)
	//POE.OCSR1.BIT.OIE1 = 0;		// POE output short circuit interrupt enable 1
	POE.POECR2.BIT.MTU3BDZE = 1;	// P71-P74	UH-UL
	POE.POECR2.BIT.MTU4ACZE = 1;	// P72-P75	VH-VL
	POE.POECR2.BIT.MTU4BDZE = 1;	// P73-P76	WH-WL
	//POE.SPOER.BIT.MTUCH34HIZ = 0;	// MTU ch 3, 4 not in HIZ

	// pin assignment to the peripheral
	MPC.PWPR.BYTE = 0x00;			// enable writing BIT6
	MPC.PWPR.BYTE = 0x40;			// enable writing MPC registers
	MPC.P70PFS.BYTE = 0x07;			// pin39 P70 OVC I -> POE0
	MPC.PWPR.BYTE = 0x80;			// disable writing MPC registers

	// port mode register setting
	PORT7.PMR.BIT.B0 = 1;			// pin39 P70 OVC I POE0

}

/******************************************************************************
Function:		MC_StopOut
Description:	disable pwm outputs
Input:			nothing
Output:			nothing
******************************************************************************/
void MC_StopOut(void)
{
	DISABLE_OUT			// all outputs disabled (at inactive level)
	RES_COM_ON
}

/******************************************************************************
Function:		MC_StartOut
Description:	enables pwm outputs and insert a loop for bootstrap
Input:			nothing
Output:			nothing
******************************************************************************/
void MC_StartOut(void)
{
	if(OUT_ENABLED)
	{
		SET_COM_ON		// set the flag after one main loop (bootstrap)
	}
	else
	{
		RES_COM_ON
		ENABLE_OUT;		// all outputs enabled
	}
}

/******************************************************************************
Function:		MC_EnablePOE
Description:	POE interrupt enable
Input:			nothing
Output:			nothing
******************************************************************************/
void MC_EnablePOE(void)
{
	// POE pin status check before interrupt enable
	if(0 == PORT7.PIDR.BIT.B0)		// pin39 P70 OVC I POE0
	{
		MC_StopOut();
		SET_ALRM_ON
		UIF_R.var.all = FAULT_ALL;
		return;
	}

	// flags reset and interrupt enable
	if(0 != POE.ICSR1.BIT.POE0F)
	{
		POE.ICSR1.BIT.POE0F = 0;	// reset flag
	}
	POE.ICSR1.BIT.PIE1 = 1;			// interrupt request enable
	if(0 != POE.OCSR1.BIT.OSF1)
	{
		POE.OCSR1.BIT.OSF1 = 0;		// reset flag
	}
	POE.OCSR1.BIT.OIE1 = 1;			// interrupt request enable
	IPR(POE, OEI1) = OVC_INT_P;		// interrupt priority
	IR(POE, OEI1) = 0;				// reset interrupt request
	IEN(POE, OEI1) = 1;				// interrupt enable
}

/******************************************************************************
Function:		MC_StartPWM
Description:	starts pwm and main interrupt (but not the outputs)
Input:			nothing
Output:			nothing
******************************************************************************/
void MC_StartPWM(void)
{

	// A/D converter start trigger selection
	MTU4.TADCR.WORD = 0x8080;		// Timer A/D Converter Start Request Control Register (Reset val 0x0000)
	//MTU4.TADCR.BIT.BF = 2;		// B15, B14		10 -> transfer at trough
	//MTU4.TADCR.BIT.UT4AE = 1;		// B7			1  -> trigger enabled in up counting (after trough)
	//MTU4.TADCR.BIT.DT4AE = 0;		// B6
	//MTU4.TADCR.BIT.UT4BE = 0;		// B5
	//MTU4.TADCR.BIT.DT4BE = 0;		// B4
	//MTU4.TADCR.BIT.ITA3AE = 0;	// B3
	//MTU4.TADCR.BIT.ITA4VE = 0;	// B2			int skipping still NOT linked
	//MTU4.TADCR.BIT.ITB3AE = 0;	// B1
	//MTU4.TADCR.BIT.ITB4VE = 0;	// B0
	MTU4.TADCOBRA = CON_DELAY;		// Timer A/D Converter Start Request Cycle Set Buffer Register (Reset val 0xFFFF)
	MTU4.TADCORA = CON_DELAY;		// Timer A/D Converter Start Request Cycle Set Register (Reset val 0xFFFF)

	// interrupt skipping (Timer 4 underflow -> trough)
	MTU.TITCR1A.BYTE = 0x00;		// Timer Interrupt Skipping Set Register 1A (Reset val 0x00)
	//MTU.TITCR1A.BIT.T3AEN = 0;	// B7
	//MTU.TITCR1A.BIT.T3ACOR = 0;	// B6, B5, B4
	//MTU.TITCR1A.BIT.T4VEN = 0;	// B3
	//MTU.TITCR1A.BIT.T4VCOR = 0;	// B2,B1, B0
	if(0 != int_skip)
	{
		MTU.TITCR1A.BYTE |= int_skip;
		MTU4.TADCR.BIT.ITA4VE = 1;	// B2			1  -> int skipping is linked
	}

	// A/D converter configuration for triggered single scan conversion of currents
	S12AD.ADSTRGR.BIT.TRSA = 9;		// select TRG4AN (MTU4.TCNT and MTU4.TADCORA compare match) as trigger source
	SEL_CVB_AD						// channels select (0, 1, 2 and 3)

	// interrupt setup
	S12AD.ADCSR.WORD = 0x1200; 		// ADIE, TRGE are 1
	IPR(S12AD, S12ADI0) = MC_INT_P;	// interrupt priority (S12ADI0)
	IR(S12AD, S12ADI0) = 0;			// reset interrupt request
	IEN(S12AD, S12ADI0) = 1;		// enable interrupt

	// timer start
	MTU.TSTRA.BYTE |= 0xC0;			// simultaneously start timer 3 and 4

	// POE interrupt enable
	MC_EnablePOE();

	// stop outputs for safety
	MC_StopOut();
}

/******************************************************************************
Function:		MC_PWMGen
Description:	output duties calc, and three shunts current reading status
				cr_status determination
Input:			nothing
Note:			uses global variables vuc, vvc and vwc (requested voltages),
				global variable IU, IV, IW (currents),
				global variable vbus (bus voltage)
Output:			nothing
Modifies:		global variables duty_u, duty_v, duty_w, cr_status, vuc, vvc, vwc
******************************************************************************/
#ifndef NOCOMP
	float32_t	eu, ev, ew;
#endif	// NOCOMP
void MC_PWMGen(void)
{
	float32_t	f32a;
	int32_t		s32a;
	int16_t		*amin, *amed, *amax;
	int16_t		ms, hmed, hmax;
#ifdef CENTRED
	int16_t		s16a;
#endif	// CENTRED

	if(VBMIN > vbus)
	{
		duty_u = duty_v = duty_w = halfperdead;
		vuc = vvc = vwc = 0.0f;
		cr_status = CR_NO;
		return;
	}

#ifndef NOCOMP
	// errors calculation and desired output voltages pre-compensation
	eu = MC_INL(IU);
	ev = MC_INL(IV);
	ew = MC_INL(IW);
	vuc = vuc + eu;
	vvc = vvc + ev;
	vwc = vwc + ew;
#endif	// NOCOMP

	// duty-cycles calculation
	// note: no problems if vbus is summed to all the three duties
	f32a = (-((float32_t)halfper)) / vbus;	// f32a = halfper / vbus;
	duty_u = f32a * vuc;					// duty_u = f32a * (vbus - vuc);
	duty_v = f32a * vvc;					// duty_v = f32a * (vbus - vvc);
	duty_w = f32a * vwc;					// duty_w = f32a * (vbus - vwc);

	// finding duties order and limits
	if(duty_u >= duty_v)
	{										// du >= dv
		if(duty_v >= duty_w)
		{									// du >= dv >= dw
			cr_status = CR_UV;				// w is the min -> we choose iu and iv
			amin = (&duty_w);
			amed = (&duty_v);
			amax = (&duty_u);
		}
		else
		{									// du >= dv, dw > dv
			cr_status = CR_WU;				// v is the min -> we choose iw and iu
			amin = (&duty_v);
			if(duty_u >= duty_w)
			{								// du >= dw > dv
				amed = (&duty_w);
				amax = (&duty_u);
			}
			else
			{								// dw > du > dv
				amed = (&duty_u);
				amax = (&duty_w);
			}
		}
	}
	else
	{										// dv > du
		if(duty_u >= duty_w)
		{									// dv > du >= dw
			cr_status = CR_UV;				// w is the min -> we choose iu and iv
			amin = (&duty_w);
			amed = (&duty_u);
			amax = (&duty_v);
		}
		else
		{									// dv > du, dw > du
			cr_status = CR_VW;				// u is the min -> we choose iv and iw
			amin = (&duty_u);
			if(duty_v >= duty_w)
			{								// dv > dw > du
				amed = (&duty_w);
				amax = (&duty_v);
			}
			else
			{								// dw > dv > du
				amed = (&duty_v);
				amax = (&duty_w);
			}
		}
	}

	// duties differences calculation
	hmed = (*amed) - (*amin);
	hmax = (*amax) - (*amin);

	// maximum limit control (and saturation)
	if(hmax > delmax)
	{
		s32a = mul_w(hmed, delmax);
		hmed = div_w(s32a, hmax);
		hmax = delmax;
	}

	// possible shift calculation
	ms = halfper - hmax;

// now we have min_duty equal to ms, med_duty equal to (ms + hmed), and max_duty equal to (ms + hmax)
// the maximum duty is at its maximum value halfper, that is, the modulation is clamped at the minimum voltage
// the minimum duty is surely greater than DMIN, due to the previous clamping (delmax)
// the duty corresponding to the voltage average (star-centre) is: ms + ((hmed + hmax) / 3)

	// duties update
#ifdef CENTRED
	// moving the duties to obtain centred modulation
	s32a = mul_w((hmed + hmax), 5461);			// 5461/16384 ~ 1/3
	s16a = ms + (s32a >> 14);					// star centre equivalent duty
	if(s16a > centre_pos)						// we can only decrease the duties (increase the voltages)
	{
		ms = ms + centre_pos - s16a;
		if(ms < DMIN)
		{
			ms = DMIN;							// cannot go under the minimum
		}
	}
	(*amax) = ms + hmax;
#else	// ifdef CENTRED
	// keeping clamped position
	(*amax) = halfper;
#endif	// else ifdef CENTRED
	(*amed) = ms + hmed;
	(*amin) = ms;

	// output voltages calculation after clamping
	f32a = vbus * uhalfper;
	vuc = (halfper - duty_u) * f32a;
	vvc = (halfper - duty_v) * f32a;
	vwc = (halfper - duty_w) * f32a;

#ifndef NOCOMP
	// inverter errors compensation
	vuc = vuc - eu;
	vvc = vvc - ev;
	vwc = vwc - ew;
#endif	// NOCOMP

	// commutations number reducing
#ifdef COMMRED
	if(halfper == duty_u)
	{
		duty_u = halfperdead;
	}
	if(halfper == duty_v)
	{
		duty_v = halfperdead;
	}
	if(halfper == duty_w)
	{
		duty_w = halfperdead;
	}
#endif	// COMMRED

}	// end of function MC_PWMGen()
// end of PWM management functions

/******************************************************************************
Function:		MC_VecComp
Description:	calculation of the second component of a vector (absolute value)
Input:			vector amplitude amp (greater than zero)
				first component cmp
Output:			second component
Note:			result is positive or zero
******************************************************************************/
float32_t MC_VecComp(float32_t amp, float32_t cmp)
{
	float32_t f32a;

	f32a = (amp * amp) - (cmp * cmp);
	if(0.0f < f32a)
	{
		return(sqrtf(f32a));
	}
	else
	{
		return(0.0f);
	}
}

/******************************************************************************
Function:		MC_IniPar
Description:	internal constants initialization, using eeprom values
Input:			nothing
Output:			nothing
Modifies:		many internal variables, using eeprom parameters values
******************************************************************************/
void MC_IniPar(void)
{
	uint16_t u16a;

	// pwm constants setting, based on eeprom values
	MC_SetPwmConst(SAM_FRE, F_RATIO);
	UIF_R.var.pwmf = pwm_fre_Hz;
	UIF_R.var.sf = sam_fre_Hz;

#ifdef CONST_DEBUG
	const_debug();
#endif	// CONST_DEBUG

	// additional features enabling
#ifdef CPI_TUN
	ENA_CURPI_TUN
#endif	// CPI_TUN
#ifdef CPI_AUT
	ENA_CURPI_AUT
#endif	// CPI_AUT
#ifdef	MOT_IDENT
	ENA_AUTO_IDEN
#endif	//	MOT_IDENT
#ifdef OSC_WIN
	ENA_OSCI_WIND
#endif	// OSC_WIN

#ifdef FLUXWEAK
	// direct current variation ramps (flux weakening)
	intid_rup = ((float32_t)DCUR_DT) * sam_per_s;	// id integral variation ramp up in FW
	intid_rdw = ((float32_t)DCUR_DT) * sam_per_s;	// id integral variation ramp dw in FW
#endif	// ifdef FLUXWEAK

	// polar couples and related constants
	c_poli = C_POLI;
	krpmocp = c_poli * KRPMO;
	ukrpmocp = ((float32_t)(1.0f / krpmocp));
	maxerr = krpmocp * MAXERR_RPM;
#ifdef RAMP_CL0
	r_marg = R_MARG * krpmocp;
#endif	// RAMP_CL0

	// rpm max and min
	rpm_max = RPM_MAX;
	rpm_min = RPM_MIN;

	// max angular speed, max speed for trip detection
	max_speed = rpm_max * krpmocp;
	max_speed_trip = max_speed * T_MARGH;
	max_s_trip = max_speed_trip;

	// min angular speed, and min speed for trip detection
	min_speed = rpm_min * krpmocp;
	min_speed_trip = min_speed * T_MARGL;
	min_s_trip = min_speed_trip;

	// startup time in internal units calculation, and delta speed every startup step
	sup_tim_ep = SUP_TIM;
	delta_om = min_speed * (sam_per_ms / sup_tim_ep);	// to be added to speed at every step

	// acceleration ramp
	r_acc_ep = R_ACC;				// rpm/s
	r_acc = r_acc_ep * mloop_per_s;	// rpm/one_loop_time

	// deceleration ramp
	r_dec_ep = R_DEC;				// rpm/s
	r_dec = r_dec_ep * mloop_per_s;	// rpm/one_loop_time

	// startup current
	i_start_ep = I_START;
	i_start = i_start_ep * ((float32_t)(1.0 / AMP_RES));

	// current rising time and current variation ramps
	is_rup = i_start * sam_per_ms * ((float32_t)(1.0 / STUP_CUR_RT_MS));
	is_rdw = i_start * sam_per_ms * ((float32_t)(1.0 / STUP_CUR_FT_MS));

	// maximum current
	i_max_ep = I_MAX;
	i_max = i_max_ep * ((float32_t)(1.0 / AMP_RES));
	id_max = i_max * K_MARGI_FW;

	// stator resistance
	r_sta_ep = R_STA;
	r_sta = r_sta_ep * ((float32_t)(1.0 / OHM_RES));

	// synchronous inductance
	l_syn_ep = L_SYN;
	l_syn = l_syn_ep * ((float32_t)(1.0 / HEN_RES));

	// permanent magnets flux
	pm_flx_ep = PM_FLX;
	pm_flx = pm_flx_ep * ((float32_t)(1.0 / WEB_RES));

	// flux amplitude feedback gain (used in case of exact integration estimation algo)
	fb_gain_ep = FB_GAIN;
	fb_gain = fb_gain_ep;

	// flux estimation filter time constant tau (used in case of approximate integration estimation algo)
	flx_ftau_ep = FLX_FTAU;
	flx_lpco_hz = (SEC_RES / FTWOPI) / flx_ftau_ep;

	// PI gains
	kp_cur_ep = KP_CUR;
	kp_cur = kp_cur_ep * ((float32_t)(1.0 / AMP_KPCUR));
	ki_cur_ep = KI_CUR;
	ki_cur = ki_cur_ep * sam_per_s * ((float32_t)(1.0 / AMP_KICUR));
	kp_vel_ep = KP_VEL;
	kp_vel = kp_vel_ep * ((float32_t)(1.0 / AMP_KPVEL));
	ki_vel_ep = KI_VEL;
	ki_vel = ki_vel_ep * sam_per_s * ((float32_t)(1.0 / AMP_KIVEL));

	// flux and speed estimation functions init
#ifndef APP_INT
	McrpLibf_FluxEst_SetParam(r_sta, l_syn, pm_flx, fb_gain, sam_per_s, SPEED_LP_CUTOFF_HZ);
#else	// APP_INT
	McrpLibf_FluxEstA_SetParam(r_sta, l_syn, pm_flx, flx_lpco_hz, sam_per_s, SPEED_LP_CUTOFF_HZ);
#endif	// APP_INT

	// speed ramp state machine
	ramp_status = STOP_0;
	SET_STOPPED
	RES_STARTED
	// phase offset
	Phase_offset = 0.0f;

#ifdef MOT_IDENT
	// motor identification variables
	ial_1 = i_max * HIG_CUR_RATIO;
	ial_2 = i_max * MID_CUR_RATIO;
	ial_3 = i_max * LOW_CUR_RATIO;
	ial_r = ial_1 * sam_per_ms * ((float32_t)(1.0 / CUR_RISE_TIME_MS));
	iden_om = ((float32_t)(TWOPI * IDENT_SPEED_HZ));
	iden_or = iden_om * sam_per_ms * ((float32_t)(1.0f / SPE_RISE_TIME_MS));
#endif	// MOT_IDENT

	// buffers
	rbuf = ((uint16_t *)(outbuf.ss));
	wbuf = ((uint16_t *)(outbuf1.ss));
	pbuf = rbuf;
	for(u16a = 0; u16a < OUTBUF_SDIM; u16a++)
	{
		rbuf[u16a] = 0;
		wbuf[u16a] = 0;
	}

#ifdef OSC_WIN
	// oscilloscope window variables
	selmem = 0;
	bcnt0 = 0;
	bcnt1 = 0;
	bcntm = 0;
	phmem = 0.0f;
	sample = 0;
	RES_TRGED
	RES_RBUSY
	RES_CSELD
#endif	// OSC_WIN

	// mode
	SET_NORM_MODE

}

/******************************************************************************
Function:		MC_UpdPar
Description:	internal constants update, if parameters are changed
Input:			nothing
Output:			nothing
Modifies:		many internal variables, using eeprom parameters values
******************************************************************************/
void MC_UpdPar(void)
{
	int16_t	s16mod = 0;

	// parameters update if changed
	if(	(RPM_MIN != rpm_min) || (RPM_MAX != rpm_max) ||
		(C_POLI != c_poli)   || (SUP_TIM != sup_tim_ep) )
	{
		rpm_min = RPM_MIN;
		rpm_max = RPM_MAX;
		c_poli = C_POLI;
		sup_tim_ep = SUP_TIM;
		krpmocp = c_poli * ((float32_t) KRPMO);
		ukrpmocp = ((float32_t)(1.0f / krpmocp));
		maxerr = krpmocp * MAXERR_RPM;
		max_speed = rpm_max * krpmocp;
		min_speed = rpm_min * krpmocp;
		max_s_trip = max_speed_trip = max_speed * T_MARGH;
		min_s_trip = min_speed_trip = min_speed * T_MARGL;
		delta_om = min_speed * (sam_per_ms / sup_tim_ep);	// to be added to speed at every step
#ifdef RAMP_CL0
		r_marg = R_MARG * krpmocp;
#endif	// RAMP_CL0
	}
	if(R_ACC != r_acc_ep)
	{
		r_acc_ep = R_ACC;					// in rpm/sec
		r_acc = r_acc_ep * ((float32_t) mloop_per_s);
	}
	if(R_DEC != r_dec_ep)
	{
		r_dec_ep = R_DEC;
		r_dec = r_dec_ep * ((float32_t) mloop_per_s);
	}
	if(I_START != i_start_ep)
	{
		i_start_ep = I_START;
		i_start = i_start_ep * ((float32_t)(1.0 / AMP_RES));
		is_rup = i_start * sam_per_ms * ((float32_t)(1.0 / STUP_CUR_RT_MS));
		is_rdw = i_start * sam_per_ms * ((float32_t)(1.0 / STUP_CUR_FT_MS));
	}
	if(I_MAX != i_max_ep)
	{
		i_max_ep = I_MAX;
		i_max = i_max_ep * ((float32_t)(1.0 / AMP_RES));
		id_max = i_max * K_MARGI_FW;
#ifdef MOT_IDENT
		ial_1 = i_max * HIG_CUR_RATIO;
		ial_2 = i_max * MID_CUR_RATIO;
		ial_3 = i_max * LOW_CUR_RATIO;
		ial_r = ial_1 * sam_per_ms * ((float32_t)(1.0f / CUR_RISE_TIME_MS));
#endif	// MOT_IDENT
	}

	if(R_STA != r_sta_ep)
	{
		r_sta_ep = R_STA;
		r_sta = r_sta_ep * ((float32_t)(1.0 / OHM_RES));
		s16mod = -1;
	}
	if(L_SYN != l_syn_ep)
	{
		l_syn_ep = L_SYN;
		l_syn = l_syn_ep * ((float32_t)(1.0 / HEN_RES));
		s16mod = -1;
	}
	if(PM_FLX != pm_flx_ep)
	{
		pm_flx_ep = PM_FLX;
		pm_flx = pm_flx_ep * ((float32_t)(1.0 / WEB_RES));
		s16mod = -1;
	}
	if(FB_GAIN != fb_gain_ep)
	{
		fb_gain_ep = FB_GAIN;
		fb_gain = fb_gain_ep;
		s16mod = -1;
	}
	if(FLX_FTAU != flx_ftau_ep)
	{
		flx_ftau_ep = FLX_FTAU;
		flx_lpco_hz = (SEC_RES / FTWOPI) / flx_ftau_ep;
		s16mod = -1;
	}

	if(0 != s16mod)
	{
#ifndef APP_INT
		McrpLibf_FluxEst_SetParam(r_sta, l_syn, pm_flx, fb_gain, sam_per_s, SPEED_LP_CUTOFF_HZ);
#else	// APP_INT
		McrpLibf_FluxEstA_SetParam(r_sta, l_syn, pm_flx, flx_lpco_hz, sam_per_s, SPEED_LP_CUTOFF_HZ);
#endif	// APP_INT
	}

	if(KP_CUR != kp_cur_ep)
	{
		kp_cur_ep = KP_CUR;
		kp_cur = kp_cur_ep * ((float32_t)(1.0 / AMP_KPCUR));
	}
	if(KI_CUR != ki_cur_ep)
	{
		ki_cur_ep = KI_CUR;
		ki_cur = ki_cur_ep * sam_per_s * ((float32_t)(1.0 / AMP_KICUR));
	}
	if(KP_VEL != kp_vel_ep)
	{
		kp_vel_ep = KP_VEL;
		kp_vel = kp_vel_ep * ((float32_t)(1.0 / AMP_KPVEL));
	}
	if(KI_VEL != ki_vel_ep)
	{
		ki_vel_ep = KI_VEL;
		ki_vel = ki_vel_ep * sam_per_s * ((float32_t)(1.0 / AMP_KIVEL));
	}

	// phase offset
	if(0.0f > omrif)
	{
		Phase_offset = PHA_OFF * (-FTWOPI / 360.0f);
	}
	else if(0.0f < omrif)
	{
		Phase_offset = PHA_OFF * (FTWOPI / 360.0f);
	}
	else
	{
		Phase_offset = 0.0f;
	}

}

/******************************************************************************
Function:		MC_ConFil
Description:	management of measured values, to give them to the GUI
Input:			nothing
Output:			nothing
Modifies:		interface variables defined in userif.h
******************************************************************************/
void MC_ConFil(void)
{

	// speed reference
	UIF_R.var.rif = rpmrif_y;

	// mechanical speed
	mec_rpm = omf * ukrpmocp;
	UIF_R.var.rpm = mec_rpm;

	// imposed frequency
	freq = omf * ((float32_t) KOF);
	UIF_R.var.fre = freq * 10.0f;		// visualization in Hz/10

	// direct current
	UIF_R.var.id = idmf * 1000.0f;		// visualization in mA(peak)

	// quadrature current
	UIF_R.var.iq = iqmf * 1000.0f;		// visualization in mA(peak)

	// direct voltage
	UIF_R.var.vd = vdmf * 10.0f;		// visualization in V/10(peak, star)

	// quadrature voltage
	UIF_R.var.vq = vqmf * 10.0f;		// visualization in V/10(peak, star)

	// bus voltage
	UIF_R.var.vb = vbusf * 10.0f;		// visualization in V/10

	// total current
	McrpLibf_xy_rt(idmf, iqmf, &f_amp_i, &f_arg_i);
	UIF_R.var.toti = f_amp_i * 1000.0f;	// visualization in mA(peak)

	// total voltage
	McrpLibf_xy_rt(vdmf, vqmf, &f_amp_v, &f_arg_v);
	UIF_R.var.totv = f_amp_v * 10.0f;	// visualization in V/10(peak, star)

	// dummy
#ifndef APP_INT
	FluxAmp = McrpLibf_FluxEst_GetAmp();
#else	// APP_INT
	FluxAmp = McrpLibf_FluxEstA_GetAmp();
#endif	// APP_INT
	UIF_R.var.du0 = FluxAmp * WEB_RES;
//	UIF_R.var.du0 = ((float32_t)(360 / TWOPI)) * McrpLibf_AngleNrm(f_arg_v - f_arg_i);
}

/******************************************************************************
Function:		MC_SpeedPI
Description:	Speed PI control
Input:			reference speed wr, measured speed wm, maximum output imx,
				integral term memory address *i
Output:			control output
Modifies:		integral memory i
******************************************************************************/
float32_t MC_SpeedPI(float32_t wr, float32_t wm, float32_t imx, float32_t *i)
{
	float32_t f32a = wr - wm;

#ifdef SPEEDPI_ERRCLAMP
	if(maxerr < f32a)
	{
		f32a = maxerr;
	}
	else if(-maxerr > f32a)
	{
		f32a = -maxerr;
	}
#endif	// SPEEDPI_ERRCLAMP

	return(McrpLibf_PIW(f32a, imx, -imx, kp_vel, ki_vel, i));
}

/******************************************************************************
Function:		MC_IdIqPI
Description:	Current PI control
Input:			reference direct current ird,
				measured direct current imd,
				reference quadrature current irq,
				measured quadrature current imq,
				maximum allowable output vmx,
				direct output address *avd,
				quadrature output address *avq,
				direct integral memory address *iid,
				quadrature integral memory address *iiq
Output:			control output
Modifies:		integral memories iid, iiq
******************************************************************************/
void MC_IdIqPI(	float32_t ird, float32_t imd, float32_t irq, float32_t imq, float32_t vmx,
				float32_t *avd, float32_t *avq, float32_t *iid, float32_t *iiq)
{
	float32_t f32a;

	if(0.0f >= vmx)
	{
		*avd = 0.0f;
		*avq = 0.0f;
		*iid = 0.0f;
		*iiq = 0.0f;
		return;
	}

	// id control
	f32a = vmx * K_MARGV_DQ;			// vmaxd
	(*avd) = McrpLibf_PIW((ird - imd), f32a, -f32a, kp_cur, ki_cur, iid);

	// iq control
	f32a = MC_VecComp(vmx, (*avd));	// vmaxq
	(*avq) = McrpLibf_PIW((irq - imq), f32a, -f32a, kp_cur, ki_cur, iiq);
}

#ifdef CPI_TUN
/******************************************************************************
Function:		MC_CurPITun
Description:	creation of a current step, to manually tune the current PIs
Input:			nothing
Output:			nothing
******************************************************************************/
void MC_CurPITun(void)
{
	if(MAX_SAM_CNT > pit_c)
	{
		idr = i_max * ratio;
		iqr = 0.0f;
		if(50 > pit_c)				// 50 samples	0, .., 49, and 50 interrupts
		{
			rbuf[pit_c++] = 1000.0f * idm;
			cur_ampf = 0.0f;
			pit_s = 0;
		}
		else if(65 > pit_c)			// 15 samples	50, .., 64, and 45 interrupts
		{
			if(3 > pit_s)
			{
				pit_s++;
				cur_ampf = cur_ampf + idm;
			}
			else
			{
				rbuf[pit_c++] = 1000.0f * cur_ampf / pit_s;
				cur_ampf = 0.0f;
				pit_s = 0;
			}
		}
		else if(80 > pit_c)			// 15 samples	65, .., 79, and 135 interrupts
		{
			if(9 > pit_s)
			{
				pit_s++;
				cur_ampf = cur_ampf + idm;
			}
			else
			{
				rbuf[pit_c++] = 1000.0f * cur_ampf / pit_s;
				cur_ampf = 0.0f;
				pit_s = 0;
			}
		}
		else if(95 > pit_c)			// 15 samples	80, .., 94, and 405 interrupts
		{
			if(27 > pit_s)
			{
				pit_s++;
				cur_ampf = cur_ampf + idm;
			}
			else
			{
				rbuf[pit_c++] = 1000.0f * cur_ampf / pit_s;
				cur_ampf = 0.0f;
				pit_s = 0;
			}
		}
		else if(110 > pit_c)		// 15 samples	95, .., 109, and 1215 interrupts
		{
			if(81 > pit_s)
			{
				pit_s++;
				cur_ampf = cur_ampf + idm;
			}
			else
			{
				rbuf[pit_c++] = 1000.0f * cur_ampf / pit_s;
				cur_ampf = 0.0f;
				pit_s = 0;
			}
		}
		else /*if(MAX_SAM_CNT > pit_c)*/	// remaining samples	110, .., 1XX, and YYYY interrupts
		{
			if(243 > pit_s)
			{
				pit_s++;
				cur_ampf = cur_ampf + idm;
			}
			else
			{
				rbuf[pit_c++] = 1000.0f * cur_ampf / pit_s;
				cur_ampf = 0.0f;
				pit_s = 0;
			}
		}
	}		// total: MAX_SAM_CNT samples and less than 5495 interrupts (around 343ms @16kHz)
	else	// pit_c >= MAX_SAM_CNT	(MAX_SAM_CNT <= 125)
	{
		idr = 0.0f;
		iqr = 0.0f;
		if(150 > pit_c)
		{
			pit_c++;
		}
	}
}
#endif	// CPI_TUN

#ifdef CPI_AUT
/******************************************************************************
Function:		MC_CurPIAuto
Description:	creation of a voltage profile, used to automatically tune
				the current PIs
Input:			nothing
Output:			nothing
******************************************************************************/
void MC_CurPIAuto(void)
{
	idr = 0.0f;
	iqr = 0.0f;
	PIT_SetCur(idm, iqm);
	switch(pitun_status)
	{
		case R_UP:		// voltage ramp up till current reaches rated value
			if(PIT_RampUp(&v_req))
			{
				pitun_status = R_MS;
			}
			break;
		case R_MS:		// wait some time, measuring the current level (filtered)
			if(PIT_Measure(&v_req))
			{
				pitun_status = R_DW;
			}
			break;
		case R_DW:		// voltage ramp down
			if(PIT_RampDown(&v_req))
			{
				pitun_status = R_ST;
			}
			break;
		case R_ST:		// voltage step
			if(PIT_VoltStep(&v_req))
			{
				pitun_status = R_EN;
			}
			break;		// finished
		case R_EN:
		default:
			v_req = 0.0f;
			break;
	}
	vdc = v_req;
	vqc = 0.0f;
}
#endif	// CPI_AUT

#ifdef MOT_IDENT
/******************************************************************************
Function:		MC_MotIdent
Description:	motor identification sequence
Input:			nothing
Output:			nothing
******************************************************************************/
void MC_MotIdent(void)
{

	iqr = 0.0f;
	MC_Ident_SetMea(idm, iqm, vdm, vqm, omegae);

	if(0 > x_cnt)
	{
		x_cnt = 0;
	}
	else if(X_TIM1 >= x_cnt)	// id ramp up
	{
		if(idr < (ial_1 - ial_r))
		{
			idr = idr + ial_r;
		}
		else
		{
			idr = ial_1;
		}
		omegae = 0.0f;
		SystemPhase = 0.0f;
	}
	else if(X_TIM2 > x_cnt)		// first measurement cycle
	{
		MC_Ident_UpdAcc();
		idr = ial_1;
		omegae = 0.0f;
		SystemPhase = 0.0f;
	}
	else if(X_TIM2 == x_cnt)	// first measurement acknowledge
	{
		MC_Ident_Step1();
		idr = ial_1;
		omegae = 0.0f;
		SystemPhase = 0.0f;
	}
	else if(X_TIM3 >= x_cnt)	// id ramp dw
	{
		if(idr > (ial_2 + ial_r))
		{
			idr = idr - ial_r;
		}
		else
		{
			idr = ial_2;
		}
		omegae = 0.0f;
		SystemPhase = 0.0f;
	}
	else if(X_TIM4 > x_cnt)		// second measurement cycle
	{
		MC_Ident_UpdAcc();
		idr = ial_2;
		omegae = 0.0f;
		SystemPhase = 0.0f;
	}
	else if(X_TIM4 == x_cnt)	// second measurement acknowledge
	{
		MC_Ident_Step2();
		idr = ial_2;
		omegae = 0.0f;
		SystemPhase = 0.0f;
	}
	else if(X_TIM5 >= x_cnt)	// speed ramp
	{
		idr = ial_2;
		if(omegae < (iden_om - iden_or))
		{
			omegae = omegae + iden_or;
		}
		else
		{
			omegae = iden_om;
		}
		SystemPhase = McrpLibf_AngleNrm(SystemPhase + (omegae * sam_per_s));
	}
	else if(X_TIM6 > x_cnt)		// third measurement cycle
	{
		MC_Ident_UpdAcc();
		idr = ial_2;
		omegae = iden_om;
		SystemPhase = McrpLibf_AngleNrm(SystemPhase + (omegae * sam_per_s));
	}
	else if(X_TIM6 == x_cnt)	// third measurement acknowledge
	{
		MC_Ident_Step3();
		idr = ial_2;
		omegae = iden_om;
		SystemPhase = McrpLibf_AngleNrm(SystemPhase + (omegae * sam_per_s));
	}
	else if(X_TIM7 >= x_cnt)	// id ramp dw
	{
		if(idr > (ial_3 + ial_r))
		{
			idr = idr - ial_r;
		}
		else
		{
			idr = ial_3;
		}
		omegae = iden_om;
		SystemPhase = McrpLibf_AngleNrm(SystemPhase + (omegae * sam_per_s));
	}
	else if(X_TIM8 > x_cnt)		// fourth measurement cycle
	{
		MC_Ident_UpdAcc();
		idr = ial_3;
		omegae = iden_om;
		SystemPhase = McrpLibf_AngleNrm(SystemPhase + (omegae * sam_per_s));
	}
	else if(X_TIM8 == x_cnt)	// fourth measurement acknowledge
	{
		MC_Ident_Step4();
		idr = 0.0f;
		omegae = 0.0f;
		SystemPhase = 0.0f;
	}
	else						// waiting end
	{
		idr = 0.0f;
		omegae = 0.0f;
		SystemPhase = 0.0f;
	}
	if(0x7FFFFFFF > x_cnt)
	{
		x_cnt++;
	}
}
#endif	// MOT_IDENT

/******************************************************************************
Function:		MC_WorkMan
Description:	Working modes management function (speed ramps etc.)
Input:			nothing
Output:			nothing
Modifies:		internal speed reference
******************************************************************************/
void MC_WorkMan(void)
{
	float32_t 		f32a;
#ifdef RAMP_CL0			// ramp clamp if heavy speed error
	float32_t 		f32b;
#endif	// RAMP_CL0
	static int16_t	rif_mem;

	// alarm management
	if(0 != UIF_R.var.all)
	{
		MC_StopOut();
		RES_ALIGNED
		RES_STARTED
		SET_STOPPED
		RES_DECEL
		rpmrif_x = 0.0f;
		rpmrif_y = 0.0f;
		omrif = 0.0f;
		ramp_status = STOP_0;
		if(TRIP_ALL == UIF_R.var.all)
		{
			if(	(0 == UIF_W.var.rif) ||
				((0 > rif_mem) && (0 < UIF_W.var.rif)) ||
				((0 < rif_mem) && (0 > UIF_W.var.rif)) )
			{
				UIF_R.var.all = 0;
				RES_ALRM_ON
			}
		}
		rif_mem = UIF_W.var.rif;
		SET_NORM_MODE		// on alarm, normal mode is reestablished
		RES_STA_BSY
		SET_END_NOK
		RES_GUI_TRI
		RES_GUI_STP
		return;
	}
	rif_mem = UIF_W.var.rif;

	// working mode requests management
	if(NOT_OUT_ENABLED)	// all requests are taken into consideration only if COM_OFF
	{
#ifdef CPI_TUN
		if(CURPI_TUN_REQ)
		{
			SET_CURPI_TUN
		}
		else
#endif	// CPI_TUN
#ifdef CPI_AUT
		if(CURPI_AUT_REQ)
		{
			SET_CURPI_AUT
		}
		else
#endif	// CPI_AUT
#ifdef MOT_IDENT
		if(AUTO_IDEN_REQ)
		{
			SET_AUTO_IDEN
		}
#endif	// MOT_IDENT
		if(NOT_NORM_MODE)
		{
			RES_STARTED
			SET_STOPPED
			RES_DECEL
			rpmrif_x = 0.0f;
			rpmrif_y = 0.0f;
			omrif = 0.0f;
			ramp_status = STOP_0;
			if(NORM_MODE_REQ)
			{
				UIF_R.var.kpi = ((uint16_t)KP_CUR);
				UIF_R.var.kii = ((uint16_t)KI_CUR);
				UIF_R.var.rs = ((uint16_t)R_STA);
				UIF_R.var.ls = ((uint16_t)L_SYN);
				UIF_R.var.fl = ((uint16_t)PM_FLX);
				RES_STA_BSY
				SET_NORM_MODE
			}
		}
	}

#ifdef CPI_TUN
	// Manual current PI tuning mode (here normal eeprom parameters are used)
	if(CURPI_TUN)
	{
		if(NOT_OUT_ENABLED)
		{
			if(GUI_TRI)
			{
				SET_STA_BSY
				ratio = 0.01f * UIF_W.var.cra;
				if(0.0f > ratio)
				{
					ratio = 0.0f;
				}
				else if(1.0f < ratio)
				{
					ratio = 1.0f;
				}
				rbuf = ((uint16_t *)(outbuf.ss));
				wbuf = ((uint16_t *)(outbuf1.ss));
				pbuf = rbuf;
				pit_c = 0;
				MC_StartOut();
			}
		}
		else if(GUI_STP)
		{
			MC_StopOut();
			SET_END_NOK
			RES_STA_BSY
		}
		else if(130 <= pit_c)	// 130 surely > MAX_SAM_CNT
		{
			MC_StopOut();
			RES_END_NOK
			RES_STA_BSY
		}
		else
		{
			MC_StartOut();
		}
		RES_GUI_TRI
		RES_GUI_STP
		return;
	}
#endif	// CPI_TUN

#ifdef CPI_AUT
	// Automatic current PI tuning mode
	if(CURPI_AUT)
	{
		if(NOT_OUT_ENABLED)
		{
			if(GUI_TRI)
			{
				SET_STA_BSY
				PIT_IniVar(i_max * I_PIT_RATIO, vbusf, sam_per_s);
				v_req = 0.0f;
				pitun_status = R_UP;
				MC_StartOut();
			}
		}
		else if(GUI_STP)
		{
			MC_StopOut();
			SET_END_NOK
			RES_STA_BSY
		}
		else if(R_EN <= pitun_status)
		{
			MC_StopOut();
			UIF_R.var.kpi = AMP_KPCUR * PIT_ReadKP();
			UIF_R.var.kii = (AMP_KICUR * sam_fre_Hz) * PIT_ReadKI();
			RES_END_NOK
			RES_STA_BSY
		}
		else
		{
			SET_STA_BSY
			MC_StartOut();
		}
		RES_GUI_TRI
		RES_GUI_STP
		return;
	}
#endif	// CPI_AUT

#ifdef MOT_IDENT
	// Motor parameters auto identification mode
	if(AUTO_IDEN)
	{
		if(NOT_OUT_ENABLED)
		{
			if(GUI_TRI)
			{
				SET_STA_BSY
				INI_IDENT
				MC_StartOut();
			}
		}
		else if(GUI_STP)
		{
			MC_StopOut();
			SET_END_NOK
			RES_STA_BSY
		}
		else if(IDENT_ENDED)
		{
			MC_StopOut();
			MC_Ident_CalMotPar(&iden_rs, &iden_ls, &iden_fl);
			UIF_R.var.rs = ((uint16_t)(iden_rs * ((float32_t)OHM_RES)));
			UIF_R.var.ls = ((uint16_t)(iden_ls * ((float32_t)HEN_RES)));
			UIF_R.var.fl = ((uint16_t)(iden_fl * ((float32_t)WEB_RES)));
			RES_END_NOK
			RES_STA_BSY
		}
		else
		{
			SET_STA_BSY
			MC_StartOut();
		}
		RES_GUI_TRI
		RES_GUI_STP
		return;
	}
#endif	// MOT_IDENT

	// From here normal mode management
	RES_END_NOK

	// preliminary reference management
#ifdef	ROT_REV
	f32a = -UIF_W.var.rif;
#else	// ROT_REV
	f32a = UIF_W.var.rif;
#endif	// ROT_REV
	if(f32a >= rpm_min)
	{
		if(f32a > rpm_max)
		{
			f32a = rpm_max;
		}
	}
	else if(-rpm_min >= f32a)
	{
		if(-rpm_max > f32a)
		{
			f32a = -rpm_max;
		}
	}
	else
	{
		f32a = 0.0f;
#ifdef DEBUG_OPENLOOP
		if(DEB_OPNLP)
		{
			MC_StopOut();
			rpmrif_x = 0.0f;
			rpmrif_y = 0.0f;
			omrif = 0.0f;
			ramp_status = STOP_0;
			RES_STARTED
			SET_STOPPED
			RES_DECEL
			return;
		}
#endif	// DEBUG_OPENLOOP
	}

#ifdef DCCUR_SETTING
	// DC current vector setting
	if(DCCUR_SET)
	{
		rpmrif_x = 0.0f;
		rpmrif_y = 0.0f;
		omrif = 0.0f;
		ramp_status = STOP_0;
		RES_STARTED
		SET_STOPPED
		RES_DECEL
		if(0.0f == f32a)
		{
			MC_StopOut();
		}
		else
		{
			MC_StartOut();
		}
		return;
	}
#endif	// DCCUR_SETTING

	// ramp management state-machine
	switch(ramp_status)
	{
		case START_P:
			RES_STOPPED
			RES_DECEL
			MC_StartOut();
			rpmrif_x = rpm_min;
			rpmrif_y = rpm_min;
			if(f32a < rpm_min)
			{
				ramp_status = STOP_W;
				if(-rpm_min >= f32a)
				{
					rpmrif_x = -rpm_min;
				}
				else
				{
					rpmrif_x = 0.0f;
				}
				break;
			}
			else if(NOT_STARTED)
			{
				break;
			}
			ramp_status = GO_P;
			break;
		case GO_P:
			RES_STOPPED
			MC_StartOut();
			if(f32a < rpm_min)
			{
				if(rpmrif_y <= rpm_min)
				{
					rpmrif_y = 0.0f;
					ramp_status = STOP_W;
					if(-rpm_min >= f32a)
					{
						rpmrif_x = -rpm_min;
					}
					else
					{
						rpmrif_x = 0.0f;
					}
					break;
				}
				rpmrif_x = rpm_min;
			}
			else
			{
				rpmrif_x = f32a;
			}
			f32a = rpmrif_y;
			if(f32a < (rpmrif_x - r_acc))
			{
				RES_DECEL
				f32a = f32a + r_acc;
			}
			else if(f32a > (rpmrif_x + r_dec))
			{
				SET_DECEL
				if(NOT_RAMPLIM)
				{
					f32a = f32a - r_dec;
				}
			}
			else
			{
				RES_DECEL
				f32a = rpmrif_x;
			}
#ifdef RAMP_CL0			// ramp clamp if heavy speed error
			f32b = mec_rpm + MAXERR_RPM;
			if((f32a > f32b) && (f32b > rpm_min))
			{
				f32a = f32b;
			}
#endif	// RAMP_CL0
			rpmrif_y = f32a;
			break;
		case START_N:
			RES_STOPPED
			RES_DECEL
			MC_StartOut();
			rpmrif_x = -rpm_min;
			rpmrif_y = -rpm_min;
			if(-rpm_min < f32a)
			{
				ramp_status = STOP_W;
				if(f32a >= rpm_min)
				{
					rpmrif_x = rpm_min;
				}
				else
				{
					rpmrif_x = 0.0f;
				}
				break;
			}
			else if(NOT_STARTED)
			{
				break;
			}
			ramp_status = GO_N;
			break;
		case GO_N:
			RES_STOPPED
			MC_StartOut();
			if(-rpm_min < f32a)
			{
				if(-rpm_min <= rpmrif_y)
				{
					rpmrif_y = 0.0f;
					ramp_status = STOP_W;
					if(f32a >= rpm_min)
					{
						rpmrif_x = rpm_min;
					}
					else
					{
						rpmrif_x = 0.0f;
					}
					break;
				}
				rpmrif_x = -rpm_min;
			}
			else
			{
				rpmrif_x = f32a;
			}
			f32a = rpmrif_y;
			if(f32a > (rpmrif_x + r_acc))
			{
				RES_DECEL
				f32a = f32a - r_acc;
			}
			else if(f32a < (rpmrif_x - r_dec))
			{
				SET_DECEL
				if(NOT_RAMPLIM)
				{
					f32a = f32a + r_dec;
				}
			}
			else
			{
				RES_DECEL
				f32a = rpmrif_x;
			}
#ifdef RAMP_CL0			// ramp clamp if heavy speed error
			f32b = mec_rpm - MAXERR_RPM;
			if((f32a < f32b) && (-rpm_min > f32b))
			{
				f32a = f32b;
			}
#endif	// RAMP_CL0
			rpmrif_y = f32a;
			break;
		case STOP_W:
			RES_STARTED
			RES_DECEL
			rpmrif_y = 0.0f;
			MC_StartOut();
			if(f32a >= rpm_min)
			{
				rpmrif_x = rpm_min;
				if(STOPPED)
				{
					ramp_status = START_P;
				}
			}
			else if(-rpm_min >= f32a)
			{
				rpmrif_x = -rpm_min;
				if(STOPPED)
				{
					ramp_status = START_N;
				}
			}
			else
			{
				rpmrif_x = 0.0f;
				if(STOPPED)
				{
					ramp_status = STOP_0;
				}
			}
			break;
		case STOP_0:
		default:
			MC_StopOut();
			SET_STOPPED
			RES_STARTED
			RES_DECEL
			rpmrif_x = 0.0f;
			rpmrif_y = 0.0f;
			if(f32a >= rpm_min)
			{
				ramp_status = START_P;
			}
			else if(-rpm_min >= f32a)
			{
				ramp_status = START_N;
			}
			else
			{
				ramp_status = STOP_0;
			}
			break;
	}

	// ramp output update and conversion in rad/sec
	omrif = rpmrif_y * krpmocp;
	omrif_abs = (0.0f > omrif? -omrif: omrif);

	// trip limit values calc
#ifdef RAMP_CL0
	f32b = omrif_abs * T_MARGL;
	f32a = omrif_abs - maxerr - r_marg;
	if(f32a > f32b)
	{
		f32a = f32b;
	}
	if(f32a < min_speed_trip)
	{
		min_s_trip = min_speed_trip;
	}
	else
	{
		min_s_trip = f32a;
	}
	f32b = omrif_abs * T_MARGH;
	f32a = omrif_abs + maxerr + r_marg;
	if(f32a < f32b)
	{
		f32a = f32b;
	}
	if(f32a > max_speed_trip)
	{
		max_s_trip = max_speed_trip;
	}
	else
	{
		max_s_trip = f32a;
	}
#endif	// RAMP_CL0

}

#ifdef FLUXWEAK
/******************************************************************************
Function:		MC_FluxWeak
Description:	flux weakening algorithm
Input:			voltage margin vmf,
				maximum current direct imax,
				reference current direct iref
				new direct current reference address *i
Output:			nothing
Modifies:		new direct current reference i
******************************************************************************/
void MC_FluxWeak(float32_t vmf, float32_t imax, float32_t iref, float32_t *i)
{

	if((HVBUS_LIM < vbus) || (0.0f > vmf))
	{
		if(-imax < (*i))
		{
			(*i) =  (*i) - intid_rdw;
		}
	}
	else if((*i) > (iref + intid_rdw))
	{
		(*i) = (*i) - intid_rdw;
	}
	else if((*i) < (iref - intid_rup))
	{
		(*i) = (*i) + intid_rup;
	}
	else
	{
		(*i) = iref;
	}

}
#endif	// FLUXWEAK

#ifdef TRIP_CONTROL
/******************************************************************************
Function:		MC_TripCon
Description:	loosing of control of the phase estimation algorithm check
Input:			nothing
Output:			nothing
Note:			uses some global variables
Modifies:		global alarm, if needed
******************************************************************************/
void MC_TripCon(void)
{
	if(3 < startup_cnt)
	{
		if(MAX_TRIP <= trip_cnt)
		{
			MC_StopOut();
			UIF_R.var.all = TRIP_ALL;
			SET_ALRM_ON
			trip_cnt = 0;
		}
		else if(GO_P == ramp_status)
		{
			if((Speed_est < min_s_trip) || (Speed_est > max_s_trip))
			{
				trip_cnt = trip_cnt + 1;
			}
			else if(1 < trip_cnt)
			{
				trip_cnt = trip_cnt - 2;
			}
		}
		else if(GO_N == ramp_status)
		{
			if((-min_s_trip < Speed_est) || (-max_s_trip > Speed_est))
			{
				trip_cnt = trip_cnt + 1;
			}
			else if(1 < trip_cnt)
			{
				trip_cnt = trip_cnt - 2;
			}
		}
	}
	else
	{
		trip_cnt = 0;
	}
}
#endif	// TRIP_CONTROL

/******************************************************************************
Function:		MC_ConInt
Description:	control interrupt
Input:			nothing
Output:			nothing
Note:			this function is called every sampling instant, and performs
           		the motor control
******************************************************************************/
void MC_ConInt(void)
{
	float32_t	f32a;

	IR(S12AD, S12ADI0) = 0;		// reset interrupt request

	// timing debug
	SET_DL3

	Encoder_Read(); // read the value from the encoder
	// enables lower priority interrupts (serial communication)
	set_ipl(SCI_INT_P - 1);
	setpsw_i();					// enables other interrupts

	// currents management
	MC_ADMan();					// modifies the global variables ium, ivm, iwm, iam, ibm
	if(COM_ON)
	{
		// current transformations (previous angle)
		McrpLibf_alphabeta_dq(iam, ibm, &idm, &iqm);

		// voltage measurements (related to currents measurements)
		vam = vac;
		vbm = vbc;
		vdm = vdc;
		vqm = vqc;
	}	// if(COM_ON)
	else
	{
		iqm = idm = 0.0f;
		vbm = vam = 0.0f;
		vqm = vdm = 0.0f;
	}

	// read vbus measurement
	vbus_ad = AN3_VB;
	vbus = KADV * vbus_ad;				// dc bus voltage in Volt

	// internal variables update after encoder reading
	om_eme = c_poli * Encoder_GetMechSpeed();
	if(NOT_ALIGNED)
	{
		SystemPhase = 0.0f;				// corrected electrical angle position (from encoder)
	}
	else
	{
		ele_ang = McrpLibf_AngleNrm(c_poli * Encoder_GetMechAngle());	// electrical angle	[rad]
		tele_ang = McrpLibf_AngleNrm(ele_ang + off_ang);	// corrected electrical angle	[rad]
		SystemPhase = tele_ang;			// corrected electrical angle position (from encoder)
	}
	omegae = om_eme;	
	// A/D converter re-configuration single scan conversion of remaining analog inputs
	S12AD.ADCSR.WORD = 0x0000; 			// ADIE, ADHSC, TRGE are 0
	SEL_VEX_AD							// channels select (0, 1, 2 and 3)
	S12AD_START

	// angle management
	McrpLibf_AngleSet(SystemPhase);		// from here new angle (sin and cos) (conversions etc.)

	// voltages references and duty cycles calc
	if(COM_ON)
	{

#ifdef CPI_AUT
		if(CURPI_AUT)
		{
			MC_CurPIAuto();	// produces directly voltages references vdc, vqc
		}		// if(CURPI_AUT)
		else
#endif	// CPI_AUT
		{	// normal case: idr, iqr come from previous cycle

			// current PI controls
			MC_IdIqPI(idr, idm, iqr, iqm, vfmax, &vdc, &vqc, &idint, &iqint);
		}	// end of normal case

		// voltage transformations (actual angle)
		McrpLibf_dq_alphabeta(vdc, vqc, &vac, &vbc);
		McrpLibf_alphabeta_uv(vac, vbc, &vuc, &vvc);
		vwc = -vuc - vvc;

#ifdef REFCOMP
		McrpLibf_dq_alphabeta(idr, iqr, &iar, &ibr);
		McrpLibf_alphabeta_uv(iar, ibr, &iur, &ivr);
		iwr = -iur - ivr;
#endif	// REFCOMP

		// pwm output generation
		MC_PWMGen(); // uses vuc, vvc, vwc and vbus to calculate duty_u, duty_v, duty_w; updates vuc, vvc, vwc values

		// pwm output registers setting (HERE WE SHOULD BE BEFORE PWM CREST!)
		SET_PWM(duty_u, duty_v, duty_w)

		// output voltages in (alpha, beta) frame update after modulation clamping
		McrpLibf_uvw_alphabeta(vuc, vvc, vwc, &vac, &vbc);
		McrpLibf_alphabeta_dq(vac, vbc, &vdc, &vqc);	// for visualization!!

	}
	else	// else of if(COM_ON)
	{
#ifdef REFCOMP
		iar = ibr = 0.0f;
		iur = ivr = iwr = 0.0f;
#endif	// REFCOMP
		iqint = idint = 0.0f;
		vqc = vdc = 0.0f;
		vbc = vac = 0.0f;
		vwc = vvc = vuc = 0.0f;
		duty_u = duty_v = duty_w = halfperdead;
		cr_status = CR_NO;

		// pwm output registers setting (if outputs enabled, this means the lower arm always on (used for BOOTSTRAP))
		SET_PWM(halfperdead, halfperdead, halfperdead)
	}

#ifdef VB_RAMP
	// ramp limitation depending on vbus
	if(DECEL)
	{
		if(VBUS_LIMH <= vbus)
		{
			SET_RAMPLIM
		}
		else if(VBUS_LIML > vbus)
		{
			RES_RAMPLIM
		}
	}
	else
	{
		RES_RAMPLIM
	}
#endif	// VB_RAMP

	// current references calculation, startup, position and speed management
	// the outputs of this block are: next system phase, next d and q current references
	if(COM_ON)
	{

		// measured current and voltages vectors amplitude
		//amp_i = sqrtf((iam * iam) + (ibm * ibm));		// McrpLibf_xy_rt(iam, ibm, &amp_i, &arg_i);
		amp_v = sqrtf((vam * vam) + (vbm * vbm));		// McrpLibf_xy_rt(vam, vbm, &amp_v, &arg_v);

#ifdef CPI_TUN
		if(CURPI_TUN)
		{
			MC_CurPITun();
		}
		else
#endif	// CPI_TUN

#ifdef MOT_IDENT
		if(AUTO_IDEN)
		{
			MC_MotIdent();
		}
		else
#endif	// MOT_IDENT

#ifdef DCCUR_SETTING
		if(DCCUR_SET)
		{
			f32a = McrpLibf_AngleNrm(PHA_OFF * ((float32_t)(TWOPI / 360)));
			McrpLibf_rt_xy(i_start, f32a, &idr, &iqr);
			SystemPhase = 0.0f;
		}
		else
#endif	// DCCUR_SETTING

		{	// normal behaviour block beginning

#ifdef FLUXWEAK
			// voltage margin for flux weakening calc
			vmarg = (vfmax * K_MARGV_FW) - amp_v;
			if((0.0f < omegae) && (0.0f >= vqc))
			{
				vmarg = vmarg - vqc;
			}
			else if((0.0f > omegae) && (0.0f <= vqc))
			{
				vmarg = vmarg + vqc;
			}
			if((0.0f > vmarg) && (((0.0f < omegae) && (0.0f >= iqr)) || ((0.0f > omegae) && (0.0f <= iqr))))
			{
				vmarg = 0.0f;
			}
#endif	// FLUXWEAK

			// speed reference filter
			//f_omrif = f_omrif + ((omrif - f_omrif) * omr_ukt);	// speed reference filtering
			f_omrif = rpmrif_x*krpmocp;
			// permanent magnets flux vector estimation
#ifndef APP_INT
			McrpLibf_FluxEst(vam, vbm, iam, ibm);
			FluxPhase = McrpLibf_FluxEst_GetPhase();
			Speed_est = McrpLibf_FluxEst_GetSpeed();
#else	// APP_INT
//			McrpLibf_FluxEstA(vam, vbm, iam, ibm);
//			FluxPhase = McrpLibf_FluxEstA_GetPhase();
//			Speed_est = McrpLibf_FluxEstA_GetSpeed();
#endif	// APP_INT

			// phase compensation and optional phase offset
//			Phase_est = McrpLibf_AngleNrm(FluxPhase + (comptime * Speed_est) + Phase_offset);

			// position loss control
#ifdef TRIP_CONTROL
			MC_TripCon();
#endif	// TRIP_CONTROL

			// STARTUP
			if(STOP_W == ramp_status)	// stop control
			{
				is_ref = idr = iqr = 0.0f;
				startup_cnt = 0;
				SET_STOPPED
				RES_ALIGNED
				SystemPhase = 0.0f;
				omf = omegae = 0.0f;
			}
			if(NOT_ALIGNED)
			{
				if(startup_cnt == 0)	// alignment
				{
					SystemPhase = 0.0f;
					omf = omegae = 0.0f;
					iqr = 0.0f;
					if(idr < i_start)
						idr += is_rup;
					else
					{
						idr = i_start;
						startup_cnt = 1;
					}
				}
				else if(startup_cnt < STAB_TIME)	// waiting stabilization
				{
					iqr = 0.0f;
					idr = i_start;
					++ startup_cnt;
				}
				else if(startup_cnt == STAB_TIME)	// offset angle setting
				{
					off_ang = McrpLibf_AngleNrm(-c_poli * Encoder_GetMechAngle());	// electrical angle	[rad]
					SET_ALIGNED
					iqr = 0.0f;
					idr = 0.0f;
					startup_cnt = 0;
					trip_cnt = 0;
					SET_STARTED	
				}
			} //aligned
			// STARTUP IS FINISHED, NORMAL CONTROL
			else		// normal sensorless control
			{

				// phase and speed update ESTA PARTE ESTA COMENTADA
				//SystemPhase = Phase_est; SE ACTUALIZO AL PRINCIPIO
				//omegae = Speed_est;

				// id reference calc
				idrvar = 0.0f;

				// idr and iqmax calc
#ifdef FLUXWEAK
				MC_FluxWeak(vmarg, id_max, idrvar, &idr);
#else	// FLUXWEAK
				idr = idrvar;
#endif	// FLUXWEAK
				iqmax = MC_VecComp(i_max, idr);

				// speed control
#ifdef VB_RAMP
				if(RAMPLIM)
				{
					errint = errint * K_RAMPLIM;
				}
#endif	// VB_RAMP
				//iqr = MC_SpeedPI(f_omrif, omegae, iqmax, &errint); //original
				if(f_omrif>= 100){ //prueba de anillo de corriente
					iqr= 0.1f;
				}else{
					iqr = 0;
				}
				//iqr = 0.1f;
			}	// end of normal control

		}	// end of startup and normal behaviour

		// visualization filters
		//idmf = idmf + ((idm - idmf) * visu_ukt);	// d current
		//iqmf = iqmf + ((iqm - iqmf) * visu_ukt);	// q current
		//vdmf = vdmf + ((vdm - vdmf) * visu_ukt);	// d voltage
		//vqmf = vqmf + ((vqm - vqmf) * visu_ukt);	// q voltage
		//omf = omf + ((omegae - omf) * visu_ukt);	// angular speed
		idmf = iqr;	// d current
		iqmf = iqm;	// q current
		vdmf = vdm; 	// d voltage
		vqmf = vqm; 	// q voltage
		omf = omegae; 	// angular speed

	}	// if(COM_ON)

	else
	{

		// position and speed reset
		SystemPhase = 0.0f;
		omegae = 0.0f;

		// speed PI integral term reset
		errint = 0.0f;

		// current references reset
		is_ref = iqr = idr = 0.0f;

		// measurements reset
		amp_v = 0.0f;
		//arg_v = SystemPhase;
		//amp_i = 0.0f;
		//arg_i = SystemPhase;

		// voltage margin reset
		vmarg = vfmax;

		// phase detection variables reset
		Phase_est = FluxPhase = SystemPhase;
		Speed_est = omegae;
		RES_ALIGNED
#ifndef APP_INT
		McrpLibf_FluxEst_SetFlux(FluxPhase, iam, ibm, omegae);
#else	// APP_INT
//		McrpLibf_FluxEstA_SetFlux(FluxPhase, vam, vbm, iam, ibm, omegae);
#endif	// APP_INT
		trip_cnt = 0;

		// startup variables and flags reset
		startup_cnt = 0;
		idrvar = 0.0f;
		iqmax = 0.0f;

		// filters memories reset
		f_omrif = 0.0f;
		iqmf = idmf = 0.0f;
		vqmf = vdmf = 0.0f;
		omf = 0.0f;

#ifdef RAMPLIM_VB
		RES_RAMPLIM0
#endif	// RAMPLIM_VB
	}	// else of if(COM_ON)

	// bus voltage filter and vfmax calc
	if(vbus < vbus_minf)
	{
		vbus_minf = vbus_minf + ((vbus - vbus_minf) * vb_ukt_fast); // fast decrease
	}
	else if(vbus > vbus_minf)
	{
		vbus_minf = vbus_minf + ((vbus - vbus_minf) * vb_ukt_slow);	// slow increase
	}
	vfmax = vbus_minf * k_vout;

	// bus voltage filter (visualization)
	vbusf = vbusf + ((vbus - vbusf) * visu_ukt);		// DC bus voltage

#ifdef OSC_WIN
	// management of oscilloscope window in GUI
	if(NORM_MODE)
	{
		if(CSELD)
		{
			sample = ium * 1000.0f;				// mA
		}
		else
		{
			sample = SystemPhase * 1000.0f;
		}
		if(COM_ON)
		{
			if(NOT_TRGED)
			{
				if(selmem != ((uint16_t)UIF_W.var.sel))
				{
					selmem = (uint16_t)UIF_W.var.sel;
					bcntm = selmem >> 8;
					if(0 == (selmem & 0x00FF))
					{
						RES_CSELD
					}
					else
					{
						SET_CSELD
					}
				}
				if(	((0.0f < omegae) && (0.0f > phmem) && (0.0f <= SystemPhase)) ||
					((0.0f > omegae) && (0.0f < phmem) && (0.0f >= SystemPhase)) )
				{
					SET_TRGED
					bcnt1 = 1;
					bcnt0 = 1;
					wbuf[0] = sample;
				}
			}
			else if(MAX_SAM_CNT > bcnt0)
			{
				if(bcnt1 < bcntm)
				{
					bcnt1++;
				}
				else
				{
					bcnt1 = 1;
					wbuf[bcnt0++] = sample;
				}
			}
			else if(NOT_RBUSY)
			{
				pbuf = rbuf;
				rbuf = wbuf;
				wbuf = pbuf;
				RES_TRGED
			}
		}	// COM_ON
		else
		{
			RES_TRGED
			if(selmem != ((uint16_t)UIF_W.var.sel))
			{
				selmem = (uint16_t)UIF_W.var.sel;
				bcntm = selmem >> 8;
				if(0 == (selmem & 0x00FF))
				{
					RES_CSELD
				}
				else
				{
					SET_CSELD
				}
			}
			if(MAX_SAM_CNT > bcnt0)
			{
				wbuf[bcnt0++] = 0;
			}
			else if(NOT_RBUSY)
			{
				pbuf = rbuf;
				rbuf = wbuf;
				wbuf = pbuf;
				bcnt0 = 0;
			}
		}	// COM_OFF
		phmem = SystemPhase;
	}	// NORM_MODE
#endif	// OSC_WIN

	// wait end of conversion of second group of analog inputs
	WAIT_S12AD
	// read variables

	// A/D converter re-configuration for triggered single scan conversion of currents
	SEL_CVB_AD						// channels select (0, 1, 2 and 3)
	S12AD.ADCSR.WORD = 0x1200; 		// ADIE, TRGE are 1

	// serial communication receive delay management
	UI_RTX232_RXDelMan();

	// main loop synchronization counter
	if(0 < cnt_int)
	{
		cnt_int--;
	}

	// timing debug
	CLR_DL3

	// interrupt end
	return;

}

/******************************************************************************
Function:		MC_FaultInt
Description:	interrupt routine for POE (/fault from power stage)
Input:			nothing
Output:			nothing
Note:			this function stops the PWM output
******************************************************************************/
void MC_FaultInt(void)
{
	MC_StopOut();					// all outputs disabled (at inactive level)
	UIF_R.var.all = FAULT_ALL;
	SET_ALRM_ON

	IEN(POE, OEI1) = 0;				// ICU.IER[0x15].BIT.IEN2 = 0;	// interrupt disable
	IR(POE, OEI1) = 0;				// reset interrupt request

	POE.ICSR1.BIT.PIE1 = 0;			// interrupt request disable
	if(0 != POE.ICSR1.BIT.POE0F)
	{
		POE.ICSR1.BIT.POE0F = 0;	// reset flag
	}

	POE.OCSR1.BIT.OIE1 = 0;			// interrupt request disable
	if(0 != POE.OCSR1.BIT.OSF1)
	{
		POE.OCSR1.BIT.OSF1 = 0;		// reset flag
	}
}

// EOF motorcontrol.c

