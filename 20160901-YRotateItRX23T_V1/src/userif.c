
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		userif.c
*	Project:	YRotateIt-RX23T (three shunts)
*	Revision:	1.0
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	User interface
*******************************************************************************/

#define	_USERIF_C		// insert define BEFORE including header files

#include "globdef.h"
#include "userif.h"
#include "ges_eqp.h"
#include "dtc_typedef.h"

/*
	SCI1:
		ERI1 218 0x0368 Lev	IER1B.IEN2	IPR218
		RXI1 219 0x036C Edg	IER1B.IEN3	IPR218	DTCER219
		TXI1 220 0x0370 Edg	IER1B.IEN4	IPR218	DTCER220
		TEI1 221 0x0374 Lev	IER1B.IEN5	IPR218
*/

// timing defines
#define	LED_TIME_FUN		( 20 )	// 20 main loops led blinking time
#define	LED_TIME_ALL		(  3 )	//  3 main loops led blinking time

// serial communication protocol
#define	N_CENT				( 0 )	// slave identification number
#define SEL_MAS				'?'		// master frame identification character (question)
#define SEL_SLA				'!'		// slave frame identification character (answer OK)
#define SEL_NOK				'#'		// slave frame identification character (answer NOK)
#define CCO					'c'		// Check Com operation code
#define NCO					'C'		// Check Com operation code (answer)
#define XCO					'd'		// Check Com operation code (answer, programs with auto-tuning)
#define YCO					'D'		// Check Com operation code (answer, programs with auto-tuning and new buffers)
#define RBO					'b'		// Read Byte operation code
#define RWO					'w'		// Read Word operation code
#define WWO					'W'		// Write Word operation code
#define RW3					'y'		// Read Word 3 operation code (parameter minimum values)
#define RW4					'z'		// Read Word 4 operation code (parameter maximum values)
#define RW5					'k'		// Read Word 5 operation code (measurement samples vector)
#define RW6					'j'		// Read Word 6 operation code (parameter default values)
#define	LEN_MIN				( 5 )	// shortest frame length, crc included
#define	LEN_DEL				( 2 )	// additional characters, data excluded, crc included
#define	IND_LEN				( 0 )	// string length index
#define	IND_SEL				( 1 )	// master/slave identificator index
#define	IND_ADS				( 2 )	// station address index
#define	IND_COD				( 3 )	// operation code index
#define IND_ADD				( 4 )	// data address index
#define	IND_ND				( 5 )	// data number index
#define	IND_ID				( 6 )	// data start index
#define	MAX_NWDATA			( 120 )	// max number of data words
#define	MAX_NBDATA			(2 * MAX_NWDATA)	// max number of data bytes
#define	LEN_MAX				(LEN_MIN + LEN_DEL + MAX_NBDATA)	// longest frame length, crc included

// baudrate define
#define BRR_VAL( X )		((uint8_t)((MTU_FRE_HZ / (32.0 * ( X ))) - 1.0))
#define BRR_9600			BRR_VAL( 9600 )			// 103=9600baud@32MHz (25=9600baud@8MHz)
#define BRR_19200			BRR_VAL( 19200 )		// 51=19200baud@32MHz
#define BRR_38400			BRR_VAL( 38400 )		// 25=38400baud@32MHz
#define BRR_57600			BRR_VAL( 57600 )		// 16=57600baud@32MHz
#define BRR_76800			BRR_VAL( 76800 )		// 12=76800baud@32MHz
#define BRR_115200			BRR_VAL( 115200 )		// 7=115200baud@32MHz

#define BAUDRATE			BRR_76800

// hardware related defines (a)
#define DTCE_TX232			DTCE(SCI1, TXI1)
#define DTCE_RX232			DTCE(SCI1, RXI1)
#define IEN_TXI_RTX232		IEN(SCI1, TXI1)			// transmit data register empty interrupt enable
#define IEN_TEI_RTX232		IEN(SCI1, TEI1)			// transmit end interrupt enable
#define IEN_RXI_RTX232		IEN(SCI1, RXI1)			// receive data register full interrupt enable
#define IEN_ERI_RTX232		IEN(SCI1, ERI1)			// receive error interrupt enable
#define IR_TXI_RTX232		IR(SCI1, TXI1)			// transmit data register empty interrupt flag
#define IR_TEI_RTX232		IR(SCI1, TEI1)			// transmit end interrupt flag
#define IR_RXI_RTX232		IR(SCI1, RXI1)			// receive data register full interrupt flag
#define IR_ERI_RTX232		IR(SCI1, ERI1)			// receive error interrupt flag
#define	RX232_REG			SCI1.RDRHL.BYTE.RDRL	// Receive Data Register
#define	TX232_REG			SCI1.TDRHL.BYTE.TDRL	// Transmit Data Register
#define RTX232_SCR			SCI1.SCR.BYTE			// Serial Control Register
#define RTX232_SSR			SCI1.SSR.BYTE			// Serial Status Register
#define RTX232_SCR_TXI		SCI1.SCR.BIT.TIE		// B7 Transmit Interrupt Enable
#define RTX232_SCR_RXI		SCI1.SCR.BIT.RIE		// B6 Receive Interrupt Enable
#define RTX232_SCR_TE		SCI1.SCR.BIT.TE			// B5 Transmit Enable
#define RTX232_SCR_RE		SCI1.SCR.BIT.RE			// B4 Receive Enable
#define RTX232_SCR_TEI		SCI1.SCR.BIT.TEIE		// B2 Transmit End Interrupt Enable
#define RTX232_SSR_TEND		SCI1.SSR.BIT.TEND		// B2 Transmit End Flag
#define RTX232_SSR_PER		SCI1.SSR.BIT.PER		// B3 Parity Error Flag
#define RTX232_SSR_FER		SCI1.SSR.BIT.FER		// B4 Framing Error Flag
#define RTX232_SSR_ORER		SCI1.SSR.BIT.ORER		// B5 Overrun Error Flag
#define RTX232_INIT			SCI1_Init();

#define RTX232_ERR_MSK		( 0x38 )	// B5 (ORER), B4 (FER), B3 (PER)
#define RTX232_TXE_MSK		( 0xA0 )	// B7 (TIE), B5 (TE)
#define RTX232_RXE_MSK		( 0x50 )	// B6 (RIE), B4 (RE)
#define RX232_TXENA			RTX232_SCR |= RTX232_TXE_MSK;	// { RTX232_SCR_TE = 1; RTX232_SCR_TXI = 1; }
#define RX232_RXENA			RTX232_SCR |= RTX232_RXE_MSK;	// { RTX232_SCR_RE = 1; RTX232_SCR_RXI = 1; }
#define RTX232_ON_MSK		( 0x030 )	// B5 (TE), B4 (RE)

#define	RTX232_ERR_ON		(0 != (RTX232_SSR & RTX232_ERR_MSK))
#define	RTX232_ORER_ON		(0 != SCI1.SSR.BIT.ORER)
#define	RTX232_FER_ON		(0 != SCI1.SSR.BIT.FER)
#define	RTX232_PER_ON		(0 != SCI1.SSR.BIT.PER)
#define	RX232_ON			(0 != RTX232_SCR_RE)
#define	TX232_ON			(0 != RTX232_SCR_TE)
#define	RTX232_ON			(0 != (RTX232_SCR & RTX232_ON_MSK))

#define STOP_DTC			{	DTC.DTCST.BYTE = 0x00;		\
								DTCE_TX232 = 0;				\
								DTCE_RX232 = 0;				}

#define START_DTC_RX		{	DTCE_RX232 = 1;				\
								DTC.DTCST.BIT.DTCST = 1;	}

#define START_DTC_TX		{	DTCE_TX232 = 1;				\
								DTC.DTCST.BIT.DTCST = 1;	}

#define	STOP_RTX232			{	IEN_ERI_RTX232 = 0;			\
								IEN_RXI_RTX232 = 0;			\
								IEN_TEI_RTX232 = 0;			\
								IEN_TXI_RTX232 = 0;			\
								RTX232_SCR = 0x00;			\
								while(RTX232_ERR_ON) {		\
									rtx232_dum = RX232_REG;	\
									RTX232_SSR = 0x00;	}	\
								IR_TXI_RTX232 = 0;			\
								IR_TEI_RTX232 = 0;			\
								IR_RXI_RTX232 = 0;			\
								IR_ERI_RTX232 = 0;			}
#define START_TX232			StartDTCTX();
#define	START_RX232			{	STOP_RTX232					\
								rtx232_cnt = 0;				\
								rtx232_num = 0;				\
								rtx232_tim = RTX232_BTIM;	\
								IR_RXI_RTX232 = 0;			\
								IR_ERI_RTX232 = 0;			\
								IEN_RXI_RTX232 = 1;			\
								IEN_ERI_RTX232 = 1;			\
								RX232_RXENA					}

#define	RTX232_LEN			( 256 )	// rx/tx buffer dim. (max frame length is 255)
#define	RTX232_BTIM			( 30 )	// max 1byte receive delay (sampling periods)

// constants
const uint8_t
	crc_tab[256] = {		// DOW CRC table
		  0,  94, 188, 226,  97,  63, 221, 131, 194, 156, 126,  32, 163, 253,  31,  65,
		157, 195,  33, 127, 252, 162,  64,  30,  95,   1, 227, 189,  62,  96, 130, 220,
		 35, 125, 159, 193,  66,  28, 254, 160, 225, 191,  93,   3, 128, 222,  60,  98,
		190, 224,   2,  92, 223, 129,  99,  61, 124,  34, 192, 158,  29,  67, 161, 255,
		 70,  24, 250, 164,  39, 121, 155, 197, 132, 218,  56, 102, 229, 187,  89,   7,
		219, 133, 103,  57, 186, 228,   6,  88,  25,  71, 165, 251, 120,  38, 196, 154,
		101,  59, 217, 135,   4,  90, 184, 230, 167, 249,  27,  69, 198, 152, 122,  36,
		248, 166,  68,  26, 153, 199,  37, 123,  58, 100, 134, 216,  91,   5, 231, 185,
		140, 210,  48, 110, 237, 179,  81,  15,  78,  16, 242, 172,  47, 113, 147, 205,
		 17,  79, 173, 243, 112,  46, 204, 146, 211, 141, 111,  49, 178, 236,  14,  80,
		175, 241,  19,  77, 206, 144, 114,  44, 109,  51, 209, 143,  12,  82, 176, 238,
		 50, 108, 142, 208,  83,  13, 239, 177, 240, 174,  76,  18, 145, 207,  45, 115,
		202, 148, 118,  40, 171, 245,  23,  73,   8,  86, 180, 234, 105,  55, 213, 139,
		 87,   9, 235, 181,  54, 104, 138, 212, 149, 203,  41, 119, 244, 170,  72,  22,
		233, 183,  85,  11, 136, 214,  52, 106,  43, 117, 151, 201,  74,  20, 246, 168,
		116,  42, 200, 150,  21,  75, 169, 247, 182, 232,  10,  84, 215, 137, 107,  53};

// variables
uint32_t
	//orer_cnt = 0,
	//per_cnt = 0,
	//fer_cnt = 0,
	//rxerrcnt = 0,
	//rxdelcnt = 0,
	dar_mem = 0;
uint16_t
	//baudrate = 0,
	rtx232_tim = 0,
	rtx232_cnt = 0,
	rtx232_num = 0;
uint16_t
	add_par,
	dta_par,
	sam_cnt;
uint8_t
	rtx232_buf[RTX232_LEN],		// rx/tx buffer (half duplex)
	rtx232_dum = 0;

#define DTC_VTBASEADD	( 0x00000800 )	// base address of DTC vector table (2K) (must be multiple of 1K)
//#define OFF_RXI1		( 0x036C )
//#define OFF_TXI1		( 0x0370 )
//#define DTC_RXI1ADD	( 0x00000B6C )	// 2954 (DTC_VTBASEADD+OFFRXI1), stack begins at 3072
//#define DTC_TXI1ADD	( 0x00000B70 )	// 2958 (DTC_VTBASEADD+OFFTXI1), stack begins at 3072
// need to define proper sections for the DTC transfer data structures addresses, or to
// assign these addresses with pragma address extension
// the data structures themselves can be allocated elsewhere

// WARNING: the structure definition is referred to FULL ADDRESS addressing mode!
st_DTC_TransData
	dtc_data_sciRX,
	dtc_data_sciTX;

// the address of the RXI1 vector (219) for DTC is (DTC_VTBASEADD+OFFRXI1)=0x0800+0x036C=0x0B6C
#pragma address dtc_vec_sciRX = 0x00000B6C
unsigned long
	dtc_vec_sciRX;		// = ((unsigned long)(& dtc_data_sciRX));

// the address of the TXI1 vector (220) for DTC is (DTC_VTBASEADD+OFFTXI1)=0x0800+0x0370=0x0B70
#pragma address dtc_vec_sciTX = 0x00000B70
unsigned long
	dtc_vec_sciTX;		// = ((unsigned long)(& dtc_data_sciTX));

/******************************************************************************
Function:		DTC_Init
Description:	Direct Transfer Control init
Input:			none
Output:			none
******************************************************************************/
void DTC_Init(void)
{
	// wake up peripheral
	WAKEUP_DTC
	DELAY1MS(1);

	// module stop
	DTC.DTCST.BYTE = 0x00;
		// B7, .., B1
	//DTC.DTCST.BIT.DTCST = 0;		// B0

	// set base address
	DTC.DTCVBR = ((void *) DTC_VTBASEADD);

	// set FULL ADDRESS mode
	DTC.DTCADMOD.BYTE = 0x00;
		// B7, .., B1
	//DTC.DTCADMOD.BIT.SHORT = 0;	// B0

	// reset RRS bit (read skip)
	DTC.DTCCR.BYTE = 0x08;
		// B7, B6, B5
	//DTC.DTCCR.BIT.RRS = 0;		// B4
		// B3, .., B0

	// status monitor
	//DTC.DTCSTS.WORD = 0x0000;
	//DTC.DTCSTS.BIT.ACT = 0;		// B15
		// B14, .., B8
	//DTC.DTCSTS.BIT.VECN = 0;		// B7, .., B0

	// receive transfer data structure init (full address selected)
    //dtc_data_sciRX.DW0.UL = ((uint32_t)(0x00000000) << 24U) | ((uint32_t)(0x00000008) << 16U);
	dtc_data_sciRX.DW0.REG.dmy = 0x0000;			// dummy init
	dtc_data_sciRX.DW0.REG.MRA.BYTE = 0x00;			// DTC mode register A
	//dtc_data_sciRX.DW0.REG.MRA.BIT.MD = 0;		// B7, B6 Transfer Mode Select (00 -> normal transfer)
	//dtc_data_sciRX.DW0.REG.MRA.BIT.SZ = 0;		// B5, B4 Data Transfer Size (00 -> byte)
	//dtc_data_sciRX.DW0.REG.MRA.BIT.SM = 0;		// B3, B2 Transfer Source Address Addressing Mode (00 -> fixed)
	dtc_data_sciRX.DW0.REG.MRB.BYTE = 0x08;			// DTC mode register B
	//dtc_data_sciRX.DW0.REG.MRB.BIT.CHNE = 0;		// B7 Chain Transfer Enable (0 -> disabled)
	//dtc_data_sciRX.DW0.REG.MRB.BIT.CHNS = 0;		// B6 Chain Transfer Select (0 -> continuous)
	//dtc_data_sciRX.DW0.REG.MRB.BIT.DISEL = 0;		// B5 Interrupt Select (0 -> end of transfer)
	//dtc_data_sciRX.DW0.REG.MRB.BIT.DTS = 0;		// B4 Transfer Mode Select (0 -> destination)
	//dtc_data_sciRX.DW0.REG.MRB.BIT.DM = 2;		// B3, B2 Transfer Destination Address Addressing Mode (10 -> incremented)
	dtc_data_sciRX.SAR = ((uint32_t)(& RX232_REG)); 		// DTC source address register
	dtc_data_sciRX.DAR = ((uint32_t)(& (rtx232_buf[2])));	// DTC destination address register
	//dtc_data_sciRX.DW3.UL = ((uint32_t)(0x00000000) << 16U) | ((uint32_t)(0x00000000U));
	dtc_data_sciRX.DW3.REG.CRA = 0x0000;			// DTC transfer count register A
	dtc_data_sciRX.DW3.REG.CRB = 0x0000;			// DTC transfer count register B

	// transmit transfer data structure init (full address selected)
    //dtc_data_sciTX.DW0.UL = ((uint32_t)(0x00000008) << 24U) | ((uint32_t)(0x00000000) << 16U);
	dtc_data_sciTX.DW0.REG.dmy = 0x0000;			// dummy init
	dtc_data_sciTX.DW0.REG.MRA.BYTE = 0x08;			// DTC mode register A
	//dtc_data_sciTX.DW0.REG.MRA.BIT.MD = 0;		// B7, B6 Transfer Mode Select (00 -> normal transfer)
	//dtc_data_sciTX.DW0.REG.MRA.BIT.SZ = 0;		// B5, B4 Data Transfer Size (00 -> byte)
	//dtc_data_sciTX.DW0.REG.MRA.BIT.SM = 2;		// B3, B2 Transfer Source Address Addressing Mode (10 -> incremented)
	dtc_data_sciTX.DW0.REG.MRB.BYTE = 0x00;			// DTC mode register B
	//dtc_data_sciTX.DW0.REG.MRB.BIT.CHNE = 0;		// B7 Chain Transfer Enable (0 -> disabled)
	//dtc_data_sciTX.DW0.REG.MRB.BIT.CHNS = 0;		// B6 Chain Transfer Select (0 -> continuous)
	//dtc_data_sciTX.DW0.REG.MRB.BIT.DISEL = 0;		// B5 Interrupt Select (0 -> end of transfer)
	//dtc_data_sciTX.DW0.REG.MRB.BIT.DTS = 0;		// B4 Transfer Mode Select (0 -> destination)
	//dtc_data_sciTX.DW0.REG.MRB.BIT.DM = 0;		// B3, B2 Transfer Destination Address Addressing Mode (00 -> fixed)
	dtc_data_sciTX.SAR = ((uint32_t) rtx232_buf);	// DTC source address register
	dtc_data_sciTX.DAR = ((uint32_t)(& TX232_REG)); // DTC destination address register
	//dtc_data_sciRX.DW3.UL = ((uint32_t)(0x00000000) << 16U) | ((uint32_t)(0x00000000U));
	dtc_data_sciTX.DW3.REG.CRA = 0x0000;			// DTC transfer count register A
	dtc_data_sciTX.DW3.REG.CRB = 0x0000;			// DTC transfer count register B

	// set transfer data start addresses in the DTC vector table
	dtc_vec_sciRX = ((uint32_t) (& dtc_data_sciRX));
	dtc_vec_sciTX = ((uint32_t) (& dtc_data_sciTX));
}

/******************************************************************************
Function:		SCI1_Init
Description:	Serial communication channel 1 init
Input:			none
Output:			none
******************************************************************************/
void SCI1_Init(void)
{

	// wakes up peripheral
	WAKEUP_SCI1
	DELAY1MS(1);

	// preliminary reset of some flags, disable any tx/rx, set internal clock
	SCI1.SCR.BYTE = 0x00;		// serial control register
	//SCI1.SCR.BIT.TIE = 0;		// B7 Transmit Interrupt Enable
	//SCI1.SCR.BIT.RIE = 0;		// B6 Receive Interrupt Enable
	//SCI1.SCR.BIT.TE = 0;		// B5 Transmit Enable
	//SCI1.SCR.BIT.RE = 0;		// B4 Receive Enable
	//SCI1.SCR.BIT.MPIE = 0;	// B3 Multi-Processor Interrupt Enable
	//SCI1.SCR.BIT.TEIE = 0;	// B2 Transmit End Interrupt Enable
	//SCI1.SCR.BIT.CKE = 0;		// B1, B0 Clock Enable (00 -> on chip baud rate generator)

	// ports setup (already OK from hwsetup)
	//PORTD.PMR.BIT.B5 = 0;			// pin14 PD5 RXPC I RXD1
	//PORTD.PMR.BIT.B3 = 0;			// pin16 PD3 TXPC O TXD1
	//PORTD.PODR.BIT.B3 = 1;		// pin16 PD3 TXPC O TXD1
	//PORTD.PDR.BIT.B5 = 0;			// pin14 PD5 RXPC I RXD1
	//PORTD.PDR.BIT.B3 = 1;			// pin16 PD3 TXPC O TXD1

	// pin assignment to the peripheral
	MPC.PWPR.BYTE = 0x00;			// enable writing BIT6
	MPC.PWPR.BYTE = 0x40;			// enable writing MPC registers
	MPC.PD5PFS.BYTE = 0x0A;			// pin14 PD5 RXPC I -> RXD1
	MPC.PD3PFS.BYTE = 0x0A;			// pin16 PD3 TXPC O -> TXD1
	MPC.PWPR.BYTE = 0x80;			// disable writing MPC registers

	// ports setup
	PORTD.PMR.BIT.B5 = 1;			// pin14 PD5 RXPC I RXD1
	PORTD.PMR.BIT.B3 = 1;			// pin16 PD3 TXPC O TXD1

	// select Asynchronous, N81, clock not divided
	SCI1.SMR.BYTE = 0x00;		// serial mode register
	//SCI1.SMR.BIT.CM = 0;		// B7 Communications Mode
	//SCI1.SMR.BIT.CHR = 0;		// B6 Character Length
	//SCI1.SMR.BIT.PE = 0;		// B5 Parity Enable
	//SCI1.SMR.BIT.PM = 0;		// B4 Parity Mode
	//SCI1.SMR.BIT.STOP = 0;	// B3 Stop Bit Length
	//SCI1.SMR.BIT.MP = 0;		// B2 Multi-Processor Mode
	//SCI1.SMR.BIT.CKS = 0;		// B1, B0 Clock Select (00 -> not divided, 01 -> divided by 4)

	// select Asynchronous, N81, clock not divided
	SCI1.SCMR.BYTE = 0xF2;		// smart card mode register
	//SCI1.SCMR.BIT.BCP2 = 1;	// B7
	//SCI1.SCMR.BIT.CHR1 = 1;	// B4
	//SCI1.SCMR.BIT.SDIR = 0;	// B3
	//SCI1.SCMR.BIT.SINV = 0;	// B2
	//SCI1.SCMR.BIT.SMIF = 0;	// B0

	// select base_clock=16, no noise cancellation
	SCI1.SEMR.BYTE = 0x00;
	//SCI1.SEMR.BIT.RXDESEL = 0;// B7 Asynchronous Start Bit Edge Detection Select
	//SCI1.SEMR.BIT.BGDM = 0;	// B6 Baud Rate Generator Double-Speed Mode Select
	//SCI1.SEMR.BIT.NFEN = 0;	// B5 Digital Noise Filter Function Enable
	//SCI1.SEMR.BIT.ABCS = 0;	// B4 Asynchronous Mode Base Clock Select
		// B3
	//SCI1.SEMR.BIT.BRME = 0;	// B2 Bit Rate Modulation Enable
		// B1
	//SCI1.SEMR.BIT.ACS0 = 0;	// B0 Asynchronous Mode Clock Source Select

	// select baud rate
	SCI1.BRR = BAUDRATE;
	//baudrate = BAUDRATE;

	// reset errors
	SCI1.SSR.BYTE = 0x00;
		// B7, B6
	//SCI1.SSR.BIT.ORER = 0;	// B5 Overrun Error Flag
	//SCI1.SSR.BIT.FER = 0;		// B4 Framing Error Flag
	//SCI1.SSR.BIT.PER = 0;		// B3 Parity Error Flag
	//SCI1.SSR.BIT.TEND = 0;	// B2 Transmit End Flag
	//SCI1.SSR.BIT.MPB = 0;		// B1 Multi-Processor
	//SCI1.SSR.BIT.MPBT = 0;	// B0 Multi-Processor Bit Transfer

	// select interrupts priority
	IPR(SCI1, ) = SCI_INT_P;	// SCI1 uses single IPR for all sources (ICU.IPR[218].BIT.IPR)
	DTC_Init();

	// wait at least 1 bit time
	DELAY1MS(1);

}

/******************************************************************************
Function:		b2w
Description:	2 bytes vector conversion in a word
Input:			two bytes vector *v
Output:			the short obtained by the union of the two bytes
******************************************************************************/
static uint16_t b2w(uint8_t *v)
{
	word_t	w;

	w.byte.h = v[0];
	w.byte.l = v[1];
	return(w.word);
}

/******************************************************************************
Function:		w2b
Description:	1 word conversion in a 2 bytes vector
Input:			the word to convert and the address of the result vector
Output:			the address of the result vector
Modifies:		the vector content
******************************************************************************/
static uint8_t *w2b(uint16_t w, uint8_t *v)
{
	word_t	x;

	x.word = w;
	v[0] = x.byte.h;
	v[1] = x.byte.l;
	return(v);
}

/******************************************************************************
Function:		cal_crc
Description:	DOW CRC calculation
Input:			the number of bytes to be taken into account,
				the vector containing these bytes
Output:			CRC
Note:			it uses the DOW CRC calculation table defined before
******************************************************************************/
static uint8_t cal_crc(uint8_t n, uint8_t *v)
{
	uint8_t	i, cks = 0;

	for(i = 0; i < n; i++)
	{
		cks = crc_tab[cks ^ v[i]];
	}
	return(cks);
}

/******************************************************************************
Function:		StartDTCTX
Description:	Start DTC in combination with TX
Input:			none
Output:			none
******************************************************************************/
void StartDTCTX(void)
{

	// stop any activity in SCI232, reset flags
	STOP_RTX232

	// set RRS bit in DTCCR to 0 (already done)
	// select full address mode (already done)
	// set transfer data structure: MRA (already done)
	// set transfer data structure: MRB (already done)
	// set transfer data structure: DAR (already done)
	// set transfer data structure: CRB (already done)
	// set transfer data start address in the DTC vector table (already done)

	// set transfer data structure: SAR (needs to be updated)
	dtc_data_sciTX.SAR = ((uint32_t) rtx232_buf);			// DTC source address register

	// set transfer data structure: CRA
    //dtc_data_sciTX.DW3.UL = (uint32_t)(rtx232_buf[IND_LEN]) << 16U;	// DTC transfer count register A
	dtc_data_sciTX.DW3.REG.CRA = rtx232_buf[IND_LEN];		// DTC transfer count register A

	// set the ICU_IERm.IENj bit to 1
	IR_TXI_RTX232 = 0;
	IEN_TXI_RTX232 = 1;

	// start dct
	START_DTC_TX

	// enable TX and TXI (this produces immediately the first transfer request)
	RTX232_SSR = 0x00;
	RX232_TXENA

}

/******************************************************************************
Function:		StartDTCRX
Description:	Start DTC in combination with RX
Input:			none
Output:			none
******************************************************************************/
void StartDTCRX(void)
{

	// set RRS bit in DTCCR to 0 (already done)
	// select full address mode (already done)
	// set transfer data structure: MRA (already done)
	// set transfer data structure: MRB (already done)
	// set transfer data structure: SAR (already done)
	// set transfer data structure: CRB (already done)
	// set transfer data start address in the DTC vector table (already done)

	// set transfer data structure: DAR (needs to be updated)
	dar_mem = ((uint32_t)(& (rtx232_buf[2])));
	dtc_data_sciRX.DAR = dar_mem;									// DTC destination address register

	// set transfer data structure: CRA
    //dtc_data_sciRX.DW3.UL = (uint32_t)(rtx232_buf[IND_LEN] - 2) << 16U;	// DTC transfer count register A
	dtc_data_sciRX.DW3.REG.CRA = (rtx232_buf[IND_LEN] - 2);			// DTC transfer count register A

	// start DCT
	START_DTC_RX

}

/******************************************************************************
Function: 		UI_RTX232_Init
Description:	serial communication init
Input:			nothing
Output:			nothing
Modifies:		internal variables involved in serial communication
******************************************************************************/
void UI_RTX232_Init(void)
{

//	for(rtx232_cnt = 0; rtx232_cnt < RTX232_LEN; rtx232_cnt++)
//	{
//		rtx232_buf[rtx232_cnt] = 0;
//	}
//	rtx232_cnt = 0;
//	rtx232_tim = 0;
//	rtx232_num = 0;

	RTX232_INIT
	START_RX232					// start rx
}

/******************************************************************************
Function:		UI_RTX232_ERI
Description:	Receive error interrupt service routine
Input:			none
Output:			none
******************************************************************************/
void UI_RTX232_ERI(void)
{
/*
	if(RTX232_ORER_ON)
	{
		orer_cnt++;
	}
	if(RTX232_FER_ON)
	{
		fer_cnt++;
	}
	if(RTX232_PER_ON)
	{
		per_cnt++;
	}
*/
	STOP_RTX232
	STOP_DTC
	rtx232_cnt = 0;
}

/******************************************************************************
Function:		UI_RTX232_RXI
Description:	Receive buffer full interrupt service routine
Input:			none
Output:			none
******************************************************************************/
void UI_RTX232_RXI(void)
{
	IR_RXI_RTX232 = 0;					// reset interrupt request
	if(0 == rtx232_cnt)					// first byte received
	{
		rtx232_num = RX232_REG;
		rtx232_buf[rtx232_cnt++] = rtx232_num;
		rtx232_tim = RTX232_BTIM;		// maximum 1 byte receive delay (sampling periods)
	}
	else if(1 == rtx232_cnt)			// second byte received
	{
		rtx232_buf[rtx232_cnt++] = RX232_REG;
		if(SEL_MAS != rtx232_buf[IND_SEL])
		{
			STOP_RTX232
			rtx232_cnt = 0;				// this forces restart
		}
		else
		{
			StartDTCRX();
			rtx232_tim = RTX232_BTIM;	// maximum 1 byte receive delay (sampling periods)
		}
	}
	else	// DTC finished receive
	{
		STOP_RTX232
		STOP_DTC
		rtx232_cnt = rtx232_num;
	}
}

/******************************************************************************
Function:		UI_RTX232_TXI
Description:	Transmission buffer empty interrupt service routine
Input:			none
Output:			none
******************************************************************************/
void UI_RTX232_TXI(void)
{
	RTX232_SCR_TEI = 1;			// B2 Transmit End Interrupt Enable
	IEN_TEI_RTX232 = 1;
	RTX232_SCR_TXI = 0;			// B7 Transmit Interrupt Enable
	IEN_TXI_RTX232 = 0;
}

/******************************************************************************
Function:		UI_RTX232_TEI
Description:	Transmission end interrupt service routine
Input:			none
Output:			none
******************************************************************************/
void UI_RTX232_TEI(void)
{
	STOP_RTX232
	STOP_DTC
	START_RX232
}

/******************************************************************************
Function:		UI_RTX232_RXDelMan
Description:	RX delay management
Input:			none
Output:			none
Note:			to be called in the control interrupt
******************************************************************************/
void UI_RTX232_RXDelMan(void)
{
	unsigned long aul;

	if(RX232_ON && (0 != rtx232_num))
	{
		if(0 < rtx232_tim)
		{
			rtx232_tim--;
		}
		else
		{
			aul = dtc_data_sciRX.DAR;
			if(aul != dar_mem)
			{
				dar_mem = aul;
				rtx232_tim = RTX232_BTIM;	// maximum 1 byte receive delay (sampling periods)
			}
			else
			{
				STOP_RTX232
				STOP_DTC
				START_RX232
				//rxdelcnt++;
			}
		}
	}

}

/******************************************************************************
Function:		UI_RTX232_Management
Description:	serial communication management
Input:			nothing
Output:			nothing
Modifies:		rx/tx buffer, internal flags
Note:			to be called in the main loop;
				the slave is always waiting for commands (rx); when a command
				is received, it is decoded and, if this makes sense, executed;
				if the command is correctly received or if there are errors
				in the data or in the addresses, the slave sends an answer.
******************************************************************************/
void UI_RTX232_Management(void)
{
	uint16_t add, nda, i;

	if(TX232_ON || RX232_ON)
	{
		return;
	}

	// errors in rx
	if((LEN_MIN > rtx232_num) || (rtx232_num > rtx232_cnt))
	{
		//rxerrcnt++;
		START_RX232
		return;
	}

	// no answer cases
	if((SEL_MAS != rtx232_buf[IND_SEL]) || (N_CENT != rtx232_buf[IND_ADS]))
	{
		//rxerrcnt++;
		START_RX232
		return;
	}
	if(rtx232_buf[rtx232_num - 1] != cal_crc((rtx232_num - 1), rtx232_buf))
	{
		//rxerrcnt++;
		START_RX232
		return;
	}

	// operation codes select and answer coding
	rtx232_buf[IND_SEL] = SEL_SLA;
    rtx232_num = LEN_MIN;					// frame lenght
	switch(rtx232_buf[IND_COD])
	{
		case CCO:	// check
		case NCO:	// check
		case XCO:	// check (programs with auto tuning)
		case YCO:	// check (programs with auto tuning and new buffers)
			rtx232_buf[IND_COD] = YCO;		// answer of programs with auto tuning and new buffers
			break;
		case RWO:	// answer coding
			add = rtx232_buf[IND_ADD];
			nda = rtx232_buf[IND_ND];
			if(NUM_PAR > add)		// reading from parameters table
			{
				if((NUM_PAR < nda) || (add > (NUM_PAR - nda)))
				{
					rtx232_buf[IND_SEL] = SEL_NOK;
					break;
				}
				for(i = 0; i < nda; i++)
				{
					w2b(par_vec[add + i], &(rtx232_buf[IND_ID + (i << 1)]));
				}
				rtx232_num = rtx232_num + (nda << 1) + LEN_DEL;	// frame lenght
			}
			else					// reading from ram table
			{
				add = add - NUM_PAR;
				if((N_RAM_READ < nda) || (add > (N_RAM_READ - nda)))
				{
					rtx232_buf[IND_SEL] = SEL_NOK;
					break;
				}
				for(i = 0; i < nda; i++)
				{
					w2b(UIF_R.ram_tab[add + i], &(rtx232_buf[IND_ID + (i << 1)]));
				}
				rtx232_num = rtx232_num + (nda << 1) + LEN_DEL;	// frame lenght
			}
			break;
		case WWO:	// answer coding
			add = rtx232_buf[IND_ADD];
			nda = rtx232_buf[IND_ND];
			if(NUM_PAR > add)		// writing in parameters table
			{
				if((NUM_PAR < nda) || (add > (NUM_PAR - nda)))
				{
					rtx232_buf[IND_SEL] = SEL_NOK;
					break;
				}
				for(i = 0; i < nda; i++)
				{
					dta_par = b2w(&(rtx232_buf[IND_ID + (i << 1)]));
					add_par = add + i;
					if(par_vec[add_par] != dta_par)
					{
						GE_SetPar(dta_par, add_par);
					}
				}
				dta_par = 0;
				add_par = 0;
			}
			else						// writing in ram table
			{
				add = add - NUM_PAR;
				if((N_RAM_WRITE < nda) || (add > (N_RAM_WRITE - nda)))
				{
					rtx232_buf[IND_SEL] = SEL_NOK;
					break;
				}
				for(i = 0; i < nda; i++)
				{
					UIF_W.ram_tab[add + i] = b2w(&(rtx232_buf[IND_ID + (i << 1)]));
				}
			}
			break;
		case RW3:	// reading parameters minimum values
			add = rtx232_buf[IND_ADD];
			nda = rtx232_buf[IND_ND];
			if((NUM_PAR <= add) || (nda > (NUM_PAR - add)))
			{
				rtx232_buf[IND_SEL] = SEL_NOK;
				break;
			}
			for(i = 0; i < nda; i++)
			{
				w2b(par_min[add + i], &(rtx232_buf[IND_ID + (i << 1)]));
			}
			rtx232_num = rtx232_num + (nda << 1) + LEN_DEL;	// frame lenght
			break;
		case RW4:	// reading parameters maximum values
			add = rtx232_buf[IND_ADD];
			nda = rtx232_buf[IND_ND];
			if((NUM_PAR <= add) || (nda > (NUM_PAR - add)))
			{
				rtx232_buf[IND_SEL] = SEL_NOK;
				break;
			}
			for(i = 0; i < nda; i++)
			{
				w2b(par_max[add + i], &(rtx232_buf[IND_ID + (i << 1)]));
			}
			rtx232_num = rtx232_num + (nda << 1) + LEN_DEL;	// frame lenght
			break;
		case RW6:	// reading parameters table default values
			add = rtx232_buf[IND_ADD];
			nda = rtx232_buf[IND_ND];
			if((NUM_PAR <= add) || (nda > (NUM_PAR - add)))
			{
				rtx232_buf[IND_SEL] = SEL_NOK;
				break;
			}
			for(i = 0; i < nda; i++)
			{
				w2b(par_def[add + i], &(rtx232_buf[IND_ID + (i << 1)]));
			}
		    rtx232_num = rtx232_num + (nda << 1) + LEN_DEL;	// frame lenght
			break;
		case RW5:	// reading measurement samples
			add = rtx232_buf[IND_ADD];
			nda = rtx232_buf[IND_ND];
			if((MAX_SAM_CNT <= add) || (nda > (MAX_SAM_CNT - add)) || (nda > MAX_NWDATA))
			{
				rtx232_buf[IND_SEL] = SEL_NOK;
				break;
			}
			if(0 == add)
			{
				SET_RBUSY
				sam_cnt = 0;
			}
			for(i = 0; i < nda; i++)
			{
				w2b(rbuf[add + i], &(rtx232_buf[IND_ID + (i << 1)]));
			}
			sam_cnt = sam_cnt + nda;
			if(MAX_SAM_CNT <= sam_cnt)
			{
				RES_RBUSY
			}
			rtx232_num = rtx232_num + (nda << 1) + LEN_DEL;	// frame lenght
			break;
		default:
			rtx232_buf[IND_SEL] = SEL_NOK;
			break;
	}

	// crc calc. and answer transmission
	rtx232_buf[IND_LEN] = rtx232_num;
	rtx232_buf[rtx232_num - 1] = cal_crc(rtx232_num - 1, rtx232_buf);
	rtx232_buf[rtx232_num] = 0;			// ("C" string)
	START_TX232
	return;

}

/******************************************************************************
Function:		UI_LedMan
Description:	led blinking management
Input:			nothing
Output:			nothing
Modifies:		leds status
Note:			to be called in the main loop
******************************************************************************/
void UI_LedMan(void)
{
	static uint16_t	led_cnt;
	uint16_t		led_time;

	if(0 == UIF_R.var.all) 
	{
		led_time = LED_TIME_FUN;
	}
	else
	{
		led_time = LED_TIME_ALL;
	}
	if(led_cnt < led_time)
	{
		led_cnt++;
	}
	else
	{
		led_cnt = 0;
		if(DL2_OFF)
		{
			SET_DL2
		}
		else
		{
			CLR_DL2
		}
	}
}

// EOF userif.c

