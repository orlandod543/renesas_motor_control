
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		encoder.c
*	Project:	MCRP07 RX62T
*	Revision:	7.0
*	Date:		16-06-2014 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	encoder management routines
*******************************************************************************/

#include <mathf.h>
#include "globdef.h"
#include ".\YRotateItRX23T_Library\mcrplibf.h"
#include "encoder.h"

// encoder specifications (to be modified by the user)
#define	REV_DIR					// encoder direction
#define	ENC_PUL_REV		2000			// encoder pulses/revolution

// speed filters specifications (to be modified by the user)
#define	ENC_BUF_DIM		32			// fir buffer dim. (time const. ENC_BUF_DIM/SAM_FRE_HZ)
#define ENC_IIR_KT		0.002f		// iir filter time const. [sec]
#define SAM_FRE_HZ 		8000 		// frecuencia del sistema

// encoder macros
#define	ENC_EDG_REV		(4 * ENC_PUL_REV)									// encoder edges/revolution
#define KA_ENC			((float)(TWOPI / ENC_EDG_REV))						// angle calculation
#define KS_ENC			((float)((SAM_FRE_HZ * KA_ENC) / ENC_BUF_DIM))		// speed calculation
#define ENC_IIR_UKT		((float)(1.0 / (1.0 + (ENC_IIR_KT * SAM_FRE_HZ))))

// encoder management variables
unsigned short
	enc_sam,				// encoder sample
	enc_mem,				// encoder sample memory
	enc_ind;				// filter table index
signed short
	enc_del,				// encoder speed (delta edges in one sampling period)
	enc_ang,				// mechanical angular position
	enc_buf[ENC_BUF_DIM];	// fir filter buffer
signed long
//	enc_deb,
	enc_fir;				// fir filter accumulator
float
	enc_iir,				// filtered (iir) encoder speed
	mec_ang,				// mechanical angle position		[rad]
	om_mec;					// mechanical speed					[rad/s]

// encoder management functions
void Encoder_Init(void)
/***************************************************************************
*	Description:	timer 2 settings (phase counting mode 1) for encoder
*					input reading, timer start
***************************************************************************/
{

	// wake up module before accessing registers
//	WAKEUP_MTU					// already done
//	MC_Delay(10);

	// MTU settings (Timer 1: TCLKA, TCLKB)
	MTU.TSTRA.BIT.CST1 = 0;		// timer 1 stop counting
	MTU1.TCNT = 0;				// reset counter value
	MTU1.TMDR3.BIT.LWA = 0;
//	MTU2.TSR.BIT.TCFU = 0;		// reset underflow flag (only 0 can be written)
//	MTU2.TSR.BIT.TCFV = 0;		// reset overflow flag (only 0 can be written)
	MTU1.TCR.BIT.CCLR = 0;		// select no clearing source
	MTU1.TMDR1.BIT.MD = 4;		// set phase counting mode 1


    MPC.PWPR.BYTE = 0x00U;			// enable writing PWPR
 	MPC.PWPR.BYTE = 0x40U;			// enable writing MPC registers
    /* Set MTCLKA pin */
    MPC.P33PFS.BYTE = 0x02U;
    PORT3.PMR.BYTE |= 0x08U;
    /* Set MTCLKB pin */
    MPC.P32PFS.BYTE = 0x02U;
    PORT3.PMR.BYTE |= 0x04U;

    MPC.PWPR.BYTE = 0x80U;			// disable writing MPC registers

	// data structures for speed calculation initialization
	enc_mem = 0;
	for(enc_ind = 0; enc_ind < ENC_BUF_DIM; enc_ind++)
		enc_buf[enc_ind] = 0;	// buffer
	enc_ind = 0;				// buffer index
	enc_fir = 0;				// fir filter accumulator
	enc_iir = 0.0f;				// iir filter memory

	// input pins
	//PORT1.DR.BIT.B1 = 0;		// PIN99	P11 DIG IN 	MTCLKC_UP99		ENC_A
	//PORT1.DR.BIT.B0 = 0;		// PIN100	P10 DIG IN 	MTCLKD_UP100	ENC_B

	// start counting
	MTU.TSTRA.BIT.CST1 = 1;		// timer 1 start counting
}

void Encoder_Read(void)
/***************************************************************************
*	Description:	speed calculation from encoder sample
*	Note:			to be called every sampling period
***************************************************************************/
{

	/* Since not always the encoder pulses/revolution number is a power of
	two, the following steps are needed */

	/* calculate the delta pulses in one sampling time */
	enc_sam = MTU1.TCNT;
#ifndef REV_DIR
	enc_del = enc_sam - enc_mem;
#else	// REV_DIR
	enc_del = enc_mem - enc_sam;
#endif	// REV_DIR
	enc_mem = enc_sam;

	/* calculate the encoder angle integrating the speed (delta) */
//	enc_deb += enc_del;
	enc_ang += enc_del;
	if(enc_ang >= ENC_EDG_REV)
	{
		enc_ang -= ENC_EDG_REV;
	}
	else if(enc_ang < 0)
	{
		enc_ang += ENC_EDG_REV;
	}

	// angle determination
	mec_ang = McrpLibf_AngleNrm(enc_ang * KA_ENC);		// mechanical angle	[rad]

	// speed fir filter (output is amplified by ENC_BUF_DIM)
	enc_fir += enc_del;
	enc_fir -= enc_buf[enc_ind];
	enc_buf[enc_ind++] = enc_del;
	if(enc_ind >= ENC_BUF_DIM)
	{
		enc_ind = 0;
	}

	// speed iir filter
	enc_iir += (enc_fir - enc_iir) * ENC_IIR_UKT;

	// conversion in [rad/sec]
	om_mec = KS_ENC * enc_iir;					// mechanical speed [rad/s]

 }
// end of encoder management functions

float Encoder_GetMechAngle(void)
/***************************************************************************
*	Description:	returns the actual mechanical angle
***************************************************************************/
{
	return(mec_ang);
}

float Encoder_GetMechSpeed(void)
/***************************************************************************
*	Description:	returns the actual mechanical speed
***************************************************************************/
{
	return(om_mec);
}



