
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		reset_program.c
*	Project:	YRotateIt-RX23T (three shunts)
*	Revision:	1.0
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Renesas Project Generator
*	Contents:	Reset program
*******************************************************************************/

#define _RESET_PROGRAM_C			// insert define BEFORE including header files

#include <machine.h>
#include <_h_c_lib.h>
#include "typedefine.h"				// Define Types
#include "stacksct.h"				// Stack Sizes (Interrupt and User)

void PowerON_Reset(void);
void main(void);
extern void HardwareSetup(void);

#define PSW_init  ( 0x00010000 )	// PSW bit pattern
#define FPSW_init ( 0x00000000 )	// FPSW bit base pattern

#pragma section ResetPRG			// output PowerON_Reset to PResetPRG section

#pragma entry PowerON_Reset
void PowerON_Reset(void)
{
#ifdef __RXV2
	set_extb(__sectop("EXCEPTVECT"));
#endif
	set_intb(__sectop("C$VECT"));

#ifdef __ROZ						// Initialize FPSW
#define _ROUND ( 0x00000001 )		// Let FPSW RMbits=01 (round to zero)
#else
#define _ROUND ( 0x00000000 )		// Let FPSW RMbits=00 (round to nearest)
#endif
#ifdef __DOFF
#define _DENOM ( 0x00000100 )		// Let FPSW DNbit=1 (denormal as zero)
#else
#define _DENOM ( 0x00000000 )		// Let FPSW DNbit=0 (denormal as is)
#endif

	set_fpsw(FPSW_init | _ROUND | _DENOM);

	_INITSCT();						// Initialize Sections

	HardwareSetup();				// Use Hardware Setup
    nop();

	set_psw(PSW_init);				// Set Ubit & Ibit for PSW
	chg_pmusr();					// Change PSW PMbit (SuperVisor->User)

	main();

	brk();
}

// EOF reset_program.c

