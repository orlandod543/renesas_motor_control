
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		main.c
*	Project:	YRotateIt-RX23T (three shunts)
*	Revision:	1.0
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	Main program
*******************************************************************************/

#define _MAIN_C				// insert define BEFORE including header files

#include "globdef.h"
#include "ges_eqp.h"
#include "motorcontrol.h"
#include "userif.h"

void main(void)
{

//#define DELAY_TUNING
#ifdef DELAY_TUNING
	while(1)	// delay tuning
	{
		SET_DL1
		DELAY1MS(1)
		//DELAY100US(1)
		//DELAY10US(10)
		//DELAY1US(100)
		CLR_DL1
		DELAY1MS(1)
		//DELAY100US(1)
		//DELAY10US(10)
		//DELAY1US(100)
	}
#endif	// DELAY_TUNING

	// delay for bus voltage stabilization
	DELAY1MS(100);		// 100ms

	// bypass current inrush limitation resistor
	SET_LIMOFF

	// flags reset
	UIF_R.var.flg = 0;
	//RES_ALRM_ON
	//RES_COM_ON
	//RES_STA_BSY
	//RES_END_NOK
	CLR_DL2
	CLR_DL3

	// parameters table upload
	GE_IniEqp();
	GE_DefToRam();
//	GE_RamToEqp();
	if(0 != GE_EqpToRam())
	{
		UIF_R.var.all = EQP_ALL;
		SET_ALRM_ON
	}

	// current offsets reading
	MC_SetOff();

	// program init
	MC_IniPar();
	MC_IniPWM();
	Encoder_Init();
	// pwm start, interrupt enable
	MC_StartPWM();

	// serial communication init
	UI_RTX232_Init();

	// main loop
	while(1)
	{

		// time synchronization
		while(MC_WaitSync())
		{
			// this time can be used for asynchronous functions
		}

		//SET_DL3				// timing debug

		// working mode management function (speed ramp etc.)
		MC_WorkMan();

		// interface management
		MC_ConFil();
		UI_RTX232_Management();
		MC_UpdPar();

		// general management functions
		if(0 != GE_EqpMan())
		{
			if(0 == UIF_R.var.all)
			{
				UIF_R.var.all = EQP_ALL;
				SET_ALRM_ON
			}
		}
		UI_LedMan();

		//CLR_DL3				// timing debug

	}
}

// EOF main.c

