
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		ges_eqp.h
*	Project:	YRotateIt-RX23T (three shunts)
*	Revision:	1.0
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	Eeprom management functions
*******************************************************************************/

#ifndef	_GES_EQP_H
#define _GES_EQP_H

#include "globdef.h"

#define NUM_PAR 		( 64 )	// parameters number
#define RIP_DEF 		( 33 )	// default parameters setting password
#define DEB_OPN			( 55 )	// open loop operation debug mode password
#define DCCUR_S			( 66 )	// dc current vector setting debug mode password

#define DEB_OPNLP		(DEB_OPN == SEL_OP)
#define DCCUR_SET		(DCCUR_S == SEL_OP)

#define IND_EQP_SEL_OP	(  0 )
#define IND_EQP_R_STA	(  8 )
#define IND_EQP_L_SYN	(  9 )
#define IND_EQP_PM_FLX	( 10 )
#define IND_EQP_KP_CUR	( 11 )
#define IND_EQP_KI_CUR	( 12 )

#define SEL_OP		par_vec[ 0]	// default parameters setting
#define RPM_MIN		par_vec[ 1]	// minimum speed			rpm
#define RPM_MAX		par_vec[ 2]	// maximum speed			rpm
#define R_ACC		par_vec[ 3]	// acceleration				rpm/s
#define R_DEC		par_vec[ 4]	// deceleration				rpm/s
#define C_POLI		par_vec[ 5]	// polar couples
#define I_START		par_vec[ 6]	// startup current			Ampere/AMP_RES
#define I_MAX		par_vec[ 7]	// maximum current			Ampere/AMP_RES
#define R_STA		par_vec[ 8]	// stator resistance		Ohm/OHM_RES
#define L_SYN		par_vec[ 9]	// synchronous inductance	Henry/HEN_RES
#define PM_FLX		par_vec[10]	// permanent magnets flux	Weber/WEB_RES
#define KP_CUR		par_vec[11]	// current loop KP
#define KI_CUR		par_vec[12]	// current loop KI
#define KP_VEL		par_vec[13]	// speed loop KP
#define	KI_VEL		par_vec[14]	// speed loop KI
#define	FB_GAIN		par_vec[15]	// flux amp. feedback gain
#define PHA_OFF		par_vec[16]	// phase offset				deg
#define	SUP_TIM		par_vec[17]	// startup acc. time		sec/SEC_RES
#define FLX_FTAU	par_vec[18]	// flux est. filter tau		sec/SEC_RES
#define SAM_FRE		par_vec[19]	// sampling frequency		Hz
#define F_RATIO		par_vec[20]	// pwm_freq/sam_freq ratio
#define PAR_21		par_vec[21]	//
#define PAR_22		par_vec[22]	//
#define PAR_23		par_vec[23]	//
#define PAR_24		par_vec[24]	//
#define PAR_25		par_vec[25]	//
#define PAR_26		par_vec[26]	//
#define PAR_27		par_vec[27]	//
#define PAR_28		par_vec[28]	//
#define PAR_29		par_vec[29]	//
#define PAR_30		par_vec[30]	//
#define PAR_31		par_vec[31]	//
#define PAR_32		par_vec[32]	//
#define PAR_33		par_vec[33]	//
#define PAR_34		par_vec[34]	//
#define PAR_35		par_vec[35]	//
#define PAR_36		par_vec[36]	//
#define PAR_37		par_vec[37]	//
#define PAR_38		par_vec[38]	//
#define PAR_39		par_vec[39]	//
#define PAR_40		par_vec[40]	//
#define PAR_41		par_vec[41]	//
#define PAR_42		par_vec[42]	//
#define PAR_43		par_vec[43]	//
#define PAR_44		par_vec[44]	//
#define PAR_45		par_vec[45]	//
#define PAR_46		par_vec[46]	//
#define PAR_47		par_vec[47]	//
#define PAR_48		par_vec[48]	//
#define PAR_49		par_vec[49]	//
#define PAR_50		par_vec[50]	//
#define PAR_51		par_vec[51]	//
#define PAR_52		par_vec[52]	//
#define PAR_53		par_vec[53]	//
#define PAR_54		par_vec[54]	//
#define PAR_55		par_vec[55]	//
#define PAR_56		par_vec[56]	//
#define PAR_57		par_vec[57]	//
#define PAR_58		par_vec[58]	//
#define PAR_59		par_vec[59]	//
#define PAR_60		par_vec[60]	//
#define PAR_61		par_vec[61]	//
#define PAR_62		par_vec[62]	//
#define PAR_63		par_vec[63]	// last parameter

#define PAR_ADD( X )	((uint16_t)( X ))

#ifdef _GES_EQP_C

const uint16_t
	par_min[NUM_PAR] = {// MINIMUM VALUES
		0,				//  0 SEL_OP
		10,				//  1 RPM_MIN
		60,				//  2 RPM_MAX
		1,				//  3 R_ACC
		1,				//  4 R_DEC
		1,				//  5 C_POLI
		0,				//  6 I_START
		0,				//  7 I_MAX
		0,				//  8 R_STA
		0,				//  9 L_SYN
		0,				// 10 PM_FLX
		0,				// 11 KP_CUR
		0,				// 12 KI_CUR
		0,				// 13 KP_VEL
		0,				// 14 KI_VEL
		0,				// 15 FB_GAIN
		0,				// 16 PHA_OFF
		100,			// 17 SUP_TIM
		8,				// 18 FLX_FTAU
		SAM_FRE_MIN,	// 19 SAM_FRE
		F_RATIO_MIN,	// 20 F_RATIO
		0,				// 21 PAR_21
		0,				// 22 PAR_22
		0,				// 23 PAR_23
		0,				// 24 PAR_24
		0,				// 25 PAR_25
		0,				// 26 PAR_26
		0,				// 27 PAR_27
		0,				// 28 PAR_28
		0,				// 29 PAR_29
		0,				// 30 PAR_30
		0,				// 31 PAR_31
		0,				// 32 PAR_32
		0,				// 33 PAR_33
		0,				// 34 PAR_34
		0,				// 35 PAR_35
		0,				// 36 PAR_36
		0,				// 37 PAR_37
		0,				// 38 PAR_38
		0,				// 39 PAR_39
		0,				// 40 PAR_40
		0,				// 41 PAR_41
		0,				// 42 PAR_42
		0,				// 43 PAR_43
		0,				// 44 PAR_44
		0,				// 45 PAR_45
		0,				// 46 PAR_46
		0,				// 47 PAR_47
		0,				// 48 PAR_48
		0,				// 49 PAR_49
		0,				// 50 PAR_50
		0,				// 51 PAR_51
		0,				// 52 PAR_52
		0,				// 53 PAR_53
		0,				// 54 PAR_54
		0,				// 55 PAR_55
		0,				// 56 PAR_56
		0,				// 57 PAR_57
		0,				// 58 PAR_58
		0,				// 59 PAR_59
		0,				// 60 PAR_60
		0,				// 61 PAR_61
		0,				// 62 PAR_62
		0},				// 63 PAR_63
	par_def[NUM_PAR] = {// DEFAULT VALUES
		0,				//  0 SEL_OP
		RPM_MIN_DEF,	//  1 RPM_MIN
		RPM_MAX_DEF,	//  2 RPM_MAX
		R_ACC_DEF,		//  3 R_ACC
		R_DEC_DEF,		//  4 R_DEC
		C_POLI_DEF,		//  5 C_POLI
		I_START_DEF,	//  6 I_START
		I_MAX_DEF,		//  7 I_MAX
		R_STA_DEF,		//  8 R_STA
		L_SYN_DEF,		//  9 L_SYN
		PM_FLX_DEF,		// 10 PM_FLX
		KP_CUR_DEF,		// 11 KP_CUR
		KI_CUR_DEF,		// 12 KI_CUR
		KP_VEL_DEF,		// 13 KP_VEL
		KI_VEL_DEF,		// 14 KI_VEL
		FB_GAIN_DEF,	// 15 FB_GAIN
		PHA_OFF_DEF,	// 16 PHA_OFF
		SUP_TIM_DEF,	// 17 SUP_TIM
		FLX_FTAU_DEF,	// 18 FLX_FTAU
		SAM_FRE_DEF,	// 19 SAM_FRE
		F_RATIO_DEF,	// 20 F_RATIO
		0,				// 21 PAR_21
		0,				// 22 PAR_22
		0,				// 23 PAR_23
		0,				// 24 PAR_24
		0,				// 25 PAR_25
		0,				// 26 PAR_26
		0,				// 27 PAR_27
		0,				// 28 PAR_28
		0,				// 29 PAR_29
		0,				// 30 PAR_30
		0,				// 31 PAR_31
		0,				// 32 PAR_32
		0,				// 33 PAR_33
		0,				// 34 PAR_34
		0,				// 35 PAR_35
		0,				// 36 PAR_36
		0,				// 37 PAR_37
		0,				// 38 PAR_38
		0,				// 39 PAR_39
		0,				// 40 PAR_40
		0,				// 41 PAR_41
		0,				// 42 PAR_42
		0,				// 43 PAR_43
		0,				// 44 PAR_44
		0,				// 45 PAR_45
		0,				// 46 PAR_46
		0,				// 47 PAR_47
		0,				// 48 PAR_48
		0,				// 49 PAR_49
		0,				// 50 PAR_50
		0,				// 51 PAR_51
		0,				// 52 PAR_52
		0,				// 53 PAR_53
		0,				// 54 PAR_54
		0,				// 55 PAR_55
		0,				// 56 PAR_56
		0,				// 57 PAR_57
		0,				// 58 PAR_58
		0,				// 59 PAR_59
		0,				// 60 PAR_60
		0,				// 61 PAR_61
		0,				// 62 PAR_62
		0},				// 63 PAR_63
	par_max[NUM_PAR] = {// MAXIMUM VALUES
		32767,			//  0 SEL_OP
		5000,			//  1 RPM_MIN
		30000,			//  2 RPM_MAX
		10000,			//  3 R_ACC
		10000,			//  4 R_DEC
		24,				//  5 C_POLI
		15000,			//  6 I_START
		15000,			//  7 I_MAX
		30000,			//  8 R_STA
		30000,			//  9 L_SYN
		30000,			// 10 PM_FLX
		30000,			// 11 KP_CUR
		30000,			// 12 KI_CUR
		30000,			// 13 KP_VEL
		30000,			// 14 KI_VEL
		30000,			// 15 FB_GAIN
		359,			// 16 PHA_OFF
		30000,			// 17 SUP_TIM
		1000,			// 18 FLX_FTAU
		SAM_FRE_MAX,	// 19 SAM_FRE
		F_RATIO_MAX,	// 20 F_RATIO
		32767,			// 21 PAR_21
		32767,			// 22 PAR_22
		32767,			// 23 PAR_23
		32767,			// 24 PAR_24
		32767,			// 25 PAR_25
		32767,			// 26 PAR_26
		32767,			// 27 PAR_27
		32767,			// 28 PAR_28
		32767,			// 29 PAR_29
		32767,			// 30 PAR_30
		32767,			// 31 PAR_31
		32767,			// 32 PAR_32
		32767,			// 33 PAR_33
		32767,			// 34 PAR_34
		32767,			// 35 PAR_35
		32767,			// 36 PAR_36
		32767,			// 37 PAR_37
		32767,			// 38 PAR_38
		32767,			// 39 PAR_39
		32767,			// 40 PAR_40
		32767,			// 41 PAR_41
		32767,			// 42 PAR_42
		32767,			// 43 PAR_43
		32767,			// 44 PAR_44
		32767,			// 45 PAR_45
		32767,			// 46 PAR_46
		32767,			// 47 PAR_47
		32767,			// 48 PAR_48
		32767,			// 49 PAR_49
		32767,			// 50 PAR_50
		32767,			// 51 PAR_51
		32767,			// 52 PAR_52
		32767,			// 53 PAR_53
		32767,			// 54 PAR_54
		32767,			// 55 PAR_55
		32767,			// 56 PAR_56
		32767,			// 57 PAR_57
		32767,			// 58 PAR_58
		32767,			// 59 PAR_59
		32767,			// 60 PAR_60
		32767,			// 61 PAR_61
		32767,			// 62 PAR_62
		32767};			// 63 PAR_63
uint16_t
	par_vec[NUM_PAR];	// parameters vector

#else	// _GES_EQP_C

extern const uint16_t
	par_min[NUM_PAR],
	par_max[NUM_PAR],
	par_def[NUM_PAR];
extern uint16_t
	par_vec[NUM_PAR];

#endif	// _GES_EQP_C

/*******************************************************************************
Function:		GE_IniEqp
Description:	hardware init if needed
Input:			nothing
Output:			nothing
*******************************************************************************/
void GE_IniEqp(void);

/******************************************************************************
Function:		GE_DefToRam
Description:	copies default parameters into ram
Input:			nothing
Output:			=0 -> OK, !=0 -> NOK
Modifies:		internal parameters vector
******************************************************************************/
uint16_t GE_DefToRam(void);

/******************************************************************************
Function:		GE_RamToEqp
Description:	copies all the parameters from ram to eeprom
Input:			nothing
Output:			=0 -> OK, !=0 -> NOK
Modifies:		eeprom content
Note:			to be used out of main loop, due to the high delay
******************************************************************************/
uint16_t GE_RamToEqp(void);

/******************************************************************************
Function:		GE_EqpToRam
Description:	if par0!=RIP_DEF copies all parameters from eeprom to
				ram, else copies all default values in ram parameter
				table, then copies all the parameters from ram to eeprom
Input:			nothing
Output:			=0 -> OK, !=0 -> NOK
Modifies:		internal parameters vector or eeprom content
Note:			to be used out of main loop, due to the high delay
******************************************************************************/
uint16_t GE_EqpToRam(void);

/******************************************************************************
Function:		GE_SetPar
Description:	insertion of a parameter in eqp fifo, to be updated
Input:			parameter value, parameter address in the vector
Output:			nothing
Modifies:		internal fifo
******************************************************************************/
void GE_SetPar(uint16_t val, uint16_t add);

/******************************************************************************
Function:		GE_EqpMan
Description:	eeprom parameters modification management
Input:			parameter value, parameter address in the vector
Output:			=0 -> OK, !=0 -> NOK
Modifies:		internal fifo, eeprom content
Note:			to be called in main loop
******************************************************************************/
uint16_t GE_EqpMan(void);

#endif	// _GES_EQP_H

// EOF ges_eqp.h

