
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		ges_eqp.c
*	Project:	YRotateIt-RX23T (three shunts)
*	Revision:	1.0
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	Eeprom management functions
*******************************************************************************/

#define _GES_EQP_C			// insert define BEFORE including header files

#include "globdef.h"		// include globdef.h BEFORE including ges_eqp.h
#include "ges_eqp.h"

#ifdef EEPROM_USED

#define MAX_WAIT		( 50 )			// 50 ms
#define	NL_W_EQP		(  5 )			// 5*10ms=50ms max

#define	NUM_FIFO_EQP	(NUM_PAR + 10)	// eqp fifo dimension
uint16_t
	eqp_fifo[NUM_FIFO_EQP],	// fifo of eeprom values to be updated
	eqp_add[NUM_FIFO_EQP];	// fifo of addresses of eeprom values to be updated
uint16_t
	eqp_all = 0,
	mas_ind = 0,			// fifo master index
	sla_ind = 0;			// fifo slave index
int16_t
	modif = -1;
uint16_t
	eqp_wrt = 0,
	eqp_bsy = 0,
	eqp_aus = 0;

/* management functions for iic eeprom ****************************************/

/* Hardware notes **************************************************************
	Since used micro's ports are input or push_pull outputs, to interface an
	IIC eeprom the following strategy is used:
	_	a port of the micro (EEPSCL) is used as clock output and is directly
		connected with the SCL input of the eeprom;
	_	the SDA I/O of the eeprom is connected to a pull-up resistor and to a
		port (EEPSDA) of the micro;
	_	to read a data bit or to write a HIGH data bit the EEPSDA port is
		configured as input.
	_	to write a LOW data bit the EEPSDA port is configured as output and
		it is forced to zero.
*******************************************************************************/

	// I/O settings
	// port direction register
	//PORTA.PDR.BIT.B5 = 1;			// pin63 PA5 EEP-SCL O
	//PORTA.PDR.BIT.BC = 0;			// pin64 PA4 EEP-SDA I/O
	// port output data register
	//PORTA.PODR.BIT.B5 = 1;		// pin63 PA5 EEP-SCL O
	//PORTA.PODR.BIT.B4 = 0;		// pin64 PA4 EEP-SDA I/O
	// port input data register
	//PORTA.PIDR.BIT.B5				// pin63 PA5 EEP-SCL O
	//PORTA.PIDR.BIT.B4				// pin64 PA4 EEP-SDA I/O
	// port mode register
	//PORTA.PMR.BIT.B5 = 0;			// pin63 PA5 EEP-SCL O
	//PORTA.PMR.BIT.B4 = 0;			// pin64 PA4 EEP-SDA I/O
	// open drain control register 0
	//PORTA.ODR1.BIT.B0 = 0;		// pin64 PA4 EEP-SDA I/O

#define	SCL_OUT			PORTA.PODR.BIT.B5
#define	SDA_OUT			PORTA.PODR.BIT.B4
#define	SDA_IN			PORTA.PIDR.BIT.B4
#define	SCL_IOC			PORTA.PDR.BIT.B5
#define	SDA_IOC			PORTA.PDR.BIT.B4

#define PUT_SCL_OUT		{SCL_IOC = 1;}
#define PUT_SCL_IN		{SCL_IOC = 0;}
#define PUT_SDA_OUT		{SDA_IOC = 1;}
#define PUT_SDA_IN		{SDA_IOC = 0;}
#define	SET_SCL			{SCL_OUT = 1;}
#define	CLR_SCL			{SCL_OUT = 0;}
#define SET_SDA			PUT_SDA_IN
#define CLR_SDA			{PUT_SDA_OUT SDA_OUT = 0;}
#define	SDA_IN_H		(0 != SDA_IN)
#define	SDA_IN_L		(0 == SDA_IN)

#define	WAIT_SCL		DELAY1US( 2 )		// clock half period: minimum 1.25 microsec

#define	CODE_ADD_W		( 0xA0U )			// 1010=CODE 000=ADD 0=WRITE
#define	CODE_ADD_R		( 0xA1U )			// 1010=CODE 000=ADD 1=READ

// If the eeprom has a 2 bytes addressing please remove the comment in the following
// define, else if the eeprom has a 1 byte addressing please comment it
//#define	ADD2BYTES

// Routines are called with SCL=0 and SDA=1; they leave the same situation on exit

/*******************************************************************************
Function:		EQP_HwIni
Description:	hardware initialization
Input:			nothing
Output:			nothing
*******************************************************************************/
static void EQP_HwIni(void)
{
	// I/O settings
	PUT_SCL_OUT
	CLR_SCL
	SET_SDA
}

/*******************************************************************************
Function:		EQP_SendStart
Description:	sends start sequence
Input:			nothing
Output:			nothing
*******************************************************************************/
static void EQP_SendStart(void)
{
	CLR_SCL				// for safety
	SET_SDA				// for safety
	WAIT_SCL
	SET_SCL
	WAIT_SCL
	CLR_SDA
	WAIT_SCL
	CLR_SCL
	SET_SDA
}

/*******************************************************************************
Function:		EQP_SendStop
Description:	sends stop sequence
Input:			nothing
Output:			nothing
*******************************************************************************/
static void EQP_SendStop(void)
{
	CLR_SCL				// for safety
	CLR_SDA
	WAIT_SCL
	SET_SCL
	WAIT_SCL
	SET_SDA
	WAIT_SCL
	CLR_SCL
}

/*******************************************************************************
Function:		EQP_SendByte
Description:	sends a byte
Input:			byte to send
Output:			=0 -> OK, !=0 -> NOK
*******************************************************************************/
static uint16_t EQP_SendByte(uint8_t byt)
{
	uint8_t i;

	CLR_SCL				// for safety
	for (i = 0; i < 8; i++)
	{
		if (byt & 0x80)
		{
			SET_SDA
		}
		else
		{
			CLR_SDA
		}
		WAIT_SCL
		SET_SCL
		WAIT_SCL
		CLR_SCL
		byt = byt << 1;
	}
	SET_SDA
	WAIT_SCL
	SET_SCL
	WAIT_SCL
	if (SDA_IN_L)
	{
		CLR_SCL
		return (0);
	}
	else
	{
		CLR_SCL
		EQP_SendStop();
		return (1);
	}
}

/*******************************************************************************
Function:		EQP_ReadByte
Description:	receive a byte
Input:			ack status
Output:			read value
*******************************************************************************/
static uint8_t EQP_ReadByte(uint8_t ack)
{
	uint8_t i, msk, val;

	CLR_SCL				// for safety
	SET_SDA				// for safety
	msk = 0x80;
	val = 0;
	for (i = 0; i < 8; i++)
	{
		WAIT_SCL
		SET_SCL
		WAIT_SCL
		if (SDA_IN_H)
		{
			val |= msk;
		}
		CLR_SCL
		msk = msk >> 1;
	}
	if (ack)
	{
		CLR_SDA
	}
	else
	{
		SET_SDA
	}
	WAIT_SCL
	SET_SCL
	WAIT_SCL
	CLR_SCL
	SET_SDA
	return (val);
}

/*******************************************************************************
Function:		EQP_WriteWord
Description:	write a word data in eeprom
Input:			eeprom address to be written to, value to write
Output:			=0 -> OK, !=0 -> NOK
*******************************************************************************/
static uint16_t EQP_WriteWord(uint16_t add, uint16_t val)
{
	uint8_t msb, lsb;

	if ((NUM_PAR - 1) < add)
	{
		return (1);
	}
	add = add << 1;				// word access
	EQP_SendStart();
	if (0 != EQP_SendByte(CODE_ADD_W))
	{
		return (1);
	}
	msb = add >> 8;				// MSB of address
	lsb = add & 0x00FF;			// LSB of address
#ifdef	ADD2BYTES
	if(0 != EQP_SendByte(msb))
	{
		return (1);
	}
#endif
	if (0 != EQP_SendByte(lsb))
	{
		return (1);
	}
	msb = val >> 8;				// MSB of value
	lsb = val & 0x00FF;			// LSB of value
	if (0 != EQP_SendByte(msb))
	{
		return (1);
	}
	if (0 != EQP_SendByte(lsb))
	{
		return (1);
	}
	EQP_SendStop();
	return (0);
}

/*******************************************************************************
Function:		EQP_ReadWord
Description:	read a word data from eeprom
Input:			eeprom address to be read to, address of the variable which will
				contain the read value
Output:			=0 -> OK, !=0 -> NOK
*******************************************************************************/
static uint16_t EQP_ReadWord(uint16_t add, uint16_t *val)
{
	uint8_t		msb, lsb;
	uint16_t	result;

	if ((NUM_PAR - 1) < add)
	{
		return (1);
	}
	add = add << 1;				// word access
	EQP_SendStart();
	if (0 != EQP_SendByte(CODE_ADD_W))
	{
		return (1);
	}
	msb = add >> 8;				// MSB of address
	lsb = add & 0x00FF;			// LSB of address
#ifdef	ADD2BYTES
	if(0 != EQP_SendByte(msb))
	{
		return (1);
	}
#endif
	if (0 != EQP_SendByte(lsb))
	{
		return (1);
	}
	EQP_SendStart();
	if (0 != EQP_SendByte(CODE_ADD_R))
	{
		return (1);
	}
	msb = EQP_ReadByte(1);			// MSB of value
	lsb = EQP_ReadByte(0);			// LSB of value
	EQP_SendStop();
	result = msb;
	result = result << 8;
	result = result | lsb;
	(*val) = result;
	return (0);
}

/*******************************************************************************
Function:		EQP_EqpBusy
Description:	test busy
Input:			nothing
Output:			=0 -> OK, !=0 -> busy or NOK
*******************************************************************************/
static uint16_t EQP_EqpBusy(void)
{
	EQP_SendStart();
	if (0 != EQP_SendByte(CODE_ADD_W))
	{
		return (1);
	}
	EQP_SendStop();
	return (0);
}

/* end of management functions for iic_eeprom *********************************/

/*******************************************************************************
Function:		GE_IniEqp
Description:	hardware init if needed
Input:			nothing
Output:			nothing
*******************************************************************************/
void GE_IniEqp(void)
{
	EQP_HwIni();
}

/******************************************************************************
Function:		GE_DefToRam
Description:	copies default parameters into ram
Input:			nothing
Output:			=0 -> OK, !=0 -> NOK
Modifies:		internal parameters vector
******************************************************************************/
uint16_t GE_DefToRam(void)
{
	uint16_t i;

	for (i = 0; i < NUM_PAR; i++)
	{
/*	EQP_DEBUG
		if (par_min[i] > par_max[i])
		{
			par_vec[i] = par_def[i];
			eqp_all = 1;
		}
		else if (par_def[i] < par_min[i])
		{
			par_vec[i] = par_min[i];
			eqp_all = 1;
		}
		else if (par_vec[i] > par_max[i])
		{
			par_vec[i] = par_max[i];
			eqp_all = 1;
		}
EQP_DEBUG */
		par_vec[i] = par_def[i];
	}
	return (eqp_all = 0);
}

/******************************************************************************
Function:		GE_WaitBusy
Description:	wait if eeprom is busy, for a max time
Input:			max delay in milliseconds
Output:			=0 -> OK, !=0 -> NOK
******************************************************************************/
uint16_t GE_WaitBusy(uint16_t max_time_ms)
{
	if (eqp_all)
	{
		return (eqp_all);
	}
	while (EQP_EqpBusy())
	{
		if (0 != max_time_ms)
		{
			DELAY1MS(1);
			max_time_ms--;
		}
		else
		{
			return (eqp_all = 1);
		}
	}
	return (0);
}

/******************************************************************************
Function:		GE_RamToEqp
Description:	copies all the parameters from ram to eeprom
Input:			nothing
Output:			=0 -> OK, !=0 -> NOK
Modifies:		eeprom content
Note:			to be used out of main loop, due to the high delay
******************************************************************************/
uint16_t GE_RamToEqp(void)
{
	static uint16_t i;

	// wait if busy
	if (GE_WaitBusy(MAX_WAIT))
	{
		return (eqp_all);
	}

	// read/write cycle
	for (i = 0; i < NUM_PAR; i++)
	{
		if (EQP_ReadWord(PAR_ADD(i), &eqp_aus))
		{
			return (eqp_all = 1);
		}
		if (par_vec[i] != eqp_aus)
		{
			if (EQP_WriteWord(PAR_ADD(i), par_vec[i]))
			{
				return (eqp_all = 1);
			}
			if (GE_WaitBusy(MAX_WAIT))
			{
				return (eqp_all);
			}
			if (EQP_ReadWord(PAR_ADD(i), &eqp_aus))
			{
				return (eqp_all = 1);
			}
			if (par_vec[i] != eqp_aus)
			{
				return (eqp_all = 1);
			}
		}
	}
	return (eqp_all = 0);
}

/******************************************************************************
Function:		GE_EqpToRam
Description:	if par0!=RIP_DEF copies all parameters from eeprom to
				ram, else copies all default values in ram parameter
				table, then copies all the parameters from ram to eeprom
Input:			nothing
Output:			=0 -> OK, !=0 -> NOK
Modifies:		internal parameters vector or eeprom content
Note:			to be used out of main loop, due to the high delay
******************************************************************************/
uint16_t GE_EqpToRam(void)
{
	uint16_t i;

	// wait if busy
	if (GE_WaitBusy(MAX_WAIT))
	{
		return (eqp_all);
	}

	// par 0 reading
	if (EQP_ReadWord(PAR_ADD(0), &eqp_aus))
	{
		return (eqp_all = 1);
	}

	// default values restoration if requested, or special modes reset
	if ((RIP_DEF == eqp_aus) || (65535 == eqp_aus))
	{
		for (i = 0; i < NUM_PAR; i++)
		{
			par_vec[i] = par_def[i];
		}
		if (GE_RamToEqp())
		{
			return (eqp_all = 1);
		}
	}
	else if (0 != eqp_aus)
	{
		eqp_aus = 0;
		if (EQP_WriteWord(PAR_ADD(0), eqp_aus))
		{
			return (eqp_all = 1);
		}
		if (GE_WaitBusy(MAX_WAIT))
		{
			return (eqp_all);
		}
		if (EQP_ReadWord(PAR_ADD(0), &eqp_aus))
		{
			return (eqp_all = 1);
		}
		if (0 != eqp_aus)
		{
			return (eqp_all = 1);
		}
	}
	par_vec[0] = eqp_aus;

	// parameters reading
	for (i = 1; i < NUM_PAR; i++)
	{
		if (EQP_ReadWord(PAR_ADD(i), &eqp_aus))
		{
			return (eqp_all = 1);
		}
		if (eqp_aus < par_min[i])
		{
			return (eqp_all = 1);
		}
		if (eqp_aus > par_max[i])
		{
			return (eqp_all = 1);
		}
		par_vec[i] = eqp_aus;
	}
	return (eqp_all);
}

/******************************************************************************
Function:		GE_UpdEqp
Description:	copies a modified parameter from ram to eeprom
Input:			nothing
Output:			=0 -> OK, !=0 -> NOK
Modifies:		eeprom content
Note:			to be called in main loop
******************************************************************************/
uint16_t GE_UpdEqp(void)
{
	if (0 > modif)
	{
		return (eqp_all);
	}
	if (eqp_bsy)
	{
		if (EQP_EqpBusy())
		{
			eqp_bsy--;
			if (0 == eqp_bsy)
			{
				goto agg_eqp_err;
			}
			return (eqp_all);
		}
		eqp_bsy = 0;
		if (EQP_ReadWord(PAR_ADD(modif), &eqp_aus))
		{
			goto agg_eqp_err;
		}
		if (par_vec[modif] != eqp_aus)
		{
			goto agg_eqp_err;
		}
	}
	else if ( (0 == modif) || ( (NUM_PAR > modif) && (0 == eqp_all) ) )
	{
		if (EQP_ReadWord(PAR_ADD(modif), &eqp_aus))
		{
			goto agg_eqp_err;
		}
		else if (par_vec[modif] != eqp_aus)
		{
			if (EQP_WriteWord(PAR_ADD(modif), par_vec[modif]))
			{
				goto agg_eqp_err;
			}
			eqp_bsy = NL_W_EQP;
			return (eqp_all);
		}
	}
	modif = -1;
	return (eqp_all);

agg_eqp_err:
	modif = -1;
	return (eqp_all = 1);
}

/******************************************************************************
Function:		GE_SetPar
Description:	insertion of a parameter in eqp fifo, to be updated
Input:			parameter value, parameter address in the vector
Output:			nothing
Modifies:		internal fifo
******************************************************************************/
void GE_SetPar(uint16_t val, uint16_t add)
{
	eqp_fifo[mas_ind] = val;
	eqp_add[mas_ind++] = add;
	if (NUM_FIFO_EQP <= mas_ind)
	{
		mas_ind = 0;
	}
	return;
}

/******************************************************************************
Function:		GE_EqpMan
Description:	eeprom parameters modification management
Input:			parameter value, parameter address in the vector
Output:			=0 -> OK, !=0 -> NOK
Modifies:		internal fifo, eeprom content
Note:			to be called in main loop
******************************************************************************/
uint16_t GE_EqpMan()
{
	static uint16_t	stato, eqpbusy1;
	uint16_t		pnum, indice;

#define	NOR_0	( 10 )
#define	MOD_0	( 20 )

	switch(stato)
	{
		case NOR_0:
			if ((0 != eqp_all) && (sla_ind != mas_ind) && (IND_EQP_SEL_OP != eqp_add[sla_ind]))
			{
				eqp_fifo[sla_ind] = 0;
				eqp_add[sla_ind] = 0;
				if ((NUM_FIFO_EQP - 1) > sla_ind)
				{
					sla_ind++;
				}
				else
				{
					sla_ind = 0;
				}
			}
			else if ((sla_ind != mas_ind) && ((0 == eqp_all) || (IND_EQP_SEL_OP == eqp_add[sla_ind])))
			{
				pnum = eqp_fifo[sla_ind];
				indice = eqp_add[sla_ind];
				eqp_fifo[sla_ind] = 0;
				eqp_add[sla_ind] = 0;
				if ((NUM_FIFO_EQP - 1) > sla_ind)
				{
					sla_ind++;
				}
				else
				{
					sla_ind = 0;
				}
				if ((IND_EQP_SEL_OP == indice) && (0 != pnum) && (RIP_DEF != pnum))
				{
					par_vec[indice] = pnum;	// modif not touched, no eeprom operations
				}
				else if ((par_min[indice] <= pnum) && (par_max[indice] >= pnum))
				{
					par_vec[indice] = pnum;
					modif = indice;
					eqpbusy1 = 4;
					stato = MOD_0;
				}
			}
			break;
		case MOD_0:
			if (0 <= modif)
			{
				if (0 < eqpbusy1)
				{
					eqpbusy1--;
					break;
				}
				else
				{
					return (eqp_all = 1);
				}
			}
			// no break;
		default:
			stato = NOR_0;
			break;
	}

	// eeprom management
	return (GE_UpdEqp());

}

#else	// EEPROM_USED
void GE_IniEqp(void)
{
	return;
}
uint16_t GE_RamToEqp(void)
{
	return (0);
}
uint16_t GE_EqpToRam(void)
{
	return (0);
}
uint16_t GE_EqpMan(void)
{
	return (0);
}
uint16_t GE_DefToRam(void)
{
	uint16_t i;

	for (i = 0; i < NUM_PAR; i++)
	{
		par_vec[i] = par_def[i];
	}
	return (0);
}
void GE_SetPar(uint16_t dta, uint16_t add)
{
	if ((par_min[add] > dta) || (par_max[add] < dta))
	{
		return;
	}
	par_vec[add] = dta;
}
#endif 	// EEPROM_USED

// EOF ges_eqp.c

