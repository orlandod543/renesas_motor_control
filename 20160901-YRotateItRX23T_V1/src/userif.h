
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		userif.h
*	Project:	YRotateIt-RX23T (three shunts)
*	Revision:	1.0
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	Header for userif.c
*******************************************************************************/

#ifndef _USERIF_H
#define _USERIF_H

#include "typedefine.h"

/******************************************************************************
Macro definitions
******************************************************************************/
#define N_RAM_READ		( 32 )
#define N_RAM_WRITE		( 8 )
#define MAX_SAM_CNT		( 120 )	// can be asked all at the same time
#define OUTBUF_XDIM		( 128 )
#define OUTBUF_SDIM		OUTBUF_XDIM
#define OUTBUF_CDIM		(OUTBUF_XDIM * 2)


/******************************************************************************
Typedef definitions
******************************************************************************/
typedef union
{
	uint16_t		ram_tab[N_RAM_READ];
	struct
	{
		int16_t		rif;		//  0	internal speed reference
		int16_t		rpm;		//  1	measured speed
		int16_t		fre;		//  2	imposed frequency
		int16_t		id;			//  3	direct current
		int16_t		iq;			//  4	quadrature current
		int16_t		vd;			//  5	direct voltage
		int16_t		vq;			//  6	quadrature voltage
		int16_t		vb;			//  7	bus voltage
		int16_t		all;		//  8	alarm
		int16_t		flg;		//  9	flags
		int16_t		toti;		// 10	current vector amplitude
		int16_t		totv;		// 11	voltage vector amplitude
		int16_t		du0;		// 12
		int16_t		du1;		// 13
		int16_t		du2;		// 14
		int16_t		du3;		// 15
		int16_t		mod;		// 16	mode
		int16_t		rs;			// 17	stator resistance
		int16_t		ls;			// 18	synchronous inductance	
		int16_t		fl;			// 19	permanent magnet flux
		int16_t		kpi;		// 20	current loop kp
		int16_t		kii;		// 21	current loop ki
		int16_t		pwmf;		// 22	pwm frequency [Hz]
		int16_t		sf;			// 23	sampling frequency [Hz]
		int16_t		ena;		// 24	extra features enable flags
		int16_t		du4;		// 25
		int16_t		du5;		// 26
		int16_t		du6;		// 27
		int16_t		du7;		// 28
		int16_t		du8;		// 29
		int16_t		du9;		// 30
		int16_t		du10;		// 31
	}				var;
}	UIF_R_t;

typedef union
{
	uint16_t		ram_tab[N_RAM_WRITE];
	struct
	{
		int16_t		trg;		//  0	trigger
		int16_t		mod;		//  1	mode
		int16_t		rif;		//  2	speed reference
		int16_t		cra;		//  3	current ratio
		int16_t		sel;		//  4	variable and time scale selection
		int16_t		du0;		//  5
		int16_t		du1;		//  6
		int16_t		du2;		//  7
	}				var;
}	UIF_W_t;

typedef union
{
	int16_t			ss[OUTBUF_SDIM];
	uint8_t			uc[OUTBUF_CDIM];
}	OUTB_t;


/******************************************************************************
Variables definitions/declarations
******************************************************************************/

#ifdef _USERIF_C
UIF_R_t				UIF_R;
UIF_W_t				UIF_W;
OUTB_t				outbuf, outbuf1;
uint16_t			*wbuf = ((uint16_t *)(outbuf1.ss));
uint16_t			*rbuf = ((uint16_t *)(outbuf.ss));
uint16_t			*pbuf = ((uint16_t *)(outbuf.ss));
#else	// _USERIF_C
extern UIF_R_t		UIF_R;
extern UIF_W_t		UIF_W;
extern OUTB_t		outbuf, outbuf1;
extern uint16_t		*wbuf;
extern uint16_t		*rbuf;
extern uint16_t		*pbuf;
#endif	// _USERIF_C

/******************************************************************************
Functions Prototypes
******************************************************************************/

/******************************************************************************
Function: 		UI_RTX232_Init
Description:	serial communication init
Input:			nothing
Output:			nothing
Modifies:		internal variables involved in serial communication
******************************************************************************/
void UI_RTX232_Init(void);

/******************************************************************************
Function:		UI_RTX232_ERI
Description:	Receive error interrupt service routine
Input:			none
Output:			none
******************************************************************************/
void UI_RTX232_ERI(void);

/******************************************************************************
Function:		UI_RTX232_RXI
Description:	Receive buffer full interrupt service routine
Input:			none
Output:			none
******************************************************************************/
void UI_RTX232_RXI(void);

/******************************************************************************
Function:		UI_RTX232_TXI
Description:	Transmission buffer empty interrupt service routine
Input:			none
Output:			none
******************************************************************************/
void UI_RTX232_TXI(void);

/******************************************************************************
Function:		UI_RTX232_TEI
Description:	Transmission end interrupt service routine
Input:			none
Output:			none
******************************************************************************/
void UI_RTX232_TEI(void);

/******************************************************************************
Function:		UI_RTX232_RXDelMan
Description:	RX delay management
Input:			none
Output:			none
Note:			to be called in the control interrupt
******************************************************************************/
void UI_RTX232_RXDelMan(void);

/******************************************************************************
Function:		UI_RTX232_Management
Description:	serial communication management
Input:			nothing
Output:			nothing
Modifies:		rx/tx buffer, internal flags
Note:			to be called in the main loop;
				the slave is always waiting for commands (rx); when a command
				is received, it is decoded and, if this makes sense, executed;
				if the command is correctly received or if there are errors
				in the data or in the addresses, the slave sends an answer.
******************************************************************************/
void UI_RTX232_Management(void);

/******************************************************************************
Function:		UI_LedMan
Description:	led blinking management
Input:			nothing
Output:			nothing
Modifies:		leds status
Note:			to be called in the main loop
******************************************************************************/
void UI_LedMan(void);


/*	--- Special Working Modes ---

	Special working modes are controlled trough UIF_W.var.mod:
	if it is 0 then normal mode is used, other values stay for
	special modes. A feedback of the working mode is given in UIF_R.var.mod,
	which is equal to the working mode.
	If the working mode requires a trigger from the GUI to execute some
	operations, UIF_W.var.trg bit0 is used (it is automatically cleared after
	being detected); the feedback regarding the requested operation status
	is given by some bits in UIF_R.var.flg: bit9=1 means BUSY, while if it is 0 means
	READY, bitA=1 means last operation was ENDED_NOK, while if it is 0 means ENDED_OK.
	Trigger requests are not accepted if UIF_R.var.sta is not ready. It means that the
	GUI has to stay in polling for this flag after a trigger.
	Other important flags are bit7 (alarm), and bit8 which indicates that the motor
	is driven.
	Working status can be changed only when the motor in not driven (UIF_R.var.flg
	has bit8 equal to 0).
*/

#define ENA_CURPI_TUN	UIF_R.var.ena |= WSET0;
#define ENA_CURPI_AUT	UIF_R.var.ena |= WSET1;
#define ENA_AUTO_IDEN	UIF_R.var.ena |= WSET2;
#define ENA_OSCI_WIND	UIF_R.var.ena |= WSET3;

#define NORM_MODE_CODE	( 0 )	// normal inverter behaviour
#define CURPI_TUN_CODE	( 1 )	// current PI gains manual tuning mode
#define CURPI_AUT_CODE	( 2 )	// current PI gains automatic detection mode
#define AUTO_IDEN_CODE	( 3 )	// motor parameters auto-identification mode

#define NORM_MODE_REQ	( NORM_MODE_CODE == UIF_W.var.mod )
#define CURPI_TUN_REQ	( CURPI_TUN_CODE == UIF_W.var.mod )
#define CURPI_AUT_REQ	( CURPI_AUT_CODE == UIF_W.var.mod )
#define AUTO_IDEN_REQ	( AUTO_IDEN_CODE == UIF_W.var.mod )

#define NORM_MODE		( NORM_MODE_CODE == UIF_R.var.mod )
#define CURPI_TUN		( CURPI_TUN_CODE == UIF_R.var.mod )
#define CURPI_AUT		( CURPI_AUT_CODE == UIF_R.var.mod )
#define AUTO_IDEN		( AUTO_IDEN_CODE == UIF_R.var.mod )

#define NOT_NORM_MODE	( NORM_MODE_CODE != UIF_R.var.mod )
#define NOT_CURPI_TUN	( CURPI_TUN_CODE != UIF_R.var.mod )
#define NOT_CURPI_AUT	( CURPI_AUT_CODE != UIF_R.var.mod )
#define NOT_AUTO_IDEN	( AUTO_IDEN_CODE != UIF_R.var.mod )

#define SET_NORM_MODE	UIF_R.var.mod = NORM_MODE_CODE;
#define SET_CURPI_TUN	UIF_R.var.mod = CURPI_TUN_CODE;
#define SET_CURPI_AUT	UIF_R.var.mod = CURPI_AUT_CODE;
#define SET_AUTO_IDEN	UIF_R.var.mod = AUTO_IDEN_CODE;

#define ALRM_ON			(UIF_R.var.flg & WSET7)
#define ALRM_OFF		(! ALRM_ON)
#define COM_ON			(UIF_R.var.flg & WSET8)
#define COM_OFF			(! COM_ON)
#define STA_BSY			(UIF_R.var.flg & WSET9)
#define STA_RDY			(! STA_BSY)
#define END_NOK			(UIF_R.var.flg & WSETA)
#define END_OK			(! END_NOK)
#define ICOM_ON			(UIF_R.var.flg & WSETB)
#define ICOM_OFF		(! ICOM_ON)
#define SET_ALRM_ON		UIF_R.var.flg |= WSET7;
#define RES_ALRM_ON		UIF_R.var.flg &= WCLR7;
#define SET_COM_ON		UIF_R.var.flg |= WSET8;
#define RES_COM_ON		UIF_R.var.flg &= WCLR8;
#define SET_STA_BSY		UIF_R.var.flg |= WSET9;
#define RES_STA_BSY		UIF_R.var.flg &= WCLR9;
#define SET_END_NOK		UIF_R.var.flg |= WSETA;
#define RES_END_NOK		UIF_R.var.flg &= WCLRA;
#define SET_ICOM_ON		UIF_R.var.flg |= WSETB;
#define RES_ICOM_ON		UIF_R.var.flg &= WCLRB;

#define GUI_TRI			(UIF_W.var.trg & WSET0)
#define RES_GUI_TRI		UIF_W.var.trg &= WCLR0;
#define GUI_STP			(UIF_W.var.trg & WSET1)
#define RES_GUI_STP		UIF_W.var.trg &= WCLR1;

#endif	// _USERIF_H

// EOF userif.h

