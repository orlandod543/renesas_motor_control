
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		pws_E6108A.h
*	Project:	YRotateIt-RX23T (three shunts)
*	Revision:	1.0
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	External power stage (E6108A) related definitions
*******************************************************************************/

#ifndef	_PWS_E6108A_H
#define _PWS_E6108A_H

//#include "typedefine.h"

// current conversion
// NOTE: the current is considered positive is going out from the inverter
#define RSHUNT_OHM			( 0.05 )		// shunt value [Ohm]
#define RSGAIN				( 4.7 )			// circuit gain

// DC bus voltage conversion
#define RVBUSH_OHM			( 400000.0 )	// high side split resistor [Ohm]
#define RVBUSL_OHM			( 4700.0 )		// low side split resistor [Ohm]

// deadtimes to be inserted by MTU
#define DEADTIM_NS			( 1000 )		// deatimes [ns]

// minimum on time of low arms, to guarantee bootstrap operations
// this time should guarantee that the lower switch commutates effectively for enough time to
// charge the bootstrap capacitor
#define MIN_TON_LOW_NS		( 1500 )		// [ns] larger than deadtimes

// current conversion delay respect pwm trough (should be lesser than one half of MIN_TON_LOW_NS)
#define CON_DELAY_NS		(  700 )		// [ns] lesser than one half of minimum on time of low arms

// voltages
#define VB_NOM_VOL			( 311 )			// 220 * sqrt(2) nominal bus voltage [V]

// remove following comment to avoid ANY compensation in modulation
#define NOCOMP

// INVERTER VOLTAGE ERRORS COMPENSATION (valid only if NOCOMP is commented)
// (should be applied if you use exact integration flux estimation method,
// or to obtain better results in auto-tuning, especially with IGBT power stages)
#ifndef NOCOMP
// The voltage error model is:
//		VOLTAGE_ERROR( I ) = sign( I ) * ( V1( | I | ) + V2( | I | ) )
//		V1( I ) = ( vbus * pwm_fre_Hz * T1( | I | ) )
// The first term (V1) gives reason for the error due to the switching times (deadtimes etc.) and it is
// proportional to the bus voltage and to the pwm frequency; theoretically it is independent by the
// amplitude of the current, but in practice there is a variation with the current which needs compensation.
// The second term (V2) is only dependent on the current, and gives reason of the voltage drop on the power
// switches; the second term could be neglected with MOSFET power stage.

// NOTE: due to the table measurement method used, a multiply by sqrt(3)/2 is needed, both for the voltage
// tables and for the current.

#define INL_VB_VOL		( 320 )				// bus voltage used for table calculation
#define INL_FRE_HZ		( 16000 )			// PWM frequency used for table calculation
#define T_INL_DELTAIN	( 0.1 * 0.866 )		// current step 0.1A ( * (sqrt(3)/2))
#define T_INL_UDIN		((float32_t)(1.0 / T_INL_DELTAIN))

// T1 table
#define T_INL1_DIM		( 16 )				// rom table dimension
#define T_INL1_INDMAX	( 11 )				// max index
#define INL1_01			( 1.64 * 0.866 / (INL_VB_VOL * INL_FRE_HZ) )	//  1
#define INL1_02			( 3.35 * 0.866 / (INL_VB_VOL * INL_FRE_HZ) )	//  2
#define INL1_03			( 4.61 * 0.866 / (INL_VB_VOL * INL_FRE_HZ) )	//  3
#define INL1_04			( 5.27 * 0.866 / (INL_VB_VOL * INL_FRE_HZ) )	//  4
#define INL1_05			( 5.79 * 0.866 / (INL_VB_VOL * INL_FRE_HZ) )	//  5
#define INL1_06			( 6.11 * 0.866 / (INL_VB_VOL * INL_FRE_HZ) )	//  6
#define INL1_07			( 6.43 * 0.866 / (INL_VB_VOL * INL_FRE_HZ) )	//  7
#define INL1_08			( 6.76 * 0.866 / (INL_VB_VOL * INL_FRE_HZ) )	//  8
#define INL1_09			( 6.97 * 0.866 / (INL_VB_VOL * INL_FRE_HZ) )	//  9
#define INL1_10			( 7.19 * 0.866 / (INL_VB_VOL * INL_FRE_HZ) )	// 10
#define INL1_11			( 7.20 * 0.866 / (INL_VB_VOL * INL_FRE_HZ) )	// 11
#define INL1_12			( 7.20 * 0.866 / (INL_VB_VOL * INL_FRE_HZ) )	// 12
#define INL1_13			( 7.20 * 0.866 / (INL_VB_VOL * INL_FRE_HZ) )	// 13
#define INL1_14			( 7.20 * 0.866 / (INL_VB_VOL * INL_FRE_HZ) )	// 14
#define INL1_15			( 7.20 * 0.866 / (INL_VB_VOL * INL_FRE_HZ) )	// 15

// compensation for powers witches voltage drop (V2)
#define PSVDROP			// comment to avoid compensation for power switches voltage drop
#ifdef PSVDROP
// V2 table
#define T_INL2_DIM		( 16 )				// rom table dimension
#define T_INL2_INDMAX	(  4 )				// max index
#define INL2_01			( 0.50 * 0.866 )	//  1
#define INL2_02			( 0.65 * 0.866 )	//  2
#define INL2_03			( 0.69 * 0.866 )	//  3
#define INL2_04			( 0.70 * 0.866 )	//  4
#define INL2_05			( 0.70 * 0.866 )	//  5
#define INL2_06			( 0.70 * 0.866 )	//  6
#define INL2_07			( 0.70 * 0.866 )	//  7
#define INL2_08			( 0.70 * 0.866 )	//  8
#define INL2_09			( 0.70 * 0.866 )	//  9
#define INL2_10			( 0.70 * 0.866 )	// 10
#define INL2_11			( 0.70 * 0.866 )	// 11
#define INL2_12			( 0.70 * 0.866 )	// 12
#define INL2_13			( 0.70 * 0.866 )	// 13
#define INL2_14			( 0.70 * 0.866 )	// 14
#define INL2_15			( 0.70 * 0.866 )	// 15
#endif	// PSVDROP

#endif	// NOCOMP

#endif	// _PWS_E6108A_H

// EOF pws_E6108A.h
