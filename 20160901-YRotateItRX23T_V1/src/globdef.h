
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		globdef.h
*	Project:	YRotateIt-RX23T (three shunts)
*	Revision:	1.0
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	Global definitions
*******************************************************************************/

#ifndef	_GLOBDEF_H
#define	_GLOBDEF_H

// inclusions
#include <machine.h>
#include <stdlib.h>
#include <mathf.h>
#include "typedefine.h"
#include "iodefine.h"
#include "globvar.h"
#include "mask.h"
#include "const_def.h"

// NULL/NOP
#ifndef NULL
	#define NULL		((void *)( 0 ))
#endif                        
#ifndef NOP
	#define NOP			{ nop(); }
#endif                        

// Delay
#define US1				(    5 )
#define US10			(   50 )
#define US100			(  500 )
#define US1000			( 5000 )
#define DELAY(X, Y)		{	volatile uint16_t x, y;				\
							for(x = ( X ); x > 0; x--) {		\
								for(y = ( Y ); y > 0; y--) {	\
									NOP					}	}	}
#define DELAY1US(X)		DELAY(X, US1)
#define DELAY10US(X)	DELAY(X, US10)
#define DELAY100US(X)	DELAY(X, US100)
#define DELAY1MS(X)		DELAY(X, US1000)

// 16bit mul_w(), mulu_w(), div_w(), divu_w() define
#ifndef MYMULDIV
#define MYMULDIV
#define	mul_w(X, Y)		(( int32_t)((( int16_t)( X )) * (( int16_t)( Y ))))
#define	mulu_w(X, Y)	((uint32_t)(((uint16_t)( X )) * ((uint16_t)( Y ))))
#define	div_w(X, Y)		(( int16_t)((( int32_t)( X )) / (( int16_t)( Y ))))
#define	divu_w(X, Y)	((uint16_t)(((uint32_t)( X )) / ((uint16_t)( Y ))))
#endif	// MYMULDIV

// macro for Module Stop Control
#define WAKEUP_S12AD	{	SYSTEM.PRCR.WORD = 0xA502;	/* enable writing PRC1 */	\
							MSTP(S12AD) = 0;			/* wake-up S12AD */			\
							SYSTEM.PRCR.WORD = 0xA500;	/* disable writing PRC1 */	}
#define WAKEUP_MTU		{	SYSTEM.PRCR.WORD = 0xA502;	/* enable writing PRC1 */	\
							MSTP(MTU) = 0;				/* wake-up MTU */			\
							SYSTEM.PRCR.WORD = 0xA500;	/* disable writing PRC1 */	}
#define WAKEUP_SCI1		{	SYSTEM.PRCR.WORD = 0xA502;	/* enable writing PRC1 */	\
							MSTP(SCI1) = 0;				/* wake-up SCI1 */			\
							SYSTEM.PRCR.WORD = 0xA500;	/* disable writing PRC1 */	}
#define WAKEUP_DTC		{	SYSTEM.PRCR.WORD = 0xA502;	/* enable writing PRC1 */	\
							MSTP(DTC) = 0;				/* wake-up DTC */			\
							SYSTEM.PRCR.WORD = 0xA500;	/* disable writing PRC1 */	}

// macro for A/D conversions
#define INI_S12AD		{	S12AD.ADCSR.WORD = 0x0000;		\
							S12AD.ADANSA0.WORD = 0x0000;	\
							S12AD.ADANSA1.WORD = 0x0000;	\
							S12AD.ADCER.WORD = 0x0100;		}
#define	S12AD_START		{ S12AD.ADCSR.BIT.ADST = 1; }	// B15
#define	S12AD_STOP		{ S12AD.ADCSR.BIT.ADST = 0; }	// B15
#define	S12AD_C_ON		(0 != S12AD.ADCSR.BIT.ADST)		// B15
#define	WAIT_S12AD		{ while(S12AD_C_ON); }
#define AN0_IU			((uint16_t) S12AD.ADDR0)		// pin56 P40 IU I AN0
#define AN1_IV			((uint16_t) S12AD.ADDR1)		// pin55 P41 IV I AN1
#define AN2_IW			((uint16_t) S12AD.ADDR2)		// pin54 P42 IW I AN2
#define AN3_VB			((uint16_t) S12AD.ADDR3)		// pin53 P43 VB I AN3
#define AN4_VU			((uint16_t) S12AD.ADDR4)		// pin52 P44 VU I AN4
#define AN5_VV			((uint16_t) S12AD.ADDR5)		// pin51 P45 VV I AN5
#define AN6_VW			((uint16_t) S12AD.ADDR6)		// pin50 P46 VW I AN6
#define AN7_EX			((uint16_t) S12AD.ADDR7)		// pin49 P47 EX I AN7
#define SEL_IU_AD		S12AD.ADANSA0.WORD = WSET0;
#define SEL_IV_AD		S12AD.ADANSA0.WORD = WSET1;
#define SEL_IW_AD		S12AD.ADANSA0.WORD = WSET2;
#define SEL_VB_AD		S12AD.ADANSA0.WORD = WSET3;
#define SEL_VU_AD		S12AD.ADANSA0.WORD = WSET4;
#define SEL_VV_AD		S12AD.ADANSA0.WORD = WSET5;
#define SEL_VW_AD		S12AD.ADANSA0.WORD = WSET6;
#define SEL_EX_AD		S12AD.ADANSA0.WORD = WSET7;
#define SEL_CUR_AD		S12AD.ADANSA0.WORD = (WSET0 | WSET1 | WSET2);
#define SEL_CVB_AD		S12AD.ADANSA0.WORD = (WSET0 | WSET1 | WSET2 | WSET3);
#define SEL_VEX_AD		S12AD.ADANSA0.WORD = (WSET4 | WSET5 | WSET6 | WSET7);

// macro for complementary pwm MTU timer 3, 4 (active state = high)
#define	BUF_SP			MTU.TCBRA	// buffer for TCDRA (semiper)
#define	BUF_SPD			MTU3.TGRC	// buffer for TGRA3 (semiper + deadtime)
#define	BUF_U			MTU3.TGRD	// buffer for TGRB3 (duty U)
#define	BUF_V			MTU4.TGRC	// buffer for TGRA4 (duty V)
#define	BUF_W			MTU4.TGRD	// buffer for TGRB4 (duty W) (LAST TO BE WRITTEN!)
#define ENABLE_OUT		MTU.TOERA.BYTE = 0xFF;		// enable all PWM outputs
#define DISABLE_OUT		MTU.TOERA.BYTE = 0xC0;		// disable all PWM outputs
#define OUT_ENABLED		(0xFF == MTU.TOERA.BYTE)
#define NOT_OUT_ENABLED	(0xFF != MTU.TOERA.BYTE)
#define SET_PWM(X, Y, Z)	{ BUF_U = ( X ); BUF_V = ( Y ); BUF_W = ( Z ); }	

// hardware related defines: inputs
#define EXT_SUP_ON		(0 != PORT0.PIDR.BIT.B1)	// pin04 P01 EXT_SUP I
#define EXT_SUP_OFF		(0 == PORT0.PIDR.BIT.B1)	// pin04 P01 EXT_SUP I
#define KEY3_OFF		(0 != PORT9.PIDR.BIT.B3)	// pin30 P93 KEY3 I
#define KEY3_ON			(0 == PORT9.PIDR.BIT.B3)	// pin30 P93 KEY3 I
#define KEY2_OFF		(0 != PORT9.PIDR.BIT.B2)	// pin31 P92 KEY2 I
#define KEY2_ON			(0 == PORT9.PIDR.BIT.B2)	// pin31 P92 KEY2 I
#define KEY1_OFF		(0 != PORT9.PIDR.BIT.B1)	// pin32 P92 KEY1 I
#define KEY1_ON			(0 == PORT9.PIDR.BIT.B1)	// pin32 P92 KEY1 I
#define HALLU_OFF		(0 != PORTB.PIDR.BIT.B3)	// pin23 PB3 HALL-U I
#define HALLU_ON		(0 == PORTB.PIDR.BIT.B3)	// pin23 PB3 HALL-U I
#define HALLV_OFF		(0 != PORTB.PIDR.BIT.B2)	// pin24 PB2 HALL-V I
#define HALLV_ON		(0 == PORTB.PIDR.BIT.B2)	// pin24 PB2 HALL-V I
#define HALLW_OFF		(0 != PORTB.PIDR.BIT.B1)	// pin25 PB1 HALL-W I
#define HALLW_ON		(0 == PORTB.PIDR.BIT.B1)	// pin25 PB1 HALL-W I

// hardware related defines: outputs
#define SET_DL3			PORTB.PODR.BIT.B0 = 0;		// pin26 PB0 LED3 O
#define CLR_DL3			PORTB.PODR.BIT.B0 = 1;		// pin26 PB0 LED3 O
#define DL3_ON			(0 == PORTB.PODR.BIT.B0)	// pin26 PB0 LED3 O
#define DL3_OFF			(0 != PORTB.PODR.BIT.B0)	// pin26 PB0 LED3 O
#define SET_DL2			PORT9.PODR.BIT.B4 = 0;		// pin29 P94 LED2 O
#define CLR_DL2			PORT9.PODR.BIT.B4 = 1;		// pin29 P94 LED2 O
#define DL2_ON			(0 == PORT9.PODR.BIT.B4)	// pin29 P94 LED2 O
#define DL2_OFF			(0 != PORT9.PODR.BIT.B4)	// pin29 P94 LED2 O
#define SET_LIMOFF		PORTB.PODR.BIT.B4 = 1;		// pin21 PB4 LIM-OFF O
#define RES_LIMOFF		PORTB.PODR.BIT.B4 = 0;		// pin21 PB4 LIM-OFF O
#define LIMOFF_ON		(0 != PORTB.PODR.BIT.B4)	// pin21 PB4 LIM-OFF O
#define LIMOFF_OFF		(0 == PORTB.PODR.BIT.B4)	// pin21 PB4 LIM-OFF O

// global flags (global variables defined in "globvar.h" are used)
#define	TRGED			(0 != flg0.bit.b0)
#define	RBUSY			(0 != flg0.bit.b1)
#define	CSELD			(0 != flg0.bit.b2)
#define	FREE_03			(0 != flg0.bit.b3)
#define	FREE_04			(0 != flg0.bit.b4)
#define	FREE_05			(0 != flg0.bit.b5)
#define	FREE_06			(0 != flg0.bit.b6)
#define	FREE_07			(0 != flg0.bit.b7)

#define	NOT_TRGED		(0 == flg0.bit.b0)
#define	NOT_RBUSY		(0 == flg0.bit.b1)
#define	NOT_CSELD		(0 == flg0.bit.b2)
#define	NOT_FREE_03		(0 == flg0.bit.b3)
#define	NOT_FREE_04		(0 == flg0.bit.b4)
#define	NOT_FREE_05		(0 == flg0.bit.b5)
#define	NOT_FREE_06		(0 == flg0.bit.b6)
#define	NOT_FREE_07		(0 == flg0.bit.b7)

#define	SET_TRGED		{ flg0.bit.b0 = 1; }
#define	SET_RBUSY		{ flg0.bit.b1 = 1; }
#define	SET_CSELD		{ flg0.bit.b2 = 1; }
#define	SET_FREE_03		{ flg0.bit.b3 = 1; }
#define	SET_FREE_04		{ flg0.bit.b4 = 1; }
#define	SET_FREE_05		{ flg0.bit.b5 = 1; }
#define	SET_FREE_06		{ flg0.bit.b6 = 1; }
#define	SET_FREE_07		{ flg0.bit.b7 = 1; }

#define	RES_TRGED		{ flg0.bit.b0 = 0; }
#define	RES_RBUSY		{ flg0.bit.b1 = 0; }
#define	RES_CSELD		{ flg0.bit.b2 = 0; }
#define	RES_FREE_03		{ flg0.bit.b3 = 0; }
#define	RES_FREE_04		{ flg0.bit.b4 = 0; }
#define	RES_FREE_05		{ flg0.bit.b5 = 0; }
#define	RES_FREE_06		{ flg0.bit.b6 = 0; }
#define	RES_FREE_07		{ flg0.bit.b7 = 0; }

#define	FREE_10			(0 != flg1.bit.b0)
#define	FREE_11			(0 != flg1.bit.b1)
#define	FREE_12			(0 != flg1.bit.b2)
#define	FREE_13			(0 != flg1.bit.b3)
#define	FREE_14			(0 != flg1.bit.b4)
#define	FREE_15			(0 != flg1.bit.b5)
#define	FREE_16			(0 != flg1.bit.b6)
#define	FREE_17			(0 != flg1.bit.b7)

#define	NOT_FREE_10		(0 == flg1.bit.b0)
#define	NOT_FREE_11		(0 == flg1.bit.b1)
#define	NOT_FREE_12		(0 == flg1.bit.b2)
#define	NOT_FREE_13		(0 == flg1.bit.b3)
#define	NOT_FREE_14		(0 == flg1.bit.b4)
#define	NOT_FREE_15		(0 == flg1.bit.b5)
#define	NOT_FREE_16		(0 == flg1.bit.b6)
#define	NOT_FREE_17		(0 == flg1.bit.b7)

#define	SET_FREE_10		{ flg1.bit.b0 = 1; }
#define	SET_FREE_11		{ flg1.bit.b1 = 1; }
#define	SET_FREE_12		{ flg1.bit.b2 = 1; }
#define	SET_FREE_13		{ flg1.bit.b3 = 1; }
#define	SET_FREE_14		{ flg1.bit.b4 = 1; }
#define	SET_FREE_15		{ flg1.bit.b5 = 1; }
#define	SET_FREE_16		{ flg1.bit.b6 = 1; }
#define	SET_FREE_17		{ flg1.bit.b7 = 1; }

#define	RES_FREE_10		{ flg1.bit.b0 = 0; }
#define	RES_FREE_11		{ flg1.bit.b1 = 0; }
#define	RES_FREE_12		{ flg1.bit.b2 = 0; }
#define	RES_FREE_13		{ flg1.bit.b3 = 0; }
#define	RES_FREE_14		{ flg1.bit.b4 = 0; }
#define	RES_FREE_15		{ flg1.bit.b5 = 0; }
#define	RES_FREE_16		{ flg1.bit.b6 = 0; }
#define	RES_FREE_17		{ flg1.bit.b7 = 0; }

#endif	// _GLOBDEF_H

// EOF globdef.h

