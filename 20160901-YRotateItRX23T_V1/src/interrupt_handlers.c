
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		interrupt_handlers.c
*	Project:	YRotateIt-RX23T (three shunts)
*	Revision:	1.0
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Renesas Project Generator
*	Contents:	Interrupt programs
*******************************************************************************/

#define _INTERRUPT_HANDLERS_C

#include <machine.h>
#include "iodefine.h"
#include "vect.h"
#include "motorcontrol.h"
#include "userif.h"

#pragma section IntPRG

// Exception (Supervisor Instruction)
void Excep_SuperVisorInst(void) {/* brk(); */}

// Exception (Access Instruction)
void Excep_AccessInst(void) {/* brk(); */}

// Exception (Undefined Instruction)
void Excep_UndefinedInst(void) {/* brk(); */}

// Exception (Floating Point)
void Excep_FloatingPoint(void) {/* brk(); */}

// NMI
void NonMaskableInterrupt(void) {/* brk(); */}

// Dummy
void Dummy(void) {/* brk(); */}

// BRK
void Excep_BRK(void) { wait(); }

// BSC BUSERR
void Excep_BSC_BUSERR(void) {/* brk(); */}

// FCU FRDYI
void Excep_FCU_FRDYI(void) {/* brk(); */}

// ICU SWINT
void Excep_ICU_SWINT(void) {/* brk(); */}

// CMT0 CMI0
void Excep_CMT0_CMI0(void) {/* brk(); */}

// CMT0 CMI1
void Excep_CMT0_CMI1(void) {/* brk(); */}

// CMT1 CMI2
void Excep_CMT1_CMI2(void) {/* brk(); */}

// CMT1 CMI3
void Excep_CMT1_CMI3(void) {/* brk(); */}

// CAC FERRF
void Excep_CAC_FERRF(void) {/* brk(); */}

// CAC MENDF
void Excep_CAC_MENDF(void) {/* brk(); */}

// CAC OVFF
void Excep_CAC_OVFF(void) {/* brk(); */}

// RSPI0 SPEI0
void Excep_RSPI0_SPEI0(void) {/* brk(); */}

// RSPI0 SPRI0
void Excep_RSPI0_SPRI0(void) {/* brk(); */}

// RSPI0 SPTI0
void Excep_RSPI0_SPTI0(void) {/* brk(); */}

// RSPI0 SPII0
void Excep_RSPI0_SPII0(void) {/* brk(); */}

// DOC DOPCF
void Excep_DOC_DOPCF(void) {/* brk(); */}

// ICU IRQ0
void Excep_ICU_IRQ0(void) {/* brk(); */}

// ICU IRQ1
void Excep_ICU_IRQ1(void) {/* brk(); */}

// ICU IRQ2
void Excep_ICU_IRQ2(void) {/* brk(); */}

// ICU IRQ3
void Excep_ICU_IRQ3(void) {/* brk(); */}

// ICU IRQ4
void Excep_ICU_IRQ4(void) {/* brk(); */}

// ICU IRQ5
void Excep_ICU_IRQ5(void) {/* brk(); */}

// LVD LVD1
void Excep_LVD_LVD1(void) {/* brk(); */}

// LVD LVD2
void Excep_LVD_LVD2(void) {/* brk(); */}

// S12AD S12ADI0
void Excep_S12AD_S12ADI0(void) { MC_ConInt(); }

// S12AD GBADI
void Excep_S12AD_GBADI(void) {/* brk(); */}

// CMPC0 CMPC0
void Excep_CMPC0_CMPC0(void) {/* brk(); */}

// CMPC1 CMPC1
void Excep_CMPC1_CMPC1(void) {/* brk(); */}

// CMPC2 CMPC2
void Excep_CMPC2_CMPC2(void) {/* brk(); */}

// MTU0 TGIA0
void Excep_MTU0_TGIA0(void) {/* brk(); */}

// MTU0 TGIB0
void Excep_MTU0_TGIB0(void) {/* brk(); */}

// MTU0 TGIC0
void Excep_MTU0_TGIC0(void) {/* brk(); */}

// MTU0 TGID0
void Excep_MTU0_TGID0(void) {/* brk(); */}

// MTU0 TCIV0
void Excep_MTU0_TCIV0(void) {/* brk(); */}

// MTU0 TGIE0
void Excep_MTU0_TGIE0(void) {/* brk(); */}

// MTU0 TGIF0
void Excep_MTU0_TGIF0(void) {/* brk(); */}

// MTU1 TGIA1
void Excep_MTU1_TGIA1(void) {/* brk(); */}

// MTU1 TGIB1
void Excep_MTU1_TGIB1(void) {/* brk(); */}

// MTU1 TCIV1
void Excep_MTU1_TCIV1(void) {/* brk(); */}

// MTU1 TCIU1
void Excep_MTU1_TCIU1(void) {/* brk(); */}

// MTU2 TGIA2
void Excep_MTU2_TGIA2(void) {/* brk(); */}

// MTU2 TGIB2
void Excep_MTU2_TGIB2(void) {/* brk(); */}

// MTU2 TCIV2
void Excep_MTU2_TCIV2(void) {/* brk(); */}

// MTU2 TCIU2
void Excep_MTU2_TCIU2(void) {/* brk(); */}

// MTU3 TGIA3
void Excep_MTU3_TGIA3(void) {/* brk(); */}

// MTU3 TGIB3
void Excep_MTU3_TGIB3(void) {/* brk(); */}

// MTU3 TGIC3
void Excep_MTU3_TGIC3(void) {/* brk(); */}

// MTU3 TGID3
void Excep_MTU3_TGID3(void) {/* brk(); */}

// MTU3 TCIV3
void Excep_MTU3_TCIV3(void) {/* brk(); */}

// MTU4 TGIA4
void Excep_MTU4_TGIA4(void) {/* brk(); */}

// MTU4 TGIB4
void Excep_MTU4_TGIB4(void) {/* brk(); */}

// MTU4 TGIC4
void Excep_MTU4_TGIC4(void) {/* brk(); */}

// MTU4 TGID4
void Excep_MTU4_TGID4(void) {/* brk(); */}

// MTU4 TCIV4
void Excep_MTU4_TCIV4(void) {/* brk(); */}

// MTU5 TGIU5
void Excep_MTU5_TGIU5(void) {/* brk(); */}

// MTU5 TGIV5
void Excep_MTU5_TGIV5(void) {/* brk(); */}

// MTU5 TGIW5
void Excep_MTU5_TGIW5(void) {/* brk(); */}

// POE OEI1
void Excep_POE_OEI1(void) { MC_FaultInt(); }

// POE OEI3
void Excep_POE_OEI3(void) {/* brk(); */}

// POE OEI4
void Excep_POE_OEI4(void) {/* brk(); */}

// TMR0 CMIA0
void Excep_TMR0_CMIA0(void) {/* brk(); */}

// TMR0 CMIB0
void Excep_TMR0_CMIB0(void) {/* brk(); */}

// TMR0 OVI0
void Excep_TMR0_OVI0(void) {/* brk(); */}

// TMR1 CMIA1
void Excep_TMR1_CMIA1(void) {/* brk(); */}

// TMR1 CMIB1
void Excep_TMR1_CMIB1(void) {/* brk(); */}

// TMR1 OVI1
void Excep_TMR1_OVI1(void) {/* brk(); */}

// TMR2 CMIA2
void Excep_TMR2_CMIA2(void) {/* brk(); */}

// TMR2 CMIB2
void Excep_TMR2_CMIB2(void) {/* brk(); */}

// TMR2 OVI2
void Excep_TMR2_OVI2(void) {/* brk(); */}

// TMR3 CMIA3
void Excep_TMR3_CMIA3(void) {/* brk(); */}

// TMR3 CMIB3
void Excep_TMR3_CMIB3(void) {/* brk(); */}

// TMR3 OVI3
void Excep_TMR3_OVI3(void) {/* brk(); */}

// SCI1 ERI1
void Excep_SCI1_ERI1(void) { UI_RTX232_ERI(); }

// SCI1 RXI1
void Excep_SCI1_RXI1(void) { UI_RTX232_RXI(); }

// SCI1 TXI1
void Excep_SCI1_TXI1(void) { UI_RTX232_TXI(); }

// SCI1 TEI1
void Excep_SCI1_TEI1(void) { UI_RTX232_TEI(); }

// SCI5 ERI5
void Excep_SCI5_ERI5(void) {/* brk(); */}

// SCI5 RXI5
void Excep_SCI5_RXI5(void) {/* brk(); */}

// SCI5 TXI5
void Excep_SCI5_TXI5(void) {/* brk(); */}

// SCI5 TEI5
void Excep_SCI5_TEI5(void) {/* brk(); */}

// RIIC0 EEI0
void Excep_RIIC0_EEI0(void) {/* brk(); */}

// RIIC0 RXI0
void Excep_RIIC0_RXI0(void) {/* brk(); */}

// RIIC0 TXI0
void Excep_RIIC0_TXI0(void) {/* brk(); */}

// RIIC0 TEI0
void Excep_RIIC0_TEI0(void) {/* brk(); */}

// EOF interrupt_handlers.c

