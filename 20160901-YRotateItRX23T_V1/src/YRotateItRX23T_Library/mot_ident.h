
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		mot_ident.h
*	Project:	YRotateIt-RX23T motor control library
*	Revision:	1.0 (Little Endian Data)
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	Header for mot_ident.c
*******************************************************************************/

#ifndef _MOT_IDENT_H
#define _MOT_IDENT_H

// MISRA types inclusion
#ifndef _MISRA_H_INCLUDED_
#define _MISRA_H_INCLUDED_

typedef          char		char_t;
typedef unsigned char		uint8_t;
typedef unsigned short int  uint16_t;
typedef unsigned long int   uint32_t;
typedef signed   char		int8_t;
typedef signed   short int	int16_t;
typedef signed   long int	int32_t;
typedef          float		float32_t;
typedef          double		float64_t;

#ifdef TRUE
#undef TRUE
#endif

#ifdef FALSE
#undef FALSE
#endif

typedef enum bool
{
	FALSE = 0,
	TRUE = 1
}	bool_t;

#endif // _MISRA_H_INCLUDED_

/*
	The reference profile for the measurement should be:
	1)	the current rises from zero to the HIG_CUR level (HIG_CUR_RATIO of the maximum current),
		taking the time CUR_RISE_TIME; the motor (free) will align and stabilize in the aligned
		position; this takes in total RAMP1_TIME
	2)	the first measurement cycle is performed, taking MEAS_TIME
	3)	the current falls till MID_CUR level, and stabilizes, taking in total RAMP2_TIME
	4)	the second measurement cycle is performed, taking MEAS_TIME
	5)	the speed rises from zero to MEAS_SPEED, taking SPEED_RISE_TIME, and stabilizes,
		taking in total RAMP3_TIME
	6)	the third measurement cycle is performed, taking MEAS_TIME
	7)	the current falls till LOW_CUR level, and stabilizes, taking in total RAMP4_TIME
	8)	the fourth measurement cycle is performed, taking MEAS_TIME
*/

//#define IDENT_SPEED_HZ	( 20.0f )
#define HIG_CUR_RATIO		( 0.8f )
#define MID_CUR_RATIO		( 0.6f )
#define LOW_CUR_RATIO		( 0.4f )
#define CUR_RISE_TIME_MS	( 1000 )
#define SPE_RISE_TIME_MS	( 2500 )
#define RAMP1_TIME_MS		(CUR_RISE_TIME_MS + 2000)
#define RAMP2_TIME_MS		( 2000 )
#define RAMP3_TIME_MS		(SPE_RISE_TIME_MS + 1500)
#define RAMP4_TIME_MS		( 2000 )
#define MEAS_TIME_MS		( 1000 )
#define ID_TIM1_MS			RAMP1_TIME_MS					// current rising + alignment + stabilization
#define ID_TIM2_MS			(ID_TIM1_MS + MEAS_TIME_MS)
#define ID_TIM3_MS			(ID_TIM2_MS + RAMP2_TIME_MS)	// current falling + stabilization
#define ID_TIM4_MS			(ID_TIM3_MS + MEAS_TIME_MS)
#define ID_TIM5_MS			(ID_TIM4_MS + RAMP3_TIME_MS)	// speed rising + stabilization
#define ID_TIM6_MS			(ID_TIM5_MS + MEAS_TIME_MS)
#define ID_TIM7_MS			(ID_TIM6_MS + RAMP4_TIME_MS)	// current falling + stabilization
#define ID_TIM8_MS			(ID_TIM7_MS + MEAS_TIME_MS)
#define ID_TIM9_MS			(ID_TIM8_MS + 100)				// wait more than a loop

/*******************************************************************************
Function:		MC_Ident_IniVar
Description:	initialization routine
Input:			sf_hz sampling frequency [Hz]
Output:			nothing
Note:			to be called before the identification session
Revision:		1.0
*******************************************************************************/
void MC_Ident_IniVar(float32_t sf_hz);

/*******************************************************************************
Function:		MC_Ident_SetMea
Description:	measurement set routine
Input:			id d current sample [A]
				iq q current sample [A]
				vd d voltage sample [V]
				vq q voltage sample [V]
				om angular velocity sample [rad/sec]
Output:			nothing
Note:			to be called every sampling period, during all the
				identification session
Revision:		1.0
*******************************************************************************/
void MC_Ident_SetMea(float32_t id, float32_t iq, float32_t vd, float32_t vq, float32_t om);

/*******************************************************************************
Function:		MC_Ident_UpdAcc
Description:	measurement routine
Input:			nothing
Output:			nothing
Note:			accumulates the measurement samples
Revision:		1.0
*******************************************************************************/
void MC_Ident_UpdAcc(void);

/*******************************************************************************
Function:		MC_Ident_Step1
Description:	trigger routine
Input:			nothing
Output:			nothing
Note:			indicates the end of the first measurement stage
Revision:		1.0
*******************************************************************************/
void MC_Ident_Step1(void);

/*******************************************************************************
Function:		MC_Ident_Step2
Description:	trigger routine
Input:			nothing
Output:			nothing
Note:			indicates the end of the second measurement stage
Revision:		1.0
*******************************************************************************/
void MC_Ident_Step2(void);

/*******************************************************************************
Function:		MC_Ident_Step3
Description:	trigger routine
Input:			nothing
Output:			nothing
Note:			indicates the end of the third measurement stage
Revision:		1.0
*******************************************************************************/
void MC_Ident_Step3(void);

/*******************************************************************************
Function:		MC_Ident_Step4
Description:	trigger routine
Input:			nothing
Output:			nothing
Note:			indicates the end of the fourth measurement stage
Revision:		1.0
*******************************************************************************/
void MC_Ident_Step4(void);

/*******************************************************************************
Function:		MC_Ident_CalMotPar
Description:	final identification routine
Input:			*res stator phase resistance address
				*ind stator synchronous inductance address
				*flx permanent magnets flux address
Output:			nothing
Modifies:		res stator phase resistance [Ohm]
				ind stator synchronous inductance [Henry]
				flx permanent magnets flux [Weber]
Note:			to be called once, after the four measurements steps
Revision:		1.0
*******************************************************************************/
void MC_Ident_CalMotPar(float32_t *res, float32_t *ind, float32_t *flx);

#endif	// _MOT_IDENT_H

// EOF mot_ident.h

