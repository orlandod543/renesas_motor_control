
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		PI_tun.h
*	Project:	YRotateIt-RX23T motor control library
*	Revision:	1.0 (Little Endian Data)
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	Header for PI_tun.c
*******************************************************************************/

#ifndef _PI_TUN_H
#define _PI_TUN_H

// MISRA types inclusion
#ifndef _MISRA_H_INCLUDED_
#define _MISRA_H_INCLUDED_

typedef          char		char_t;
typedef unsigned char		uint8_t;
typedef unsigned short int  uint16_t;
typedef unsigned long int   uint32_t;
typedef signed   char		int8_t;
typedef signed   short int	int16_t;
typedef signed   long int	int32_t;
typedef          float		float32_t;
typedef          double		float64_t;

#ifdef TRUE
#undef TRUE
#endif

#ifdef FALSE
#undef FALSE
#endif

typedef enum bool
{
	FALSE = 0,
	TRUE = 1
}	bool_t;

#endif // _MISRA_H_INCLUDED_

/*******************************************************************************
Function:		PIT_IniVar
Description:	internal variables init
Input:			is current step amplitude [A]
				vn nominal bus voltage [V]
				sp sampling period [sec]
Output:			nothing
Revision:		1.0
*******************************************************************************/
void PIT_IniVar(float32_t is, float32_t vn, float32_t sp);

/*******************************************************************************
Function:		PIT_SetCur
Description:	measurement setting routine; it is needed to pass to the
				tuning algorithm the currents measurements
Input:			id d current measurement [A]
				iq q current measurement [A]
Output:			nothing
Revision:		1.0
*******************************************************************************/
void PIT_SetCur(float32_t id, float32_t iq);

/*******************************************************************************
Function:		PIT_RampUp
Description:	voltage ramp up
Input:			*v voltage reference address
Output:			1 if final level is reached, 0 otherwise
Modifies:		v voltage reference [V]
Revision:		1.0
*******************************************************************************/
uint16_t PIT_RampUp(float32_t *v);

/*******************************************************************************
Function:		PIT_Measure
Description:	waiting routine, when delay elapsed takes the measurements
				samples
Input:			*v voltage reference address
Output:			1 if delay elapsed, 0 otherwise
Modifies:		v voltage reference [V]
Revision:		1.0
*******************************************************************************/
uint16_t PIT_Measure(float32_t *v);

/*******************************************************************************
Function:		PIT_RampDown
Description:	voltage ramp down
Input:			*v voltage reference address
Output:			1 if final level is reached, 0 otherwise
Modifies:		v voltage reference [V]
Revision:		1.0
*******************************************************************************/
uint16_t PIT_RampDown(float32_t *v);

/*******************************************************************************
Function:		PIT_VoltStep
Description:	voltage step
Input:			*v voltage reference address
Output:			1 if measurements finished, 0 otherwise
Modifies:		v voltage reference [V]
Revision:		1.0
*******************************************************************************/
uint16_t PIT_VoltStep(float32_t *v);

/*******************************************************************************
Function:		PIT_ReadKP
Description:	output routine (it returns the calculated kp value)
Input:			nothing
Output:			proportional gain
Revision:		1.0
*******************************************************************************/
float32_t PIT_ReadKP(void);

/*******************************************************************************
Function:		PIT_ReadKI
Description:	output routine (it returns the calculated ki value)
Input:			nothing
Output:			integral gain
Revision:		1.0
*******************************************************************************/
float32_t PIT_ReadKI(void);

#endif	// _PI_TUN_H

// EOF PI_tun.h

