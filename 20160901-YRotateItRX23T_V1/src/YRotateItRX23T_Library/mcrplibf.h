
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		mcrplibf.h
*	Project:	YRotateIt-RX23T motor control library
*	Revision:	1.0 (Little Endian Data)
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	Motor control library functions definition
*******************************************************************************/

/* THIS FILE IS INCLUDED ALSO IN LIBRARY SO NOTHING CAN BE MODIFIED! */

#ifndef _MCRPLIBF_H
#define _MCRPLIBF_H

// MISRA types inclusion
#ifndef _MISRA_H_INCLUDED_
#define _MISRA_H_INCLUDED_

typedef          char		char_t;
typedef unsigned char		uint8_t;
typedef unsigned short int  uint16_t;
typedef unsigned long int   uint32_t;
typedef signed   char		int8_t;
typedef signed   short int	int16_t;
typedef signed   long int	int32_t;
typedef          float		float32_t;
typedef          double		float64_t;

#ifdef TRUE
#undef TRUE
#endif

#ifdef FALSE
#undef FALSE
#endif

typedef enum bool
{
	FALSE = 0,
	TRUE = 1
}	bool_t;

#endif // _MISRA_H_INCLUDED_

// floating point numerical constants
#ifndef F_CONST
#define F_CONST
#define PI				( 3.141592654 )		// 180
#define PISIXTH			(PI / 6.0)			// 030
#define PIFOURTHS		(PI / 4.0)			// 045
#define PITHIRDS		(PI / 3.0)			// 060
#define PIHALVES		(PI / 2.0)			// 090
#define TWOPITHIRDS		(PI * 2.0 / 3.0)	// 120
#define THREEPIFOURTHS	(PI * 3.0 / 4.0)	// 135
#define FIVEPISIXTH		(PI * 5.0 / 6.0)	// 150
#define SEVENPISIXTH	(PI * 7.0 / 6.0)	// 210
#define FIVEPIFOURTHS	(PI * 5.0 / 4.0)	// 225
#define FOURPITHIRDS	(PI * 4.0 / 3.0)	// 240
#define THREEPIHALVES	(PI * 3.0 / 2.0)	// 270
#define FIVEPITHIRDS	(PI * 5.0 / 3.0)	// 300
#define SEVENPIFOURTHS	(PI * 7.0 / 4.0)	// 315
#define ELEVENPISIXTH	(PI * 11.0 / 6.0)	// 330
#define TWOPI			(PI * 2.0)			// 360
#define	ONETHIRD		(1.0 / 3.0)
#define	TWOTHIRDS		(2.0 / 3.0)
#define	FOURTHIRDS		(4.0 / 3.0)
#define	SQRT2			( 1.414213562 )
#define	SQRT3			( 1.732050808 )
#define SQRT2D2			(SQRT2 / 2.0)
#define SQRT3D2			(SQRT3 / 2.0)
#define SQRT3D3			(SQRT3 / 3.0)
#define FPI				((float32_t) PI)
#define FPISIXTH		((float32_t) PISIXTH)
#define FPIFOURTHS		((float32_t) PIFOURTHS)
#define FPITHIRDS		((float32_t) PITHIRDS)
#define FPIHALVES		((float32_t) PIHALVES)
#define FTWOPITHIRDS	((float32_t) TWOPITHIRDS)
#define FTHREEPIFOURTHS	((float32_t) THREEPIFOURTHS)
#define FFIVEPISIXTH	((float32_t) FIVEPISIXTH)
#define FSEVENPISIXTH	((float32_t) SEVENPISIXTH)
#define FFIVEPIFOURTHS	((float32_t) FIVEPIFOURTHS)
#define FFOURPITHIRDS	((float32_t) FOURPITHIRDS)
#define FTHREEPIHALVES	((float32_t) THREEPIHALVES)
#define FFIVEPITHIRDS	((float32_t) FIVEPITHIRDS)
#define FSEVENPIFOURTHS	((float32_t) SEVENPIFOURTHS)
#define FELEVENPISIXTH	((float32_t) ELEVENPISIXTH)
#define FTWOPI			((float32_t) TWOPI)
#define	FONETHIRD		((float32_t) ONETHIRD)
#define	FTWOTHIRDS		((float32_t) TWOTHIRDS)
#define	FFOURTHIRDS		((float32_t) FOURTHIRDS)
#define	FSQRT2			((float32_t) SQRT2)
#define	FSQRT3			((float32_t) SQRT3)
#define FSQRT2D2		((float32_t) SQRT2D2)
#define FSQRT3D2		((float32_t) SQRT3D2)
#define FSQRT3D3		((float32_t) SQRT3D3)
#endif	// F_CONST

/* ANGLES MANAGEMENT FUNCTIONS ************************************************/
#ifndef FANGLES

/*******************************************************************************
Function:		McrpLibf_AngleNrm
Description:	angle normalization
Input:			ang[rad]
Output:			a=ang, -FPI<a<=FPI
Revision:		1.0
*******************************************************************************/
float32_t McrpLibf_AngleNrm(float32_t ph);

#define FANGLES
#endif	// FANGLES
/* END OF ANGLES MANAGEMENT FUNCTIONS ****************************************/

/* TRANSFORMATION FUNCTIONS **************************************************/
#ifndef FTRANS

/******************************************************************************
Function:		McrpLibf_AngleSet
Description:	internal angle variables setting
Input:			ang[rad], -FPI<=ang<FPI
Output:			nothing
Modifies:		internal_angle (base for (d, q) or (alpha, beta) transformations)
Revision:		1.0
******************************************************************************/
void McrpLibf_AngleSet(float32_t ang);

/******************************************************************************
Function:		McrpLibf_uv_alphabeta
Description:	unitary gain transformation (u, v, (w))->(alpha, beta):
					alpha=u
					beta=(u+2*v)/sqrt(3)
Input:			u component, v component,
				alpha component address, beta component address
Output:			nothing
Modifies:		alpha, beta components
Note:			simplified transf.: (u+v+w) is supposed to be zero!
Revision:		1.0
******************************************************************************/
void McrpLibf_uv_alphabeta(	float32_t u, float32_t v,
							float32_t *alpha, float32_t *beta);

/******************************************************************************
Function:		McrpLibf_uvw_alphabeta
Description:	unitary gain transformation (u, v, w)->(alpha, beta):
					alpha=(2/3)(u-v/2-w/2)=(1/3)(2u-v-w)
					beta=(2/3)(v*SQRT(3)/2-w*SQRT(3)/2)=(v-w)/SQRT(3)
Input:			u component, v component, w component,
				alpha component address, beta component address
Output:			nothing
Modifies:		alpha, beta components
Note:			complete transformation: no matter if (u+v+w)!=0
Revision:		1.0
******************************************************************************/
void McrpLibf_uvw_alphabeta(float32_t u, float32_t v, float32_t w,
							float32_t *alpha, float32_t *beta);

/******************************************************************************
Function:		McrpLibf_alphabeta_uv
Description:	unitary gain transformation (alpha, beta)->(u, v, (w)):
					u=alpha
					v=((sqrt(3)*beta-alpha)/2
Input:			alpha component, beta component,
				u component address, v component address
Output:			nothing
Modifies:		u, v components
Note:			third component can be obtained as w=-u-v
Revision:		1.0
******************************************************************************/
void McrpLibf_alphabeta_uv(	float32_t alpha, float32_t beta,
							float32_t *u, float32_t *v);

/******************************************************************************
Function:		McrpLibf_alphabeta_dq
Description:	unitary gain transformation (alpha, beta)->(d, q):
					d=alpha*cos(internal_angle)+beta*sin(internal_angle)
					q=alpha*cos(internal_angle)-beta*sin(internal_angle)
Input:			alpha component, beta component,
				d component address, q component address
Output:			nothing
Modifies:		d, q components
Revision:		1.0
******************************************************************************/
void McrpLibf_alphabeta_dq(	float32_t alpha, float32_t beta,
							float32_t *d, float32_t *q);

/******************************************************************************
Function:		McrpLibf_dq_alphabeta
Description:	unitary gain transformation (d, q)->(alpha, beta):
					alpha=d*cos(internal_angle)-q*sin(internal_angle)
					beta=d*sin(internal_angle)+q*cos(internal_angle)
Input:			d component, q component,
				alpha component address, beta component address
Output			nothing
Modifies:		alpha, beta components
Revision:		1.0
******************************************************************************/
void McrpLibf_dq_alphabeta(	float32_t d, float32_t q,
							float32_t *alpha, float32_t *beta);

/******************************************************************************
Function:		McrpLibf_xy_rt
Description:	transformation (x, y)->(ro, theta)
					ro=|(x, y)|
					theta=arg(x, y)
Input:			x component, y component,
				ro component address, theta component address
Output:			nothing
Modifies:		ro, theta components
Revision:		1.0
******************************************************************************/
void McrpLibf_xy_rt(float32_t x, float32_t y,
					float32_t *ro, float32_t *theta);

/******************************************************************************
Function:		McrpLibf_rt_xy
Description:	transformation (ro, theta)->(x, y)
					x=ro*cos(theta)
					y=ro*sin(theta)
Input:			ro component, theta component,
				x component address, y component address
Output:			nothing
Modifies:		x, y components
Revision:		1.0
******************************************************************************/
void McrpLibf_rt_xy(float32_t ro, float32_t theta,
					float32_t *x, float32_t *y);

#define FTRANS
#endif	// FTRANS
/* END OF TRANSFORMATION FUNCTIONS *******************************************/

/* CONTROL FUNCTIONS *********************************************************/
#ifndef FCONTROL

/******************************************************************************
Function:		McrpLibf_PIW
Description:	Proportional and Integral control, with anti-windup
Input:			error err,
				maximum output maxp,
				minimum output maxn,
				proportional gain kp,
				integral gain ki (see note)
				integral memory address *intg
Output:			PI control output value
Modifies:		integral memory intg
Note:			the sampling freq is not kept into consideration; this means
				that the integration is simply and addition of err*ki; so
				it is ki itself which has to keep into account the sampling
				frequency, i.e. the true KI is ki*Tc=ki/Fc
Revision:		1.0
******************************************************************************/
float32_t McrpLibf_PIW(	float32_t err, float32_t maxp, float32_t maxn,
						float32_t kp, float32_t ki, float32_t *intg);

#define FCONTROL
#endif	// FCONTROL
/* END OF CONTROL FUNCTIONS **************************************************/

/* EXACT INTEGRATION FLUX ESTIMATION FUNCTIONS *******************************/
#ifndef EIFEF

/******************************************************************************
Function:		McrpLibf_FluxEst_SetParam
Description:	exact integration flux estimation functions parameters setting
Input:			rs phase resistance [Ohm]
				ls syncronous inductance [Henry]
				mf permanent magnets flux vector amplitude [Weber] (bemf [Volt/(rad/sec)])
				fg flux amplitude feedback gain (it's a pure number)
				sp sampling period [sec]
				cf speed filter cut-off frequency [Hz]
Output:			nothing
Modifies:		internal memories
******************************************************************************/
void McrpLibf_FluxEst_SetParam(	float32_t rs, float32_t ls, float32_t mf,
								float32_t fg, float32_t sp, float32_t cf);

/******************************************************************************
Function:		McrpLibf_FluxEst
Description:	exact integration flux estimation
Input:			va alpha voltage sample [Volt]
				vb beta voltage sample [Volt]
				ia alpha current sample [Volt]
				ib beta current sample [Volt]
Output:			nothing
Modifies:		internal memories
Note:			to be called every sampling period
******************************************************************************/
void McrpLibf_FluxEst(float32_t va, float32_t vb, float32_t ia, float32_t ib);

/******************************************************************************
Function:		McrpLibf_FluxEst_SetFlux
Description:	exact integration flux setting
Input:			ph permanent magnets flux vector phase referred to (alpha, beta) system [rad]
				ia alpha current sample [Volt]
				ib beta current sample [Volt]
				av permanent magnets flux vector angular velocity [rad/sec]
Output:			nothing
Modifies:		internal memories
******************************************************************************/
void McrpLibf_FluxEst_SetFlux(float32_t ph, float32_t ia, float32_t ib, float32_t av);

/******************************************************************************
Function:		McrpLibf_FluxEst_GetAmp
Description:	estimated flux amplitude reading function
Input:			nothing
Output:			estimated permanent magnets flux vector amplitude [Weber]
******************************************************************************/
float32_t McrpLibf_FluxEst_GetAmp(void);

/******************************************************************************
Function:		McrpLibf_FluxEst_GetPhase
Description:	estimated flux phase reading function
Input:			nothing
Output:			estimated permanent magnets flux vector phase referred to (alpha, beta) system [rad]
******************************************************************************/
float32_t McrpLibf_FluxEst_GetPhase(void);

/******************************************************************************
Function:		McrpLibf_FluxEst_GetSpeed
Description:	estimated angular velocity reading function
Input:			nothing
Output:			estimated permanent magnets flux vector angular velocity [rad/s]
******************************************************************************/
float32_t McrpLibf_FluxEst_GetSpeed(void);

#define EIFEF
#endif	// EIFEF
/* END OF EXACT INTEGRATION FLUX ESTIMATION FUNCTIONS ************************/


/* APPROXIMATED INTEGRATION FLUX ESTIMATION FUNCTIONS ************************/
#ifndef AIFEF

/******************************************************************************
Function:		McrpLibf_FluxEstA_SetParam
Description:	approximated integration flux estimation functions parameters setting
Input:			rs phase resistance [Ohm]
				ls synchronous inductance [Henry]
				mf permanent magnets flux vector amplitude [Weber] (bemf [Volt/(rad/sec)])
				f1 flux estimation filter cut-off frequency [Hz]
				sp sampling period [sec] (> 0.0f)
				f2 speed filter cut-off frequency [Hz]
Output:			nothing
Modifies:		internal memories
******************************************************************************/
void McrpLibf_FluxEstA_SetParam(float32_t rs, float32_t ls, float32_t mf,
								float32_t f1, float32_t sp, float32_t f2);

/******************************************************************************
Function:		McrpLibf_FluxEstA
Description:	approximated integration flux estimation
Input:			va alpha voltage sample [Volt]
				vb beta voltage sample [Volt]
				ia alpha current sample [Volt]
				ib beta current sample [Volt]
Output:			nothing
Modifies:		internal memories
Note:			to be called every sampling period
******************************************************************************/
void McrpLibf_FluxEstA(float32_t va, float32_t vb, float32_t ia, float32_t ib);

/******************************************************************************
Function:		McrpLibf_FluxEstA_SetFlux
Description:	approximated integration flux setting
Input:			ph permanent magnets flux vector phase referred to (alpha, beta) system [rad]
				va alpha voltage sample [Volt]
				vb beta voltage sample [Volt]
				ia alpha current sample [Volt]
				ib beta current sample [Volt]
				av permanent magnets flux vector angular velocity [rad/sec]
Output:			nothing
Modifies:		internal memories
Note:			this function should be used only in DC conditions (speed=0)
******************************************************************************/
void McrpLibf_FluxEstA_SetFlux(	float32_t ph, float32_t va, float32_t vb,
								float32_t ia, float32_t ib, float32_t av);

/******************************************************************************
Function:		McrpLibf_FluxEstA_GetAmp
Description:	estimated flux amplitude reading function
Input:			nothing
Output:			estimated permanent magnets flux vector amplitude [Weber]
******************************************************************************/
float32_t McrpLibf_FluxEstA_GetAmp(void);

/******************************************************************************
Function:		McrpLibf_FluxEstA_GetPhase
Description:	estimated flux phase reading function
Input:			nothing
Output:			estimated permanent magnets flux vector phase referred to (alpha, beta) system [rad]
******************************************************************************/
float32_t McrpLibf_FluxEstA_GetPhase(void);

/******************************************************************************
Function:		McrpLibf_FluxEstA_GetSpeed
Description:	estimated angular velocity reading function
Input:			nothing
Output:			estimated permanent magnets flux vector angular velocity [rad/s]
******************************************************************************/
float32_t McrpLibf_FluxEstA_GetSpeed(void);

#define AIFEF
#endif	// AIFEF
/* END OF APPROXIMATED INTEGRATION FLUX ESTIMATION FUNCTIONS *****************/

#endif	// _MCRPLIBF_H

