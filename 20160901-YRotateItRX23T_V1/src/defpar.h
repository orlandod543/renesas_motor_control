/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		defpar.h
*	Contents:	Parameters default values
*******************************************************************************/

#ifndef _DEFPAR_H
#define _DEFPAR_H

// Motor default parameters
/*
#define SEL_OP_DEF      			(      0 )		//     0 <= X <=  32767		//00. Operation Select
#define RPM_MIN_DEF     			(    600 )		//     0 <= X <=   3000		//01. Minimum Speed
#define RPM_MAX_DEF     			(   3500 )		//    60 <= X <=  30000		//02. Maximum Speed
#define R_ACC_DEF       			(   3000 )		//     1 <= X <=  10000		//03. Acceleration
#define R_DEC_DEF       			(   3000 )		//     1 <= X <=  10000		//04. Deceleration
#define C_POLI_DEF      			(      2 )		//     1 <= X <=     24		//05. Polar Couples
#define I_START_DEF     			(    200 )		//     0 <= X <=  30000		//06. Startup Current
#define I_MAX_DEF       			(   3700 )		//     0 <= X <=  30000		//07. Maximum Current
#define R_STA_DEF       			(     60 )		//     0 <= X <=  30000		//08. Stator Resistance
#define L_SYN_DEF       			(     04 )		//     0 <= X <=  30000		//09. Synchronous Inductance
#define PM_FLX_DEF      			(     60 )		//     0 <= X <=  30000		//10. Permanent Magnets Flux
#define KP_CUR_DEF      			(     20 )		//     0 <= X <=  30000		//11. Current Loop Kp
#define KI_CUR_DEF      			(     50 )		//     0 <= X <=  30000		//12. Current Loop Ki
#define KP_VEL_DEF      			(     50 )		//     0 <= X <=  30000		//13. Speed Loop Kp
#define KI_VEL_DEF      			(    200 )		//     0 <= X <=  30000		//14. Speed Loop Ki
#define FB_GAIN_DEF     			(    300 )		//     0 <= X <=  30000		//15. Flux Feedback Gain
#define PHA_OFF_DEF     			(      0 )		//     0 <= X <=    359		//16. Phase Offset
#define SUP_TIM_DEF     			(   1000 )		//   100 <= X <=  30000		//17. StartUp Time
#define FLX_FTAU_DEF    			(     80 )		//     8 <= X <=   1000		//18. App. FE Time Constant
#define SAM_FRE_DEF     			(   8000 )		//  4000 <= X <=  14000		//19. Sampling Frequency
#define F_RATIO_DEF     			(      2 )		//     1 <= X <=      4		//20. PWM/SAM Freq. Ratio
*/ // default parameters for motor MSSI-040H
#define SEL_OP_DEF      	(      0 )	//     0 <= X <=  32767	// operation selection code
#define RPM_MIN_DEF     	(      0 )	//     0 <= X <=   3000	// minimum speed [rpm]
#define RPM_MAX_DEF     	(   6200 )	//    60 <= X <=  30000	// maximum speed [rpm]
#define R_ACC_DEF       	(  10000 )	//     1 <= X <=  10000	// acceleration ramp [rpm/sec]
#define R_DEC_DEF       	(  10000 )	//     1 <= X <=  10000	// deceleration ramp [rpm/sec]
#define C_POLI_DEF      	(      3 )	//     1 <= X <=     24	// polar couples number
#define I_START_DEF     	(    200 )	//     0 <= X <=  30000	// start-up current (peak) [Amp/AMP_RES]
#define I_MAX_DEF       	(   3700 )	//     0 <= X <=  30000	// maximum current (peak) [Amp/AMP_RES]
#define R_STA_DEF       	(     40 )	//     0 <= X <=  30000	// stator phase resistance [Ohm/OHM_RES]
#define L_SYN_DEF       	(      4 )	//     0 <= X <=  30000	// synchronous inductance [Hen/HEN_RES]
#define PM_FLX_DEF      	(     60 )	//     0 <= X <=  30000	// permanent magnet flux [Web/WEB_RES]
#define KP_CUR_DEF      	(      7 )	//     0 <= X <=  30000	// current control proportional gain
#define KI_CUR_DEF      	(     10 )	//     0 <= X <=  30000	// current control integral gain
#define KP_VEL_DEF      	(      4 )	//     0 <= X <=  30000	// speed control proportional gain
#define KI_VEL_DEF      	(      2 )	//     0 <= X <=  30000	// speed control integral gain
#define FB_GAIN_DEF     	(    300 )	//     0 <= X <=  30000	// flux amplitude feedback gain (exact. est.)
#define PHA_OFF_DEF     	(      0 )	//     0 <= X <=    359	// offset angle [deg]
#define SUP_TIM_DEF     	(   1000 )	//   100 <= X <=  30000	// speed rising time (0 -> min_speed) during start-up [Sec/SEC_RES]
#define FLX_FTAU_DEF    	(     80 )	//     8 <= X <=   1000	// flux est. filter time constant [Sec/SEC_RES] (approx. est.)
#define SAM_FRE_DEF     	(   8000 )	//  4000 <= X <=  14000	// sampling frequency [Hz]
#define F_RATIO_DEF     	(      2 )	//     1 <= X <=      4	// pwm_freq/sam_freq ratio


#endif	// _DEFPAR_H
