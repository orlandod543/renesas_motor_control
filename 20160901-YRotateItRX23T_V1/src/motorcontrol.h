
/*******************************************************************************
*	DISCLAIMER
*
*	THIS SOFTWARE IS PROVIDED "AS IS" AND BFG ENGINEERING MAKES NO WARRANTIES
*	REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING
*	BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
*	PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
*
*	WE (BFG ENGINEERING) DO NOT WARRANT THAT THIS SOFTWARE IS FREE FROM CLAIMS
*	BY A THIRD PARTY OF COPYRIGHT, PATENT, TRADEMARK, TRADE SECRET OR ANY OTHER
*	INTELLECTUAL PROPERTY INFRINGIMENT.
*
*	TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, BFG ENGINEERING
*	SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR
*	CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF BFG
*	ENGINEERING HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*	PARTICULARLY, UNDER NO CIRCUMSTANCES ARE WE LIABLE FOR ANY OF THE FOLLOWING:
*	- THIRD PARTY CLAIMS AGAINST YOU FOR LOSSES OR DAMAGES;
*	- LOSS OF, OR DAMAGE TO YOUR RECORDS OR DATA; OR
*	- ECONOMIC CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS OR SAVINGS) OR
*	  INCIDENTAL DAMAGES, EVEN IF WE ARE INFORMED OF THEIR POSSIBILITY.
*
*	BFG ENGINEERING DOES NOT WARRANT UNINTERRUPTED OR ERROR_FREE OPERATION OF
*	THIS SOFTWARE.
*	BFG ENGINEERING HAS NO OBLIGATION TO PROVIDE SERVICE, DEFECT CORRECTION OR
*	ANY MANTEINANCE OF THIS SOFTWARE; OR TO SUPPLY ANY UPDATES OR ENHANCEMENTS
*	EVEN IF SUCH ARE, OR LATER BECOME, AVAILABLE.
*
*	IF YOU DOWNLOAD OR USE THIS SOFTWARE YOU AGREE TO THESE TERMS, AND DO IT AT
*	YOUR OWN RISK.
*******************************************************************************/

/*******************************************************************************
*	File:		motorcontrol.h
*	Project:	YRotateIt-RX23T (three shunts)
*	Revision:	1.0
*	Date:		01-09-15 (DD-MM-YYYY)
*	Author:		Andrea Ghirardello
*	Contents:	Header for motorcontrol.c
*******************************************************************************/

#ifndef _MOTORCONTROL_H
#define _MOTORCONTROL_H

#include "typedefine.h"

/******************************************************************************
Function:		MC_WaitSync
Description:	time synchronization control function
Input:			nothing
Output:			0 if main loop time is elapsed, 1 if not
Modifies:		internal status variable
******************************************************************************/
uint16_t MC_WaitSync(void);

/******************************************************************************
Function:		MC_SetOff
Description:	current offsets measurement
Input:			nothing
Output:			nothing
Modifies:		current reading offsets, conversion constants
******************************************************************************/
void MC_SetOff(void);

/******************************************************************************
Function:		MC_IniPWM
Description:	pwm initialization
Input:			nothing
Output:			nothing
******************************************************************************/
void MC_IniPWM(void);

/******************************************************************************
Function:		MC_StartPWM
Description:	starts pwm and main interrupt (but not the outputs)
Input:			nothing
Output:			nothing
******************************************************************************/
void MC_StartPWM(void);

/******************************************************************************
Function:		MC_IniPar
Description:	internal constants initialization, using eeprom values
Input:			nothing
Output:			nothing
Modifies:		many internal variables, using eeprom parameters values
******************************************************************************/
void MC_IniPar(void);

/******************************************************************************
Function:		MC_UpdPar
Description:	internal constants update, if parameters are changed
Input:			nothing
Output:			nothing
Modifies:		many internal variables, using eeprom parameters values
******************************************************************************/
void MC_UpdPar(void);

/******************************************************************************
Function:		MC_ConFil
Description:	management of measured values, to give them to the GUI
Input:			nothing
Output:			nothing
Modifies:		interface variables defined in userif.h
******************************************************************************/
void MC_ConFil(void);

/******************************************************************************
Function:		MC_WorkMan
Description:	Working modes management function (speed ramps etc.)
Input:			nothing
Output:			nothing
Modifies:		internal speed reference
******************************************************************************/
void MC_WorkMan(void);

/******************************************************************************
Function:		MC_ConInt
Description:	control interrupt
Input:			nothing
Output:			nothing
Note:			this function is called every sampling instant, and performs
           		the motor control
******************************************************************************/
void MC_ConInt(void);

/******************************************************************************
Function:		MC_FaultInt
Description:	interrupt routine for POE (/fault from power stage)
Input:			nothing
Output:			nothing
Note:			this function stops the PWM output
******************************************************************************/
void MC_FaultInt(void);


#endif	// _MOTORCONTROL_H

// EOF motorcontrol.h

